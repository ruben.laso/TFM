#!/usr/bin/env bash

time_format="\nCommand 'time' results:\n"
time_format="${time_format}\tProgram Name and Command Line: %C\n"
time_format="${time_format}\tElapsed Real Time (secs): %e\n"
time_format="${time_format}\tTotal CPU-seconds: %S\n"
time_format="${time_format}\tCPU Percentage: %P\n"
time_format="${time_format}\tContext Switches: %c\n"
time_format="${time_format}\tMax Resident Set Size (Kb): %M\n"
time_format="${time_format}--------------------------------------\n"

images=("../../images/gaussian/florencia.jpg")
# images=("../../images/gaussian/lena_512.jpg")
original=("../../images/florencia.jpg")
# threads=(1 2 4 8 16 20 40 80)
threads=(1 2 4)
# threads=(1 2 4 8 16 32 68 136 272)
numtests=5
program="./color_bc.out"
# program="./color_bc_cuda.out"
# program="./color_bc_knl.out"
# program="./bw_var.out"
# program="./bw_var_cuda.out"
# program="./bw_var_knl.out"
# program="./color_var.out"
# program="./color_var_cuda.out"
# program="./color_var_knl.out"
# options="-d"
# options="-d -j 0 -i 30 -s 0.0000001 -b 1"
# options="-d -i 0 -j 30 -t 0.0000001 -c 1"
options="-d -i 30 -j 30 -b 1 -c 1"

for image in ${images[@]}; do
    for thread in ${threads[@]}; do
        export OMP_NUM_THREADS=${thread}
        for (( i = 0; i < ${numtests}; i++ )); do
            # date >> "${thread}_threads.times"
            # /usr/bin/time -f "${time_format}" -a -o "${thread}_threads.output" ${program} -n ${image} ${options} >> "${thread}_threads.times"
            # ${program} -n ${image} ${options} | grep  -A1 "Time;" >> "${thread}_threads.times"
            ${program} -n ${image} ${options} | awk '/Total;/{getline; print}' >> "${thread}_threads.times"
        done
    done
done
