#ifndef COLORIMAGE_BC
#define COLORIMAGE_BC

#include "Image.hpp"

class ColorImage_BC: public Image {
public:
    Matrix<float> brightness;
    Matrix<Vec3<float>> chromaticity;

    enum Chromaticity {
        anisotropic_flow,
        isotropic_flow
    };

    static Chromaticity switch_chromaticity(const int op) {
        switch (op) {
            case isotropic_flow:
                return isotropic_flow;
            case anisotropic_flow:
                return anisotropic_flow;
            default:
                std::cerr << "Wrong input..." << '\n';
                return isotropic_flow;
        }
    }

    enum Brightness {
        anisotropic,
        laplacian,
        bilaplacian,
        median
    };

    static Brightness switch_brightness(const int op) {
        switch (op) {
            case anisotropic:
                return anisotropic;
            case laplacian:
                return laplacian;
            case bilaplacian:
                return bilaplacian;
            case median:
                return median;
            default:
                std::cerr << "Wrong input..." << '\n';
                return anisotropic;
        }
    }

protected:
    template<class T, class VT>
    static inline void fill_channels(const cv::Mat & image, Matrix<T> & brightness, Matrix<VT> & chromaticity) {
        // #pragma omp parallel for shared(image, brightness, chromaticity) default(none)
        #pragma ivdep
        for (int i = 0; i < image.rows; i++) {
            #pragma ivdep
            for (int j = 0; j < image.cols; j++) {
                VT color = image.at<cv::Vec3f>(i,j); // BGR color format
                double magnitude = color.norm();
                if (magnitude == 0) {
                    color = VT(sqrt(1.0/3.0));
                }
                chromaticity.at(i,j) = color * 1.0 / (0 != magnitude ? magnitude : 1) ;
                brightness.at(i,j) = magnitude;
            }
        }
    }

    template<class T, class VT>
    static inline void fill_image(cv::Mat & image, const Matrix<T> & brightness, const Matrix<VT> & chromaticity) {
        #pragma omp parallel for shared(image, brightness, chromaticity) default(none)
        #pragma ivdep
        for (int i = 0; i < image.rows; i++) {
            #pragma ivdep
            for (int j = 0; j < image.cols; j++) {
                VT c = brightness.at(i,j) * chromaticity.at(i,j);
                image.at<cv::Vec3f>(i,j) = cv::Vec3f(c(0), c(1), c(2));
            }
        }
    }

public:
    ColorImage_BC(const ColorImage_BC & im) :
        Image(im),
        brightness(im.brightness),
        chromaticity(im.chromaticity)
    {}

    ColorImage_BC(const char * filename) {
        this->image = cv::imread(filename, CV_LOAD_IMAGE_COLOR);
        this->image.convertTo(this->image, CV_32F, 1.0 / UCHAR_MAX);

        if (!image.data) {
            std::cerr << "Could not open image " << filename << "." << '\n';
            return;
        }

        this->brightness = Matrix<float>(this->image.rows, this->image.cols);
        this->chromaticity = Matrix<Vec3<float>>(this->image.rows, this->image.cols);

        fill_channels(this->image, this->brightness, this->chromaticity);
    }

    ColorImage_BC(const cv::Mat & m) :
        Image(m)
    {
        this->brightness = Matrix<float>(this->image.rows, this->image.cols);
        this->chromaticity = Matrix<Vec3<float>>(this->image.rows, this->image.cols);

        fill_channels(this->image, this->brightness, this->chromaticity);
    }

    ColorImage_BC() {};

    ~ColorImage_BC() {};

    // Add noise to an image
    void gaussian_noise(const double variance) {
        this->Image::gaussian_noise(variance);
        fill_channels(this->image, this->brightness, this->chromaticity);
    }

    void denoise(const Brightness brightness, const Chromaticity chromaticity, const double lambda, const double threshold_brightness, const double threshold_chromaticity, const size_t c_max_iterations, const size_t b_max_iterations, double & seconds) {
        auto start = std::chrono::system_clock::now();

        switch (brightness) {
            case anisotropic:
                #ifdef __CUDA__
                brightness_anisotropic_CUDA(this->brightness, threshold_brightness, c_max_iterations);
                #else
                brightness_anisotropic_flow<Matrix<float>,float>(this->brightness, threshold_brightness, c_max_iterations);
                #endif
                break;
            case laplacian:
                #ifdef __CUDA__
                brightness_laplacian_CUDA(this->brightness, threshold_brightness, c_max_iterations);
                #else
                brightness_laplacian_flow<Matrix<float>,float>(this->brightness, threshold_brightness, c_max_iterations);
                #endif
                break;
            case bilaplacian: { // define scope to declare variables
                std::cout << "Bilaplacian method:" << '\n';
                cv::Mat br(this->brightness.rows, this->brightness.cols, CV_32F, this->brightness.values);
                float * b = conjugate_gradient<float>(br, lambda, Image::Method::conj_grad, seconds);
                type2cv(b, br);
                delete[] b;
                std::swap((void *&) br.data, (void *&) this->brightness.values);
                std::cout << "Bilaplacian time: " << seconds << '\n';
                break;
            }
            case median: {
                median_filter<Matrix<float>, float>(this->brightness, 3);
                break;
            }
            default:
                #ifdef __CUDA__
                brightness_anisotropic_CUDA(this->brightness, threshold_brightness, c_max_iterations);
                #else
                brightness_anisotropic_flow<Matrix<float>,float>(this->brightness, threshold_brightness, c_max_iterations);
                #endif
                break;
        }

        switch (chromaticity) {
            case isotropic_flow:
                #ifdef __CUDA__
                chromaticity_isotropic_CUDA(this->chromaticity, threshold_chromaticity, b_max_iterations);
                #else
                gradient_descent_isotropic<Matrix<Vec3<float>>, float, Vec3<float>>(this->chromaticity, threshold_chromaticity, b_max_iterations);
                #endif
                break;
            case anisotropic_flow:
                #ifdef __CUDA__
                chromaticity_anisotropic_CUDA(this->chromaticity, threshold_chromaticity, b_max_iterations);
                #else
                gradient_descent_anisotropic<Matrix<Vec3<float>>, float, Vec3<float>>(this->chromaticity, threshold_chromaticity, b_max_iterations);
                #endif
                break;
            default:
                #ifdef __CUDA__
                chromaticity_isotropic_CUDA(this->chromaticity, threshold_chromaticity, b_max_iterations);
                #else
                gradient_descent_isotropic<Matrix<Vec3<float>>, float, Vec3<float>>(this->chromaticity, threshold_chromaticity, b_max_iterations);
                #endif
                break;
        }
        fill_image(this->image, this->brightness, this->chromaticity);

        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_time = end-start;
        seconds = elapsed_time.count();
    }
};

#endif
