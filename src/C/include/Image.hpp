#ifndef IMAGE
#define IMAGE

#include <iostream>
#include <cstdlib>
#include <chrono>

#include "Eigen"
#include "SVD"

#include <opencv2/opencv.hpp>
#include <opencv2/core/eigen.hpp>

#include "BilapSparse.hpp"
#include "Matrix.hpp"
#include "Vec3.hpp"
#include "Math.hpp"

#ifdef __CUDA__
#include "CUDA/Math_CUDA.hpp"
#endif

#ifdef ENABLE_OPENMP
#include <omp.h>
#endif


class Image
{
public:
    cv::Mat image;

    enum Method {
        eigen_cg,
        conj_grad,
    };

    static Method switch_method(const int op) {
        switch (op) {
            case eigen_cg:
                return eigen_cg;
            case conj_grad:
                return conj_grad;
            default:
                std::cerr << "Wrong input..." << '\n';
                return conj_grad;
        }
    }

protected:

    template <class T>
    static void cv2type(const cv::Mat & mat, T *& matrix) {
        matrix = new T[mat.rows * mat.cols];
        #pragma omp parallel for
        for (int j = 0; j < mat.cols; j++) {
            for (int i = 0; i < mat.rows; i++) {
                matrix[j*mat.rows + i] = mat.at<T>(i,j);
            }
        }
    }

    template <class T>
    static void type2cv(const T * matrix, cv::Mat & mat) {
        #pragma omp parallel for
        for (int j = 0; j < mat.cols; j++) {
            for (int i = 0; i < mat.rows; i++) {
                mat.at<T>(i,j) = matrix[j*mat.rows + i];
            }
        }
    }

    template<class T>
    static T * conjugate_gradient(const cv::Mat & mat, const float lambda, const Method method, double &seconds) {
        auto start = std::chrono::system_clock::now();
        const int rows = mat.rows;
        const int cols = mat.cols;
        // Create independent term vector, elements are stored by columns
        T * rh;
        cv2type<T>(mat, rh);
        // Introduce black frame
        for (int i = 0; i < rows; i++) {
            rh[i] = 0.0; // Left
            rh[(cols-1)*rows + i] = 0.0; // Right
        }
        for (int i = 1; i < cols-1; i++) {
            rh[i*rows] = 0.0; // Top
            rh[i*rows + rows-1] = 0.0; // Bottom
        }
        // Linear system construction
        BilapSparse<T> Lh(lambda, rows*cols, rows);

        T * result = nullptr;
        switch (method) {
            case eigen_cg:
                result = solve_system<T>(Lh, rh);
                break;
            case conj_grad:
                #ifdef __CUDA__
                    result = solve_system_CG_CUDA(Lh, rh);
                #else
                    result = solve_system_CG<T>(Lh, rh);
                #endif
                break;
            default:
                #ifdef __CUDA__
                    result = solve_system_CG_CUDA(Lh, rh);
                #else
                    result = solve_system_CG<T>(Lh, rh);
                #endif
                break;
        }
        delete[] rh;
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_time = end-start;
        seconds = elapsed_time.count();

        return result;
    }

    template<class M, class T>
    inline void median_filter(M & im, const size_t window_size) {
        M r(im.rows, im.cols);
        auto start = std::chrono::system_clock::now();
        const size_t ws = window_size / 2;

        #pragma omp parallel for firstprivate(ws) shared(r, im) default(none)
        for (size_t i = 0; i < ws; i++) {
            for (size_t j = 0; j < im.cols; j++) {
                std::vector<T> v(window_size * window_size);
                size_t pos = 0;
                for (size_t ii = 0; ii <= i + ws; ii++) {
                    for (size_t jj = (int(j - ws) < 0 ? 0 : j - ws); jj <= (j + ws >= im.cols ? im.cols - 1 : j + ws) ; jj++) {
                        v[pos++] = im.at(ii, jj);
                    }
                }
                v.resize(pos);
                std::sort(v.begin(), v.end());
                r.at(i,j) = v[v.size() / 2]; // median value
            }
        }
        #pragma omp parallel for firstprivate(ws) shared(r, im) default(none)
        for (size_t i = ws; i < im.rows - ws; i++) {
            for (size_t j = 0; j < ws; j++) {
                std::vector<T> v(window_size * window_size);
                size_t pos = 0;
                for (size_t ii = i - ws; ii <= i + ws; ii++) {
                    for (size_t jj = 0; jj <= j + ws; jj++) {
                        v[pos++] = im.at(ii, jj);
                    }
                }
                v.resize(pos);
                std::sort(v.begin(), v.end());
                r.at(i,j) = v[v.size() / 2]; // median value
            }
            for (size_t j = ws; j < im.cols - ws; j++) {
                std::vector<T> v(window_size * window_size);
                size_t pos = 0;
                for (size_t ii = i - ws; ii <= i + ws; ii++) {
                    for (size_t jj = j - ws; jj <= j + ws; jj++) {
                        v[pos++] = im.at(ii, jj);
                    }
                }
                std::sort(v.begin(), v.end());
                r.at(i,j) = v[window_size * window_size / 2]; // median value
            }
            for (size_t j = im.cols - ws; j < im.cols; j++) {
                std::vector<T> v(window_size * window_size);
                size_t pos = 0;
                for (size_t ii = i - ws; ii <= i + ws; ii++) {
                    for (size_t jj = j - ws; jj < im.cols; jj++) {
                        v[pos++] = im.at(ii, jj);
                    }
                }
                v.resize(pos);
                std::sort(v.begin(), v.end());
                r.at(i,j) = v[v.size() / 2]; // median value
            }
        }
        #pragma omp parallel for firstprivate(ws) shared(r, im) default(none)
        for (size_t i = im.rows - ws; i < im.rows; i++) {
            for (size_t j = 0; j < im.cols; j++) {
                std::vector<T> v(window_size * window_size);
                size_t pos = 0;
                for (size_t ii = i - ws; ii <= (i + ws >= im.rows ? im.rows - 1 : i + ws) ; ii++) {
                    for (size_t jj = (int(j - ws) < 0 ? 0 : j - ws) ; jj <= (j + ws >= im.cols ? im.cols - 1 : j + ws) ; jj++) {
                        v[pos++] = im.at(ii, jj);
                    }
                }
                v.resize(pos);
                std::sort(v.begin(), v.end());
                r.at(i,j) = v[v.size() / 2]; // median value
            }
        }

        std::swap(im.values, r.values);
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_time = end-start;
        double seconds = elapsed_time.count();
        std::cout << "Median filter time: " << seconds << " seconds" << '\n';
    }

public:
    /* Constructors */
    Image() {}

    Image(const Image &im) :
        image(im.image.clone())
    {}

    Image(const char * filename) :
        image(cv::imread(filename, CV_LOAD_IMAGE_GRAYSCALE))
    {
        this->image.convertTo(this->image, CV_32F, 1.0 / UCHAR_MAX);

        if (!image.data) {
            std::cerr << "Could not open image " << filename << "." << '\n';
            return;
        }

        std::cout << "Image " << filename << " opened. Size: " << this->image.rows << " x " << this->image.cols << '\n';
    }

    template <class T>
    Image(const T * matrix, const int rows, const int cols, const int type) :
        image(rows, cols, type)
    {
        type2cv(matrix);
    }

    Image(const cv::Mat matrix) :
        image(matrix)
    {}

    ~Image() {
        // do nothing
    }

    // Add noise to an image
    void gaussian_noise(const double variance) {
        const int type = this->image.type();
        cv::Mat noise(this->image.size(), type);
        cv::theRNG().state = cv::getTickCount();
        cv::randn(noise, cv::Scalar::all(0.0), cv::Scalar::all(sqrt(variance)));
        this->image += noise;
    }

    void write(const char * filename) {
        cv::imwrite(filename, image * UCHAR_MAX);
        std::cout << "Image written in file " << filename << ". Size: " << this->image.rows << " x " << this->image.cols << '\n';
    }

    void display(const char * window) {
        cv::namedWindow(window, cv::WINDOW_NORMAL);
        imshow(window, this->image);
    }

    double norm() {
        return cv::norm(this->image);
    }

    template<class T>
    void denoise(const float lambda, const Method method, double & seconds) {
        // Clean noise
        auto start = std::chrono::system_clock::now();

        T * result = conjugate_gradient<T>(this->image, lambda, method, seconds);

        type2cv(result, this->image);

        delete[] result;

        // End of denoising
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_time = end-start;
        seconds = elapsed_time.count();
    }

    double PSNR(const Image & im) {
        cv::Mat s1;
        cv::absdiff(this->image, im.image, s1);
        s1 = s1.mul(s1);
        cv::Scalar s = sum(s1);

        double sse = 0.0;
        for (int i = 0; i < this->image.channels(); i++) {
            sse += s.val[i];
        }

        if (sse <= 1e-10) {
            return std::numeric_limits<double>::infinity();
        } else {
            double mse = sse / double(this->image.channels() * this->image.total());
            double psnr = 10.0*log10(1.0 / mse);
            return psnr;
        }
    }
};

#endif
