#ifndef BILAPSPARSE
#define BILAPSPARSE

#ifdef ENABLE_OPENMP
#include <omp.h>
#endif

template<class T>
class BilapSparse {
public:
    size_t rows;
    size_t cols;
    size_t dim;
    size_t block_dim;
    size_t num_blocks;
    T c1;
    T c1_1;
    T c1_2;
    T c2;
    T c3;
    T c4;
    T c5;
    T c6;
    const size_t max_nz = 13;

public:
    BilapSparse(BilapSparse && a) :
        rows(a.rows),
        cols(a.cols),
        dim(a.dim),
        block_dim(a.block_dim),
        num_blocks(a.num_blocks),
        c1(a.c1),
        c1_1(a.c1_1),
        c1_2(a.c1_2),
        c2(a.c2),
        c3(a.c3),
        c4(a.c4),
        c5(a.c5),
        c6(a.c6)
    { }

    BilapSparse(const BilapSparse & a) :
        rows(a.rows),
        cols(a.cols),
        dim(a.dim),
        block_dim(a.block_dim),
        num_blocks(a.num_blocks),
        c1(a.c1),
        c1_1(a.c1_1),
        c1_2(a.c1_2),
        c2(a.c2),
        c3(a.c3),
        c4(a.c4),
        c5(a.c5),
        c6(a.c6)
    { }

    BilapSparse() :
        rows(0),
        cols(0),
        dim(0),
        block_dim(0),
        num_blocks(0),
        c1(20+1),
        c1_1(21+1),
        c1_2(22+1),
        c2(-8),
        c3(1),
        c4(-8),
        c5(2),
        c6(1)
    { }

    BilapSparse(const T lambda) :
        rows(0),
        cols(0),
        dim(0),
        block_dim(0),
        num_blocks(0),
        c1(lambda*20+1),
        c1_1(lambda*21+1),
        c1_2(lambda*22+1),
        c2(lambda*-8),
        c3(lambda*1),
        c4(lambda*-8),
        c5(lambda*2),
        c6(lambda*1)
    { }

    BilapSparse(const size_t size, const size_t block_size) :
        rows(size),
        cols(size),
        dim(size),
        block_dim(block_size),
        num_blocks(size / block_size),
        c1(20+1),
        c1_1(21+1),
        c1_2(22+1),
        c2(-8),
        c3(1),
        c4(-8),
        c5(2),
        c6(1)
    { }

    BilapSparse(const T lambda, const size_t size, const size_t block_size) :
        rows(size),
        cols(size),
        dim(size),
        block_dim(block_size),
        num_blocks(size / block_size),
        c1(lambda*20+1),
        c1_1(lambda*21+1),
        c1_2(lambda*22+1),
        c2(lambda*-8),
        c3(lambda*1),
        c4(lambda*-8),
        c5(lambda*2),
        c6(lambda*1)
    { }

    ~BilapSparse() { }

    inline T at(size_t i, size_t j) const {
        if (i > j) {
            std::swap(i, j);
        }
        unsigned int sw = j - i;

        switch (sw) {
            case 0: // the element is in the diagonal
                // Handle diagonal cases
                if (i == 0 ||
                    i == block_dim-1 ||
                    i == (block_dim-1)*block_dim-1 ||
                    i == block_dim*block_dim-1) {
                    return c1_2;
                }
                else if (i % block_dim == 0 || i % block_dim == block_dim-1) {
                    return c1_1;
                }
                else if (i/block_dim == 0 || i/block_dim == block_dim-1) {
                    return c1_1;
                }
                return c1;
            case 1: // the element is just over/below the diagonal
                if (i % block_dim == block_dim-1) {
                    return 0;
                }
                return c2;
            case 2: // the element is 2 positions over/below the diagonal
                if (i % block_dim == block_dim-1 || i % block_dim == block_dim-2) {
                    return 0;
                }
                return c3;
            default:
                break;
        }

        if (sw == block_dim) {
            return c4;
        }
        else if (sw == block_dim+1 || sw == block_dim-1) {
            if ((i == ((i+1)/block_dim)*block_dim - 1 && j % block_dim == 0) ||
                (j == ((j+1)/block_dim)*block_dim - 1 && i % block_dim == 0)) {
                return 0;
            }
            return c5;
        }
        else if (sw == 2*block_dim) {
            return c6;
        }

        return 0;
    }

    const inline std::vector<size_t> nz(const size_t row, std::vector<T> & values) const {
        std::vector<size_t> nz;
        nz.reserve(max_nz);
        size_t first, last;

        // Compute first element j index
        if (int(row-2*block_dim) < 0) {
            if (int(row-block_dim) < 0) {
                if (int(row-2) < 0) {
                    first = 0;
                } else {
                    first = row-2;
                }
            } else {
                first = int(row-block_dim-1) < 0 ? row-block_dim : row-block_dim-1 ;
            }
        } else {
            first = row - 2*block_dim;
        }
        // Compute last element j index
        if (row + 2*block_dim >= dim) {
            if (row + block_dim >= dim) {
                if (row+block_dim+2 >= dim) {
                    last = row+block_dim+1 >= dim ? dim-1 : row+block_dim+1;
                } else {
                    last = row+block_dim+2;
                }
            } else {
                last = row+block_dim+1 >= dim ? dim-1 : row+block_dim+1;
            }
        } else {
            last = row + 2*block_dim;
        }

        // Store indexes of non-zero elements
        T aij_old = this->at(row,first);
        for (size_t j = first; j <= last; j++) {
            T aij = this->at(row,j);
            if (aij != 0) {
                nz.push_back(j);
                values.push_back(aij);
                aij_old = aij;
            } else if (aij == 0 && aij_old != 0) {
                // Found a block of zeroes, "jump" to the next block
                j += block_dim - 5;
                aij_old = 0;
            }
        }

        return nz;
    }

    inline T * operator*(const T * v) const {
        T * r = new T[this->dim];

        this->prod_vec(v, r);

        return r;
    }

    inline void prod_vec(const T * __restrict__ v, T * __restrict__ res) const {
        const size_t n = this->block_dim;
        size_t i;
        size_t N;

        // FIRST BLOCK OF ROWS
        res[0] = c1_2*v[0] + c2*v[1] + c3*v[2] +
            c4*v[n] + c5*v[n+1] +
            c6*v[2*n];
        res[1] = c2*v[0] + c1_1*v[1] + c2*v[2] + c3*v[3] +
            c5*v[n] + c4*v[n+1] + c5*v[n+2] +
            c6*v[2*n+1];
        #pragma omp parallel for private(i)
        for (i = 2; i < n-2; i++) {
            res[i] = c3*v[i-2] + c2*v[i-1] + c1_1*v[i] + c2*v[i+1] + c3*v[i+2] +
                c5*v[n+i-1] + c4*v[n+i] + c5*v[n+i+1] +
                c6*v[2*n+i];
        }
        i = n-2;
        res[i] = c3*v[i-2] + c2*v[i-1] + c1_1*v[i] + c2*v[i+1] +
            c5*v[n+i-1] + c4*v[n+i] + c5*v[n+i+1] +
            c6*v[2*n+i];
        i++;
        res[i] = c3*v[i-2] + c2*v[i-1] + c1_2*v[i] +
            c5*v[n+i-1] + c4*v[n+i] +
            c6*v[2*n+i];

        // SECOND BLOCK OF ROWS
        res[n] = c4*v[0] + c5*v[1]  +
            c1_1*v[n] + c2*v[n+1]   + c3*v[n+2] +
            c4*v[2*n] + c5*v[2*n+1] +
            c6*v[3*n];
        res[n+1] = c5*v[0] + c4*v[1] + c5*v[2] +
            c2*v[n]   + c1*v[n+1]   + c2*v[n+2]   + c3*v[n+3] +
            c5*v[2*n] + c4*v[2*n+1] + c5*v[2*n+2] +
            c6*v[3*n];
        #pragma omp parallel for private(i)
        for (i = 2; i < n-2; i++) {
            res[n+i] = c5*v[i-1] + c4*v[i]  + c5*v[i+1] +
                c3*v[n+i-2]   + c2*v[n+i-1] + c1*v[n+i]     + c2*v[n+i+1] + c3*v[n+i+2] +
                c5*v[2*n+i-1] + c4*v[2*n+i] + c5*v[2*n+i+1] +
                c6*v[3*n+i];
        }
        i = n-2;
        res[n+i] = c5*v[i-1] + c4*v[i]  + c5*v[i+1] +
            c3*v[n+i-2]   + c2*v[n+i-1] + c1*v[n+i]     + c2*v[n+i+1] +
            c5*v[2*n+i-1] + c4*v[2*n+i] + c5*v[2*n+i+1] +
            c6*v[3*n+i];
        i++;
        res[n+i] = c5*v[i-1] + c4*v[i]  +
            c3*v[n+i-2]   + c2*v[n+i-1] + c1_1*v[n+i] +
            c5*v[2*n+i-1] + c4*v[2*n+i] +
            c6*v[3*n+i];

        // MEDIUM BLOCKS [2, N-2)
        #pragma omp parallel for private (N, i)
        for (N = 0; N < num_blocks-4; N++) {
            res[(N+2)*n] = c6*v[N*n] +
                c4*v[N*n+n]     + c5*v[N*n+n+1]   +
                c1_1*v[N*n+2*n] + c2*v[N*n+2*n+1] + c3*v[N*n+2*n+2] +
                c4*v[N*n+3*n]   + c5*v[N*n+3*n+1] +
                c6*v[N*n+4*n];
            res[(N+2)*n+1] = c6*v[N*n+1] +
                c5*v[N*n+n]   + c4*v[N*n+n+1]   + c5*v[N*n+n+2]   +
                c2*v[N*n+2*n] + c1*v[N*n+2*n+1] + c2*v[N*n+2*n+2] + c3*v[N*n+2*n+3] +
                c5*v[N*n+3*n] + c4*v[N*n+3*n+1] + c5*v[N*n+3*n+2] +
                c6*v[N*n+4*n+1];
            for (i = 2; i < n-2; i++) {
                res[(N+2)*n+i] = c6*v[N*n+i] +
                    c5*v[N*n+n+i-1]   + c4*v[N*n+n+i]     + c5*v[N*n+n+i+1]   +
                    c3*v[N*n+2*n+i-2] + c2*v[N*n+2*n+i-1] + c1*v[N*n+2*n+i]   + c2*v[N*n+2*n+i+1] + c3*v[N*n+2*n+i+2] +
                    c5*v[N*n+3*n+i-1] + c4*v[N*n+3*n+i]   + c5*v[N*n+3*n+i+1] +
                    c6*v[N*n+4*n+i];
            }
            i = n - 2;
            res[(N+2)*n+i] = c6*v[N*n+i] +
                c5*v[N*n+n+i-1]   + c4*v[N*n+n+i]     + c5*v[N*n+n+i+1]   +
                c3*v[N*n+2*n+i-2] + c2*v[N*n+2*n+i-1] + c1*v[N*n+2*n+i]   + c2*v[N*n+2*n+i+1] +
                c5*v[N*n+3*n+i-1] + c4*v[N*n+3*n+i]   + c5*v[N*n+3*n+i+1] +
                c6*v[N*n+4*n+i];
            i++;
            res[(N+2)*n+i] = c6*v[N*n+i] +
                c5*v[N*n+n+i-1]   + c4*v[N*n+n+i]     +
                c3*v[N*n+2*n+i-2] + c2*v[N*n+2*n+i-1] + c1_1*v[N*n+2*n+i] +
                c5*v[N*n+3*n+i-1] + c4*v[N*n+3*n+i]   +
                c6*v[N*n+4*n+i];
        }
        N = num_blocks-4;

        // BLOCK N-2
        res[(N+2)*n] = c6*v[N*n+0] +
            c4*v[N*n+n]     + c5*v[N*n+n+1]   +
            c1_1*v[N*n+2*n] + c2*v[N*n+2*n+1] + c3*v[N*n+2*n+2] +
            c4*v[N*n+3*n]   + c5*v[N*n+3*n+1];
        res[(N+2)*n+1] = c6*v[N*n+1] +
            c5*v[N*n+n]   + c4*v[N*n+n+1]   + c5*v[N*n+n+2]   +
            c2*v[N*n+2*n] + c1*v[N*n+2*n+1] + c2*v[N*n+2*n+2] + c3*v[N*n+2*n+3] +
            c5*v[N*n+3*n] + c4*v[N*n+3*n+1] + c5*v[N*n+3*n+2];
        #pragma omp parallel for private(i) firstprivate(N)
        for (i = 2; i < n-2; i++) {
            res[(N+2)*n+i] = c6*v[N*n+i] +
                c5*v[N*n+n+i-1]   + c4*v[N*n+n+i]     + c5*v[N*n+n+i+1] +
                c3*v[N*n+2*n+i-2] + c2*v[N*n+2*n+i-1] + c1*v[N*n+2*n+i] + c2*v[N*n+2*n+i+1] + c3*v[N*n+2*n+i+2] +
                c5*v[N*n+3*n+i-1] + c4*v[N*n+3*n+i]   + c5*v[N*n+3*n+i+1];
        }
        i = n-2;
        res[(N+2)*n+i] = c6*v[N*n+i] +
            c5*v[N*n+n+i-1]   + c4*v[N*n+n+i]     + c5*v[N*n+n+i+1] +
            c3*v[N*n+2*n+i-2] + c2*v[N*n+2*n+i-1] + c1*v[N*n+2*n+i] + c2*v[N*n+2*n+i+1] +
            c5*v[N*n+3*n+i-1] + c4*v[N*n+3*n+i]   + c5*v[N*n+3*n+i+1];
        i++;
        res[(N+2)*n+i] = c6*v[N*n+i] +
            c5*v[N*n+n+i-1]   + c4*v[N*n+n+i]     + c5*v[N*n+n+i+1] +
            c3*v[N*n+2*n+i-2] + c2*v[N*n+2*n+i-1] + c1*v[N*n+2*n+i] +
            c5*v[N*n+3*n+i-1] + c4*v[N*n+3*n+i];

        N++;
        // BLOCK N-1 (LAST BLOCK)
        res[(N+2)*n] = c6*v[N*n+0] +
            c4*v[N*n+n]     + c5*v[N*n+n+1]   +
            c1_2*v[N*n+2*n] + c2*v[N*n+2*n+1] + c3*v[N*n+2*n+2];
        res[(N+2)*n+1] = c6*v[N*n+1] +
            c5*v[N*n+n]   + c4*v[N*n+n+1]     + c5*v[N*n+n+2] +
            c2*v[N*n+2*n] + c1_1*v[N*n+2*n+1] + c2*v[N*n+2*n+2] + c3*v[N*n+2*n+3];
        #pragma omp parallel for private(i) firstprivate(N)
        for (i = 2; i < n-2; i++) {
            res[(N+2)*n+i] = c6*v[N*n+i] +
                c5*v[N*n+n+i-1]   + c4*v[N*n+n+i]     + c5*v[N*n+n+i+1]   +
                c3*v[N*n+2*n+i-2] + c2*v[N*n+2*n+i-1] + c1_1*v[N*n+2*n+i] + c2*v[N*n+2*n+i+1] + c3*v[N*n+2*n+i+2];
        }
        i = n-2;
        res[(N+2)*n+i] = c6*v[N*n+i] +
            c5*v[N*n+n+i-1]   + c4*v[N*n+n+i]     + c5*v[N*n+n+i+1]   +
            c3*v[N*n+2*n+i-2] + c2*v[N*n+2*n+i-1] + c1_1*v[N*n+2*n+i] + c2*v[N*n+2*n+i+1];
        i++;
        res[(N+2)*n+i] = c6*v[N*n+i] +
            c5*v[N*n+n+i-1]   + c4*v[N*n+n+i]     +
            c3*v[N*n+2*n+i-2] + c2*v[N*n+2*n+i-1] + c1_1*v[N*n+2*n+i];
    }
};


#endif
