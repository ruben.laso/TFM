#ifndef VEC3
#define VEC3

#include <cstdlib>
#include <algorithm>

#include <opencv2/opencv.hpp>

template<class T>
class Vec3 {
public:
    T BGR[3];

public:
    Vec3()
    {}

    Vec3(T _B, T _G, T _R) {
        BGR[0] = _B;
        BGR[1] = _G;
        BGR[2] = _R;
    }

    Vec3(const Vec3 & v) {
        BGR[0] = v.BGR[0];
        BGR[1] = v.BGR[1];
        BGR[2] = v.BGR[2];
    }

    Vec3(const cv::Vec<T,3> & v) {
        BGR[0] = v[0];
        BGR[1] = v[1];
        BGR[2] = v[2];
    }

    Vec3(const T & v) {
        BGR[0] = v;
        BGR[1] = v;
        BGR[2] = v;
    }

    ~Vec3() {}

    inline const T & operator()(const size_t i) const {
        return BGR[i];
    }

    inline const T & operator()(const size_t i) {
        return BGR[i];
    }

    inline Vec3 & operator=(const Vec3 & v) {
        BGR[0] = v.BGR[0];
        BGR[1] = v.BGR[1];
        BGR[2] = v.BGR[2];
        return *this;
    }

    inline double norm() {
        return sqrt(BGR[0] * BGR[0] + BGR[1] * BGR[1] + BGR[2] * BGR[2]);
    }

    inline double norm2_pow2() {
        return BGR[0] * BGR[0] + BGR[1] * BGR[1] + BGR[2] * BGR[2];
    }

    inline double norm2(const Vec3 v) {
        return sqrt(BGR[0] * v.BGR[0] + BGR[1] * v.BGR[1] + BGR[2] * v.BGR[2]);
    }

    inline double norm2_pow2(const Vec3 v) {
        return BGR[0] * v.BGR[0] + BGR[1] * v.BGR[1] + BGR[2] * v.BGR[2];
    }
};

template<class T>
std::ostream & operator<<(std::ostream& os, const Vec3<T> & v) {
    os << v.BGR[0] << ", " << v.BGR[1] << ", " << v.BGR[2];
    return os;
}

template<class T>
inline Vec3<T> operator+(const Vec3<T> & v1, const Vec3<T> & v2) {
    return Vec3<T>(v1.BGR[0] + v2.BGR[0], v1.BGR[1] + v2.BGR[1], v1.BGR[2] + v2.BGR[2]);
}

template<class T>
inline Vec3<T> operator+(Vec3<T> & v, const int & s) {
    return Vec3<T>(v.BGR[0] + s, v.BGR[1] + s, v.BGR[2] + s);
}

template<class T>
inline Vec3<T> operator+(Vec3<T> & v, const float & s) {
    return Vec3<T>(v.BGR[0] + s, v.BGR[1] + s, v.BGR[2] + s);
}

template<class T>
inline Vec3<T> operator+(Vec3<T> & v, const double & s) {
    return Vec3<T>(v.BGR[0] + s, v.BGR[1] + s, v.BGR[2] + s);
}

template<class T>
inline Vec3<T> operator+(const int & s, Vec3<T> & v) {
    return v + s;
}

template<class T>
inline Vec3<T> operator+(const float & s, Vec3<T> & v) {
    return v + s;
}

template<class T>
inline Vec3<T> operator+(const double & s, Vec3<T> & v) {
    return v + s;
}

template<class T>
inline Vec3<T> & operator+=(const Vec3<T> & v, const int & s) {
    return v = v + s;
}

template<class T>
inline Vec3<T> & operator+=(const Vec3<T> & v, const float & s) {
    return v = v + s;
}

template<class T>
inline Vec3<T> & operator+=(const Vec3<T> & v, const double & s) {
    return v = v + s;
}

template<class T>
inline Vec3<T> & operator+=(const int & s, const Vec3<T> v) {
    return v + s;
}
template<class T>
inline Vec3<T> & operator+=(const float & s, const Vec3<T> v) {
    return v + s;
}
template<class T>
inline Vec3<T> & operator+=(const double & s, const Vec3<T> v) {
    return v + s;
}
template<class T>
inline Vec3<T> & operator+=(Vec3<T> & v1, const Vec3<T> & v2) {
    return v1 = v1 + v2;
}


template<class T>
inline Vec3<T> operator-(const Vec3<T> & v1, const Vec3<T> & v2) {
    return Vec3<T>(v1.BGR[0] - v2.BGR[0], v1.BGR[1] - v2.BGR[1], v1.BGR[2] - v2.BGR[2]);
}

template<class T>
inline Vec3<T> operator-(const Vec3<T> & v, const int & s) {
    return Vec3<T>(v.BGR[0] - s, v.BGR[1] - s, v.BGR[2] - s);
}

template<class T>
inline Vec3<T> operator-(const Vec3<T> & v, const float & s) {
    return Vec3<T>(v.BGR[0] - s, v.BGR[1] - s, v.BGR[2] - s);
}

template<class T>
inline Vec3<T> operator-(const Vec3<T> & v, const double & s) {
    return Vec3<T>(v.BGR[0] - s, v.BGR[1] - s, v.BGR[2] - s);
}

template<class T>
inline Vec3<T> operator-(const int & s, const Vec3<T> & v) {
    return v + s;
}

template<class T>
inline Vec3<T> operator-(const float & s, const Vec3<T> & v) {
    return v + s;
}

template<class T>
inline Vec3<T> operator-(const double & s, const Vec3<T> & v) {
    return v + s;
}

template<class T>
inline Vec3<T> & operator-=(Vec3<T> & v1, const Vec3<T> & v2) {
    return v1 = v1 - v2;
}

template<class T>
inline Vec3<T> & operator-=(Vec3<T> & v, const int & s) {
    return v = v - s;
}

template<class T>
inline Vec3<T> & operator-=(Vec3<T> & v, const float & s) {
    return v = v - s;
}

template<class T>
inline Vec3<T> & operator-=(Vec3<T> & v, const double & s) {
    return v = v - s;
}

template<class T>
inline Vec3<T> operator*(const Vec3<T> & v1, const Vec3<T> & v2) {
    return Vec3<T>(v1.BGR[0] * v2.BGR[0], v1.BGR[1] * v2.BGR[1], v1.BGR[2] * v2.BGR[2]);
}

template<class T>
inline Vec3<T> operator*(const Vec3<T> & v, const int & s) {
    return Vec3<T>(v.BGR[0] * s, v.BGR[1] * s, v.BGR[2] * s);
}

template<class T>
inline Vec3<T> operator*(const Vec3<T> & v, const float & s) {
    return Vec3<T>(v.BGR[0] * s, v.BGR[1] * s, v.BGR[2] * s);
}

template<class T>
inline Vec3<T> operator*(const Vec3<T> & v, const double & s) {
    return Vec3<T>(v.BGR[0] * s, v.BGR[1] * s, v.BGR[2] * s);
}

template<class T>
inline Vec3<T> operator*(const int & s, const Vec3<T> & v) {
    return v * s;
}

template<class T>
inline Vec3<T> operator*(const float & s, const Vec3<T> & v) {
    return v * s;
}

template<class T>
inline Vec3<T> operator*(const double & s, const Vec3<T> & v) {
    return v * s;
}

template<class T>
inline Vec3<T> & operator*=(Vec3<T> & v1, const Vec3<T> & v2) {
    return v1 = v1 * v2;
}

template<class T>
inline Vec3<T> & operator*=(Vec3<T> & v, const int & s) {
    return v = v * s;
}

template<class T>
inline Vec3<T> & operator*=(Vec3<T> & v, const float & s) {
    return v = v * s;
}

template<class T>
inline Vec3<T> & operator*=(Vec3<T> & v, const double & s) {
    return v = v * s;
}

template<class T>
inline Vec3<T> operator/(const Vec3<T> & v1, const Vec3<T> & v2) {
    return Vec3<T>(v1.BGR[0] / v2.BGR[0], v1.BGR[1] / v2.BGR[1], v1.BGR[2] / v2.BGR[2]);
}

template<class T>
inline Vec3<T> operator/(const Vec3<T> & v, const int & s) {
    return Vec3<T>(v.BGR[0] / s, v.BGR[1] / s, v.BGR[2] / s);
}

template<class T>
inline Vec3<T> operator/(const Vec3<T> & v, const float & s) {
    return Vec3<T>(v.BGR[0] / s, v.BGR[1] / s, v.BGR[2] / s);
}

template<class T>
inline Vec3<T> operator/(const Vec3<T> & v, const double & s) {
    return Vec3<T>(v.BGR[0] / s, v.BGR[1] / s, v.BGR[2] / s);
}

template<class T>
inline Vec3<T> operator/(const int & s, const Vec3<T> & v) {
    return v / s;
}

template<class T>
inline Vec3<T> operator/(const float & s, const Vec3<T> & v) {
    return v / s;
}

template<class T>
inline Vec3<T> operator/(const double & s, const Vec3<T> & v) {
    return v / s;
}

template<class T>
inline Vec3<T> & operator/=(Vec3<T> & v, const int & s) {
    return v = v / s;
}

template<class T>
inline Vec3<T> & operator/=(Vec3<T> & v, const float & s) {
    return v = v / s;
}

template<class T>
inline Vec3<T> & operator/=(Vec3<T> & v, const double & s) {
    return v = v / s;
}
#endif
