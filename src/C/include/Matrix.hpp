#ifndef MATRIX
#define MATRIX

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>
#ifdef ENABLE_OPENMP
#include <omp.h>
#endif

template<class T>
class Matrix {
public:
    size_t rows;
    size_t cols;
    T * values = nullptr;

    Matrix(Matrix && a) :
        rows(a.rows),
        cols(a.cols),
        values(a.values)
    {
        a.values = nullptr;
    }

    Matrix(const Matrix & a) :
        rows(a.rows),
        cols(a.cols),
        values(new T[rows * cols])
    {
        std::copy(a.values, a.values + rows * cols, values);
    }

    Matrix() :
        rows(0),
        cols(0)
    {}

    Matrix(const size_t rows, const size_t cols) :
        rows(rows),
        cols(cols),
        values(new T[rows * cols])
    {}

    ~Matrix() {
        delete[] this->values;
    }

    inline T & at(const size_t i, const size_t j) const {
        return this->values[i * cols + j];
    }

    inline T & at(const size_t i, const size_t j, const T v) const {
        return this->values[i * cols + j] = v;
    }

    inline void copy_block(const size_t offset_row_dst, const size_t offset_col_dst, Matrix & src, const size_t rows, const size_t cols, const size_t offset_row_src, const size_t offset_col_src) {
        for (size_t i = 0; i < rows; i++) {
            for (size_t j = 0; j < cols; j++) {
                this->at(i+offset_row_dst, j+offset_col_dst) = src.at(i+offset_row_src, j+offset_col_src);
            }
        }
    }

    inline void swap(Matrix & m) {
        std::swap(this->values, m.values);
    }

    inline Matrix & operator=(const Matrix & b) {
        if (this != &b) {
            if (this->rows != b.rows || this->cols != b.cols) {
                this->rows = b.rows;
                this->cols = b.cols;

                if (this->values != nullptr) {
                    delete[] this->values;
                }
                this->values = new T[rows*cols];
            }
            std::copy(b.values, b.values+(b.rows*b.cols), this->values);
        }

        return *this;
    }

    inline Matrix & operator+(Matrix & b) {
        Matrix * res = new Matrix(this->rows, this->cols);

        #pragma omp parallel for
        for (size_t i = 0; i < this->rows; i++) {
            for (size_t j = 0; j < this->cols; j++) {
                res->at(i,j) = this->at(i,j) + b.at(i,j);
            }
        }

        return *res;
    }

    inline Matrix & operator-(Matrix & b) {
        Matrix * res = new Matrix(this->rows, this->cols);

        #pragma omp parallel for
        for (size_t i = 0; i < this->rows; i++) {
            for (size_t j = 0; j < this->cols; j++) {
                res->at(i,j) = this->at(i,j) - b.at(i,j);
            }
        }

        return *res;
    }

    inline Matrix & operator*(Matrix<T> & b) {
        Matrix<T> * res = new Matrix(this->rows, b.cols);

        #pragma omp parallel for
        for (size_t i = 0; i < this->rows; i++) {
            for (size_t k = 0; k < this->rows; k++) {
                for (size_t j = 0; j < b.cols; j++) {
                    res->at(i,j) += this->at(i,k) * b.at(k,j);
                }
            }
        }

        return *res;
    }

    inline std::ostream & operator<<(std::ostream& os) {
        for (size_t i = 0; i < this->rows; i++) {
            for (size_t j = 0; j < this->cols - 1; j++) {
                os << this->at(i,j) << ",";
            }
            os << this->at(i,this->cols-1) << '\n';
        }
        return os;
    }

    void write_file(const char * filename) {
        std::cout << "Printing matrix in file " << filename << "..." << '\n';
        std::ofstream myfile;
        myfile.open (filename);
        *this << myfile;
        myfile << '\n';
        myfile.close();
    }
};

template<class T>
inline std::ostream & operator<<(std::ostream& os, Matrix<T> & matrix) {
    for (size_t i = 0; i < matrix.rows; i++) {
        for (size_t j = 0; j < matrix.cols - 1; j++) {
            os << matrix.at(i,j) << ", ";
        }
        os << matrix.at(i,matrix.cols-1) << '\n';
    }
    return os;
}

template<class T>
class Diagonal: public Matrix<T>{
public:
    Diagonal(Diagonal const & a) = delete;

    Diagonal(const size_t dim, const T val) {
        this->rows = dim;
        this->cols = dim;
        this->values = new T[dim*dim]();

        for (size_t i = 0; i < dim; i++) {
            this->at(i,i) = val;
        }
    }
};

#endif
