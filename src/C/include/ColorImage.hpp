#ifndef COLORIMAGE
#define COLORIMAGE

#include "Image.hpp"

class ColorImage: public Image {
public:
    cv::Mat B;
    cv::Mat G;
    cv::Mat R;

private:
    static inline void fill_channels(const cv::Mat image, cv::Mat & B, cv::Mat & G, cv::Mat & R) {
        #pragma omp parallel for
        #pragma ivdep
        for (int i = 0; i < image.rows; i++) {
            #pragma ivdep
            for (int j = 0; j < image.cols; j++) {
                cv::Vec3f color = image.at<cv::Vec3f>(i,j); // BGR color format
                B.at<float>(i,j) = color(0);
                G.at<float>(i,j) = color(1);
                R.at<float>(i,j) = color(2);
            }
        }
    }

    static inline void fill_image(const cv::Mat & B, const cv::Mat & G, const cv::Mat & R, cv::Mat & image) {
        #pragma omp parallel for
        #pragma ivdep
        for (int i = 0; i < image.rows; i++) {
            #pragma ivdep
            for (int j = 0; j < image.cols; j++) {
                cv::Vec3f color;
                color(0) = B.at<float>(i,j);
                color(1) = G.at<float>(i,j);
                color(2) = R.at<float>(i,j);
                image.at<cv::Vec3f>(i,j) = color; // BGR color format
            }
        }
    }

public:
    ColorImage(const ColorImage & im) :
        Image(im),
        B(im.B.clone()),
        G(im.G.clone()),
        R(im.R.clone())
    {}

    ColorImage(const char * filename) {
        this->image = cv::imread(filename, CV_LOAD_IMAGE_COLOR);
        this->image.convertTo(this->image, CV_32F, 1.0 / UCHAR_MAX);

        if (!image.data) {
            std::cerr << "Could not open image " << filename << "." << '\n';
            return;
        }

        this->B = cv::Mat(this->image.size(), this->image.type());
        this->G = cv::Mat(this->image.size(), this->image.type());
        this->R = cv::Mat(this->image.size(), this->image.type());
        fill_channels(this->image, B, G, R);

        std::cout << "Image " << filename << " opened. Size: " << this->image.rows << " x " << this->image.cols << '\n';
    }

    template <class T>
    ColorImage(const T * __restrict__ B, const T * __restrict__ G, const T * __restrict__ R, const int rows, const int cols, const int type) {
        this->image = cv::Mat3d(rows, cols, CV_32FC3);
        this->B = cv::Mat(rows, cols, type);
        this->G = cv::Mat(rows, cols, type);
        this->R = cv::Mat(rows, cols, type);

        #pragma omp parallel for
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                this->B.at<T>(i,j) = B[i*cols + j];
                this->G.at<T>(i,j) = G[i*cols + j];
                this->R.at<T>(i,j) = R[i*cols + j];
                this->image.at<cv::Vec3f>(i,j) = cv::Vec3f(B[i*cols + j], G[i*cols + j], R[i*cols + j]);
            }
        }
    }

    ColorImage(const cv::Mat & image) :
        Image(image)
    {
        this->B = cv::Mat(this->image.size(), this->image.type());
        this->G = cv::Mat(this->image.size(), this->image.type());
        this->R = cv::Mat(this->image.size(), this->image.type());
        fill_channels(image, B, G, R);
    }

    ColorImage() {};

    // Add noise to an image
    void gaussian_noise(const double variance) {
        this->Image::gaussian_noise(variance);
        fill_channels(image, B, G, R);
    }

    template<class T>
    void denoise(const float lambda, const Method method, double &seconds) {
        // Clean noise
        auto start = std::chrono::system_clock::now();

        T * b = conjugate_gradient<T>(this->B, lambda, method, seconds);
        T * g = conjugate_gradient<T>(this->G, lambda, method, seconds);
        T * r = conjugate_gradient<T>(this->R, lambda, method, seconds);

        type2cv(b, this->B);
        type2cv(g, this->G);
        type2cv(r, this->R);

        delete[] b;
        delete[] g;
        delete[] r;

        fill_image(B, G, R, image);

        // End of denoising
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_time = end-start;
        seconds = elapsed_time.count();
    }
};

#endif
