#ifndef Vec3_CUDA_CUDA
#define Vec3_CUDA_CUDA

#include <cstdlib>
#include <algorithm>

#include <opencv2/opencv.hpp>
#include "Vec3.hpp"

template<class T>
class Vec3_CUDA {
public:
    T BGR[3];

public:
    __host__ __device__ Vec3_CUDA()
    {}

    __host__ __device__ Vec3_CUDA(T _B, T _G, T _R) {
        BGR[0] = _B;
        BGR[1] = _G;
        BGR[2] = _R;
    }

    __host__ __device__ Vec3_CUDA(const Vec3_CUDA & v) {
        BGR[0] = v.BGR[0];
        BGR[1] = v.BGR[1];
        BGR[2] = v.BGR[2];
    }

    __host__ __device__ Vec3_CUDA(const Vec3<T> & v) {
        BGR[0] = v.BGR[0];
        BGR[1] = v.BGR[1];
        BGR[2] = v.BGR[2];
    }

    __host__ __device__ Vec3_CUDA(const cv::Vec<T,3> & v) {
        BGR[0] = v[0];
        BGR[1] = v[1];
        BGR[2] = v[2];
    }

    __host__ __device__ Vec3_CUDA(const T & v) {
        BGR[0] = v;
        BGR[1] = v;
        BGR[2] = v;
    }

    __host__ __device__ ~Vec3_CUDA() {}

    __host__ __device__ inline const T & operator()(const size_t i) const {
        return BGR[i];
    }

    __host__ __device__ inline const T & operator()(const size_t i) {
        return BGR[i];
    }

    __host__ __device__ inline Vec3_CUDA & operator=(const Vec3_CUDA & v) {
        BGR[0] = v.BGR[0];
        BGR[1] = v.BGR[1];
        BGR[2] = v.BGR[2];
        return *this;
    }

    __host__ __device__ inline double norm() {
        return sqrt(BGR[0] * BGR[0] + BGR[1] * BGR[1] + BGR[2] * BGR[2]);
    }

    __host__ __device__ inline double norm2_pow2() {
        return BGR[0] * BGR[0] + BGR[1] * BGR[1] + BGR[2] * BGR[2];
    }

    __host__ __device__ inline double norm2(const Vec3_CUDA v) {
        return sqrt(BGR[0] * v.BGR[0] + BGR[1] * v.BGR[1] + BGR[2] * v.BGR[2]);
    }

    __host__ __device__ inline double norm2_pow2(const Vec3_CUDA v) {
        return BGR[0] * v.BGR[0] + BGR[1] * v.BGR[1] + BGR[2] * v.BGR[2];
    }
};

template<class T>
__host__ __device__ std::ostream & operator<<(std::ostream& os, const Vec3_CUDA<T> & v) {
    os << v.BGR[0] << ", " << v.BGR[1] << ", " << v.BGR[2];
    return os;
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> operator+(const Vec3_CUDA<T> & v1, const Vec3_CUDA<T> & v2) {
    return Vec3_CUDA<T>(v1.BGR[0] + v2.BGR[0], v1.BGR[1] + v2.BGR[1], v1.BGR[2] + v2.BGR[2]);
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> operator+(Vec3_CUDA<T> & v, const int & s) {
    return Vec3_CUDA<T>(v.BGR[0] + s, v.BGR[1] + s, v.BGR[2] + s);
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> operator+(Vec3_CUDA<T> & v, const float & s) {
    return Vec3_CUDA<T>(v.BGR[0] + s, v.BGR[1] + s, v.BGR[2] + s);
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> operator+(Vec3_CUDA<T> & v, const double & s) {
    return Vec3_CUDA<T>(v.BGR[0] + s, v.BGR[1] + s, v.BGR[2] + s);
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> operator+(const int & s, Vec3_CUDA<T> & v) {
    return v + s;
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> operator+(const float & s, Vec3_CUDA<T> & v) {
    return v + s;
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> operator+(const double & s, Vec3_CUDA<T> & v) {
    return v + s;
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> & operator+=(const Vec3_CUDA<T> & v, const int & s) {
    return v = v + s;
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> & operator+=(const Vec3_CUDA<T> & v, const float & s) {
    return v = v + s;
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> & operator+=(const Vec3_CUDA<T> & v, const double & s) {
    return v = v + s;
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> & operator+=(const int & s, const Vec3_CUDA<T> v) {
    return v + s;
}
template<class T>
__host__ __device__ inline Vec3_CUDA<T> & operator+=(const float & s, const Vec3_CUDA<T> v) {
    return v + s;
}
template<class T>
__host__ __device__ inline Vec3_CUDA<T> & operator+=(const double & s, const Vec3_CUDA<T> v) {
    return v + s;
}
template<class T>
__host__ __device__ inline Vec3_CUDA<T> & operator+=(Vec3_CUDA<T> & v1, const Vec3_CUDA<T> & v2) {
    return v1 = v1 + v2;
}


template<class T>
__host__ __device__ inline Vec3_CUDA<T> operator-(const Vec3_CUDA<T> & v1, const Vec3_CUDA<T> & v2) {
    return Vec3_CUDA<T>(v1.BGR[0] - v2.BGR[0], v1.BGR[1] - v2.BGR[1], v1.BGR[2] - v2.BGR[2]);
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> operator-(const Vec3_CUDA<T> & v, const int & s) {
    return Vec3_CUDA<T>(v.BGR[0] - s, v.BGR[1] - s, v.BGR[2] - s);
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> operator-(const Vec3_CUDA<T> & v, const float & s) {
    return Vec3_CUDA<T>(v.BGR[0] - s, v.BGR[1] - s, v.BGR[2] - s);
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> operator-(const Vec3_CUDA<T> & v, const double & s) {
    return Vec3_CUDA<T>(v.BGR[0] - s, v.BGR[1] - s, v.BGR[2] - s);
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> operator-(const int & s, const Vec3_CUDA<T> & v) {
    return v + s;
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> operator-(const float & s, const Vec3_CUDA<T> & v) {
    return v + s;
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> operator-(const double & s, const Vec3_CUDA<T> & v) {
    return v + s;
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> & operator-=(Vec3_CUDA<T> & v1, const Vec3_CUDA<T> & v2) {
    return v1 = v1 - v2;
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> & operator-=(Vec3_CUDA<T> & v, const int & s) {
    return v = v - s;
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> & operator-=(Vec3_CUDA<T> & v, const float & s) {
    return v = v - s;
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> & operator-=(Vec3_CUDA<T> & v, const double & s) {
    return v = v - s;
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> operator*(const Vec3_CUDA<T> & v1, const Vec3_CUDA<T> & v2) {
    return Vec3_CUDA<T>(v1.BGR[0] * v2.BGR[0], v1.BGR[1] * v2.BGR[1], v1.BGR[2] * v2.BGR[2]);
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> operator*(const Vec3_CUDA<T> & v, const int & s) {
    return Vec3_CUDA<T>(v.BGR[0] * s, v.BGR[1] * s, v.BGR[2] * s);
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> operator*(const Vec3_CUDA<T> & v, const float & s) {
    return Vec3_CUDA<T>(v.BGR[0] * s, v.BGR[1] * s, v.BGR[2] * s);
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> operator*(const Vec3_CUDA<T> & v, const double & s) {
    return Vec3_CUDA<T>(v.BGR[0] * s, v.BGR[1] * s, v.BGR[2] * s);
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> operator*(const int & s, const Vec3_CUDA<T> & v) {
    return v * s;
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> operator*(const float & s, const Vec3_CUDA<T> & v) {
    return v * s;
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> operator*(const double & s, const Vec3_CUDA<T> & v) {
    return v * s;
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> & operator*=(Vec3_CUDA<T> & v1, const Vec3_CUDA<T> & v2) {
    return v1 = v1 * v2;
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> & operator*=(Vec3_CUDA<T> & v, const int & s) {
    return v = v * s;
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> & operator*=(Vec3_CUDA<T> & v, const float & s) {
    return v = v * s;
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> & operator*=(Vec3_CUDA<T> & v, const double & s) {
    return v = v * s;
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> operator/(const Vec3_CUDA<T> & v1, const Vec3_CUDA<T> & v2) {
    return Vec3_CUDA<T>(v1.BGR[0] / v2.BGR[0], v1.BGR[1] / v2.BGR[1], v1.BGR[2] / v2.BGR[2]);
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> operator/(const Vec3_CUDA<T> & v, const int & s) {
    return Vec3_CUDA<T>(v.BGR[0] / s, v.BGR[1] / s, v.BGR[2] / s);
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> operator/(const Vec3_CUDA<T> & v, const float & s) {
    return Vec3_CUDA<T>(v.BGR[0] / s, v.BGR[1] / s, v.BGR[2] / s);
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> operator/(const Vec3_CUDA<T> & v, const double & s) {
    return Vec3_CUDA<T>(v.BGR[0] / s, v.BGR[1] / s, v.BGR[2] / s);
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> operator/(const int & s, const Vec3_CUDA<T> & v) {
    return v / s;
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> operator/(const float & s, const Vec3_CUDA<T> & v) {
    return v / s;
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> operator/(const double & s, const Vec3_CUDA<T> & v) {
    return v / s;
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> & operator/=(Vec3_CUDA<T> & v, const int & s) {
    return v = v / s;
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> & operator/=(Vec3_CUDA<T> & v, const float & s) {
    return v = v / s;
}

template<class T>
__host__ __device__ inline Vec3_CUDA<T> & operator/=(Vec3_CUDA<T> & v, const double & s) {
    return v = v / s;
}
#endif
