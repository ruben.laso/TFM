#ifndef MATH_CUDA
#define MATH_CUDA

#include <cstdlib>
#include "BilapSparse.hpp"
#include "Matrix.hpp"
#include "Vec3.hpp"

void CUDA_warmup();

float * solve_system_CG_CUDA(BilapSparse<float> & A, const float * b);

void chromaticity_anisotropic_CUDA(Matrix<Vec3<float>> & a, const float tolerance, const size_t max_iter);

void chromaticity_isotropic_CUDA(Matrix<Vec3<float>> & a, const float tolerance, const size_t max_iter);

void brightness_anisotropic_CUDA(Matrix<float> & a, const float tolerance, const size_t max_iter);

void brightness_laplacian_CUDA(Matrix<float> & a, const float tolerance, const size_t max_iter);

#endif
