#ifndef Matrix_CUDA_CUDA
#define Matrix_CUDA_CUDA

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>
#ifdef ENABLE_OPENMP
#include <omp.h>
#endif

template<class T>
class Matrix_CUDA {
public:
    size_t rows;
    size_t cols;
    T * values = nullptr;

    __host__ __device__ Matrix_CUDA(Matrix_CUDA && a) :
        rows(a.rows),
        cols(a.cols),
        values(a.values)
    {
        a.values = nullptr;
    }

    __host__ __device__ Matrix_CUDA(const Matrix_CUDA & a) :
        rows(a.rows),
        cols(a.cols),
        values(a.values)
    {
        // std::copy(a.values, a.values + rows * cols, values);
    }

    __host__ __device__ Matrix_CUDA() :
        rows(0),
        cols(0)
    {}

    __host__ __device__ Matrix_CUDA(const size_t rows, const size_t cols) :
        rows(rows),
        cols(cols),
        values(new T[rows * cols])
    {}

    __host__ __device__ Matrix_CUDA(const size_t rows, const size_t cols, T * values) :
        rows(rows),
        cols(cols),
        values(values)
    {}

    __host__ __device__ ~Matrix_CUDA() {
        // delete[] this->values;
    }

    __host__ __device__ inline T & at(const size_t i, const size_t j) const {
        return this->values[i * cols + j];
    }

    __host__ __device__ inline T & at(const size_t i, const size_t j, const T v) const {
        return this->values[i * cols + j] = v;
    }

    __host__ __device__ inline void copy_block(const size_t offset_row_dst, const size_t offset_col_dst, Matrix_CUDA & src, const size_t rows, const size_t cols, const size_t offset_row_src, const size_t offset_col_src) {
        for (size_t i = 0; i < rows; i++) {
            for (size_t j = 0; j < cols; j++) {
                this->at(i+offset_row_dst, j+offset_col_dst) = src.at(i+offset_row_src, j+offset_col_src);
            }
        }
    }

    __host__ __device__ inline void swap(Matrix_CUDA & m) {
        // std::swap(this->values, m.values);
        T * aux = this->values;
        this->values = m.values;
        m.values = aux;
    }

    __host__ __device__ inline Matrix_CUDA & operator=(const Matrix_CUDA & b) {
        if (this != &b) {
            if (this->rows != b.rows || this->cols != b.cols) {
                this->rows = b.rows;
                this->cols = b.cols;

                if (this->values != nullptr) {
                    delete[] this->values;
                }
                this->values = new T[rows*cols];
            }
            std::copy(b.values, b.values+(b.rows*b.cols), this->values);
        }

        return *this;
    }

    __host__ __device__ inline Matrix_CUDA & operator+(Matrix_CUDA & b) {
        Matrix_CUDA * res = new Matrix_CUDA(this->rows, this->cols);

        #pragma omp parallel for
        for (size_t i = 0; i < this->rows; i++) {
            for (size_t j = 0; j < this->cols; j++) {
                res->at(i,j) = this->at(i,j) + b.at(i,j);
            }
        }

        return *res;
    }

    __host__ __device__ inline Matrix_CUDA & operator-(Matrix_CUDA & b) {
        Matrix_CUDA * res = new Matrix_CUDA(this->rows, this->cols);

        #pragma omp parallel for
        for (size_t i = 0; i < this->rows; i++) {
            for (size_t j = 0; j < this->cols; j++) {
                res->at(i,j) = this->at(i,j) - b.at(i,j);
            }
        }

        return *res;
    }

    __host__ __device__ inline Matrix_CUDA & operator*(Matrix_CUDA<T> & b) {
        Matrix_CUDA<T> * res = new Matrix_CUDA(this->rows, b.cols);

        #pragma omp parallel for
        for (size_t i = 0; i < this->rows; i++) {
            for (size_t k = 0; k < this->rows; k++) {
                for (size_t j = 0; j < b.cols; j++) {
                    res->at(i,j) += this->at(i,k) * b.at(k,j);
                }
            }
        }

        return *res;
    }

    __host__ __device__ inline std::ostream & operator<<(std::ostream& os) {
        for (size_t i = 0; i < this->rows; i++) {
            for (size_t j = 0; j < this->cols - 1; j++) {
                os << this->at(i,j) << ",";
            }
            os << this->at(i,this->cols-1) << '\n';
        }
        return os;
    }

    void write_file(const char * filename) {
        std::cout << "Printing Matrix_CUDA in file " << filename << "..." << '\n';
        std::ofstream myfile;
        myfile.open (filename);
        *this << myfile;
        myfile << '\n';
        myfile.close();
    }
};

template<class T>
__host__ __device__ inline std::ostream & operator<<(std::ostream& os, Matrix_CUDA<T> & Matrix_CUDA) {
    for (size_t i = 0; i < Matrix_CUDA.rows; i++) {
        for (size_t j = 0; j < Matrix_CUDA.cols - 1; j++) {
            os << Matrix_CUDA.at(i,j) << ", ";
        }
        os << Matrix_CUDA.at(i,Matrix_CUDA.cols-1) << '\n';
    }
    return os;
}

#endif
