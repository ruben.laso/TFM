#ifndef BILAPSPARSE_CUDA
#define BILAPSPARSE_CUDA

#include "BilapSparse.hpp"

template<class T>
class BilapSparse_CUDA {
public:
    size_t rows;
    size_t cols;
    size_t dim;
    size_t block_dim;
    size_t num_blocks;
    T c1;
    T c1_1;
    T c1_2;
    T c2;
    T c3;
    T c4;
    T c5;
    T c6;
    const size_t max_nz = 13;

public:
    __host__ __device__ BilapSparse_CUDA(BilapSparse_CUDA && a) :
        rows(a.rows),
        cols(a.cols),
        dim(a.dim),
        block_dim(a.block_dim),
        num_blocks(a.num_blocks),
        c1(a.c1),
        c1_1(a.c1_1),
        c1_2(a.c1_2),
        c2(a.c2),
        c3(a.c3),
        c4(a.c4),
        c5(a.c5),
        c6(a.c6)
    { }

    __host__ __device__ BilapSparse_CUDA(const BilapSparse_CUDA & a) :
        rows(a.rows),
        cols(a.cols),
        dim(a.dim),
        block_dim(a.block_dim),
        num_blocks(a.num_blocks),
        c1(a.c1),
        c1_1(a.c1_1),
        c1_2(a.c1_2),
        c2(a.c2),
        c3(a.c3),
        c4(a.c4),
        c5(a.c5),
        c6(a.c6)
    { }

    __host__ __device__ BilapSparse_CUDA(const BilapSparse<T> & a) :
        rows(a.rows),
        cols(a.cols),
        dim(a.dim),
        block_dim(a.block_dim),
        num_blocks(a.num_blocks),
        c1(a.c1),
        c1_1(a.c1_1),
        c1_2(a.c1_2),
        c2(a.c2),
        c3(a.c3),
        c4(a.c4),
        c5(a.c5),
        c6(a.c6)
    { }

    __host__ __device__ BilapSparse_CUDA() :
        rows(0),
        cols(0),
        dim(0),
        block_dim(0),
        num_blocks(0),
        c1(20+1),
        c1_1(21+1),
        c1_2(22+1),
        c2(-8),
        c3(1),
        c4(-8),
        c5(2),
        c6(1)
    { }

    __host__ __device__ BilapSparse_CUDA(const T lambda) :
        rows(0),
        cols(0),
        dim(0),
        block_dim(0),
        num_blocks(0),
        c1(lambda*20+1),
        c1_1(lambda*21+1),
        c1_2(lambda*22+1),
        c2(lambda*-8),
        c3(lambda*1),
        c4(lambda*-8),
        c5(lambda*2),
        c6(lambda*1)
    { }

    __host__ __device__ BilapSparse_CUDA(const size_t size, const size_t block_size) :
        rows(size),
        cols(size),
        dim(size),
        block_dim(block_size),
        num_blocks(size / block_size),
        c1(20+1),
        c1_1(21+1),
        c1_2(22+1),
        c2(-8),
        c3(1),
        c4(-8),
        c5(2),
        c6(1)
    { }

    __host__ __device__ BilapSparse_CUDA(const T lambda, const size_t size, const size_t block_size) :
        rows(size),
        cols(size),
        dim(size),
        block_dim(block_size),
        num_blocks(size / block_size),
        c1(lambda*20+1),
        c1_1(lambda*21+1),
        c1_2(lambda*22+1),
        c2(lambda*-8),
        c3(lambda*1),
        c4(lambda*-8),
        c5(lambda*2),
        c6(lambda*1)
    { }

    __host__ __device__ ~BilapSparse_CUDA() { }
};


#endif
