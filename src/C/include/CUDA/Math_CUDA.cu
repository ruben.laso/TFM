#include <cuda_runtime_api.h>
#include <cuda.h>

#include <thrust/transform_reduce.h>
#include <thrust/inner_product.h>
#include <thrust/device_vector.h>

#include <iostream>
#include <cstdlib>
#include <chrono>

#include "Matrix.hpp"
#include "Vec3.hpp"
#include "Matrix_CUDA.hpp"
#include "Vec3_CUDA.hpp"
#include "BilapSparse.hpp"
#include "BilapSparse_CUDA.hpp"

/**
 * This macro checks return value of the CUDA runtime call and exits
 * the application if the call failed.
 */
#define CUDA_CHECK_RETURN(value) {                                  \
    cudaError_t _m_cudaStat = value;                                \
    if (_m_cudaStat != cudaSuccess) {                               \
        fprintf(stderr, "Error %s at line %d in file %s\n",         \
            cudaGetErrorString(_m_cudaStat), __LINE__, __FILE__);   \
        exit(EXIT_FAILURE);                                         \
    }                                                               \
}

const size_t MAX_ITER = 100;
const double TOLERANCE = 1e-4;

const size_t BLOCK_SIZE = 32; // BLOCK_SIZE cannot be greater than 32 -> 32 * 32 = 1024 (limit of a block)

__global__ void offset(int * a) {
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    if (i == 0) {
        a[i] = 0;
    }
}

void CUDA_warmup() {
    std::cout << "Executing CUDA warmup..." << '\n';
    int * d_a;
    CUDA_CHECK_RETURN(cudaMalloc(&d_a, 1 * sizeof(int)));
    offset<<<1,1>>>(d_a);
    CUDA_CHECK_RETURN(cudaFree(d_a));
}

template<class T>
__global__ void prod_vec_bilap_sparse(const BilapSparse_CUDA<T> A, const T * __restrict__ v, T * __restrict__ res) {
    const auto num_blocks = A.num_blocks;
    const auto n  = A.block_dim;
    const auto ii = blockIdx.x * blockDim.x + threadIdx.x;
    const auto i  = ii % n;
    const auto NN = ii / n;
    const auto N  = NN - 2;

    if (ii >= A.dim) {
        return;
    }
    else if (NN == 0) { // First block
        if (i == 0) { // First rows
            res[ii] = A.c1_2*v[0] + A.c2*v[1] + A.c3*v[2] +
                A.c4*v[n] + A.c5*v[n+1] +
                A.c6*v[2*n];
        }
        else if (i == 1) { // Second row
            res[ii] = A.c2*v[0] + A.c1_1*v[1] + A.c2*v[2] + A.c3*v[3] +
                A.c5*v[n] + A.c4*v[n+1] + A.c5*v[n+2] +
                A.c6*v[2*n+1];
        }
        else if (i > 1 && i < n-2) { // Mid rows of first block
            res[ii] = A.c3*v[i-2] + A.c2*v[i-1] + A.c1_1*v[i] + A.c2*v[i+1] + A.c3*v[i+2] +
                A.c5*v[n+i-1] + A.c4*v[n+i] + A.c5*v[n+i+1] +
                A.c6*v[2*n+i];
        }
        else if (i == n-2) { // Penultimate rows of first block
            res[ii] = A.c3*v[i-2] + A.c2*v[i-1] + A.c1_1*v[i] + A.c2*v[i+1] +
                A.c5*v[n+i-1] + A.c4*v[n+i] + A.c5*v[n+i+1] +
                A.c6*v[2*n+i];
        }
        else if (i == n-1) { // Last row of first block
            res[ii] = A.c3*v[i-2] + A.c2*v[i-1] + A.c1_2*v[i] +
                A.c5*v[n+i-1] + A.c4*v[n+i] +
                A.c6*v[2*n+i];
        }
    }
    else if (NN == 1) { // Second block
        if (i == 0) { // First row of second block
            res[ii] = A.c4*v[0] + A.c5*v[1] +
                A.c1_1*v[n] + A.c2*v[n+1]   + A.c3*v[n+2] +
                A.c4*v[2*n] + A.c5*v[2*n+1] +
                A.c6*v[3*n];
        }
        else if (i == 1) { // Second row of second block
            res[ii] = A.c5*v[0] + A.c4*v[1] + A.c5*v[2] +
                A.c2*v[n]   + A.c1*v[n+1]   + A.c2*v[n+2]   + A.c3*v[n+3] +
                A.c5*v[2*n] + A.c4*v[2*n+1] + A.c5*v[2*n+2] +
                A.c6*v[3*n];
        }
        else if (i > 1 && i < n-2) { // Mid rows of second block
            res[ii] = A.c5*v[i-1] + A.c4*v[i]  + A.c5*v[i+1]     +
                A.c3*v[n+i-2]   + A.c2*v[n+i-1] + A.c1*v[n+i]     + A.c2*v[n+i+1] + A.c3*v[n+i+2] +
                A.c5*v[2*n+i-1] + A.c4*v[2*n+i] + A.c5*v[2*n+i+1] +
                A.c6*v[3*n+i];
        }
        else if (i == n-2) { // Penultimate row of second block
            res[ii] = A.c5*v[i-1] + A.c4*v[i]  + A.c5*v[i+1] +
                A.c3*v[n+i-2]   + A.c2*v[n+i-1] + A.c1*v[n+i]     + A.c2*v[n+i+1] +
                A.c5*v[2*n+i-1] + A.c4*v[2*n+i] + A.c5*v[2*n+i+1] +
                A.c6*v[3*n+i];
        }
        else if (i == n-1) { // Last row of second block
            res[ii] = A.c5*v[i-1] + A.c4*v[i]  +
                A.c3*v[n+i-2]   + A.c2*v[n+i-1] + A.c1_1*v[n+i] +
                A.c5*v[2*n+i-1] + A.c4*v[2*n+i] +
                A.c6*v[3*n+i];
        }
    }
    else if (NN > 1 && NN < num_blocks - 2) { // Mid blocks
        if (i == 0) { // First row of mid block
            res[ii] = A.c6*v[N*n] +
                A.c4*v[N*n+n]     + A.c5*v[N*n+n+1]   +
                A.c1_1*v[N*n+2*n] + A.c2*v[N*n+2*n+1] + A.c3*v[N*n+2*n+2] +
                A.c4*v[N*n+3*n]   + A.c5*v[N*n+3*n+1] +
                A.c6*v[N*n+4*n];
        }
        else if (i == 1) { // Second row of mid block
            res[ii] = A.c6*v[N*n+1] +
                A.c5*v[N*n+n]   + A.c4*v[N*n+n+1]   + A.c5*v[N*n+n+2]   +
                A.c2*v[N*n+2*n] + A.c1*v[N*n+2*n+1] + A.c2*v[N*n+2*n+2] + A.c3*v[N*n+2*n+3] +
                A.c5*v[N*n+3*n] + A.c4*v[N*n+3*n+1] + A.c5*v[N*n+3*n+2] +
                A.c6*v[N*n+4*n+1];
        }
        else if (i > 1 && i < n-2) { // Mid rows of mid block
            res[ii] = A.c6*v[N*n+i] +
                A.c5*v[N*n+n+i-1]   + A.c4*v[N*n+n+i]     + A.c5*v[N*n+n+i+1]   +
                A.c3*v[N*n+2*n+i-2] + A.c2*v[N*n+2*n+i-1] + A.c1*v[N*n+2*n+i]   + A.c2*v[N*n+2*n+i+1] + A.c3*v[N*n+2*n+i+2] +
                A.c5*v[N*n+3*n+i-1] + A.c4*v[N*n+3*n+i]   + A.c5*v[N*n+3*n+i+1] +
                A.c6*v[N*n+4*n+i];
        }
        else if (i == n-2) { // Penultimate row of mid block
            res[ii] = A.c6*v[N*n+i] +
                A.c5*v[N*n+n+i-1]   + A.c4*v[N*n+n+i]     + A.c5*v[N*n+n+i+1]   +
                A.c3*v[N*n+2*n+i-2] + A.c2*v[N*n+2*n+i-1] + A.c1*v[N*n+2*n+i]   + A.c2*v[N*n+2*n+i+1] +
                A.c5*v[N*n+3*n+i-1] + A.c4*v[N*n+3*n+i]   + A.c5*v[N*n+3*n+i+1] +
                A.c6*v[N*n+4*n+i];
        }
        else if (i == n-1) { // Last row of mid block
            res[ii] = A.c6*v[N*n+i] +
                A.c5*v[N*n+n+i-1]   + A.c4*v[N*n+n+i]     +
                A.c3*v[N*n+2*n+i-2] + A.c2*v[N*n+2*n+i-1] + A.c1_1*v[N*n+2*n+i] +
                A.c5*v[N*n+3*n+i-1] + A.c4*v[N*n+3*n+i]   +
                A.c6*v[N*n+4*n+i];
        }
    }
    else if (NN == num_blocks - 2) { // Penultimate block
        if (i == 0) { // First row of penultimate block
            res[ii] = A.c6*v[N*n+0] +
                A.c4*v[N*n+n]     + A.c5*v[N*n+n+1]   +
                A.c1_1*v[N*n+2*n] + A.c2*v[N*n+2*n+1] + A.c3*v[N*n+2*n+2] +
                A.c4*v[N*n+3*n]   + A.c5*v[N*n+3*n+1];
        }
        else if (i == 1) { // Second row of penultimate block
            res[ii] = A.c6*v[N*n+1] +
                A.c5*v[N*n+n]   + A.c4*v[N*n+n+1]   + A.c5*v[N*n+n+2]   +
                A.c2*v[N*n+2*n] + A.c1*v[N*n+2*n+1] + A.c2*v[N*n+2*n+2] + A.c3*v[N*n+2*n+3] +
                A.c5*v[N*n+3*n] + A.c4*v[N*n+3*n+1] + A.c5*v[N*n+3*n+2];
        }
        else if (i > 1 && i < n-2) { // Mid rows of penultimate block
            res[ii] = A.c6*v[N*n+i] +
                A.c5*v[N*n+n+i-1]   + A.c4*v[N*n+n+i]     + A.c5*v[N*n+n+i+1] +
                A.c3*v[N*n+2*n+i-2] + A.c2*v[N*n+2*n+i-1] + A.c1*v[N*n+2*n+i] + A.c2*v[N*n+2*n+i+1] + A.c3*v[N*n+2*n+i+2] +
                A.c5*v[N*n+3*n+i-1] + A.c4*v[N*n+3*n+i]   + A.c5*v[N*n+3*n+i+1];
        }
        else if (i == n-2) { // Penultimate row of penultimate block
            res[ii] = A.c6*v[N*n+i] +
                A.c5*v[N*n+n+i-1]   + A.c4*v[N*n+n+i]     + A.c5*v[N*n+n+i+1] +
                A.c3*v[N*n+2*n+i-2] + A.c2*v[N*n+2*n+i-1] + A.c1*v[N*n+2*n+i] + A.c2*v[N*n+2*n+i+1] +
                A.c5*v[N*n+3*n+i-1] + A.c4*v[N*n+3*n+i]   + A.c5*v[N*n+3*n+i+1];
        }
        else if (i == n-1) { // Last row of mid block
            res[ii] = A.c6*v[N*n+i] +
                A.c5*v[N*n+n+i-1]   + A.c4*v[N*n+n+i]     + A.c5*v[N*n+n+i+1] +
                A.c3*v[N*n+2*n+i-2] + A.c2*v[N*n+2*n+i-1] + A.c1*v[N*n+2*n+i] +
                A.c5*v[N*n+3*n+i-1] + A.c4*v[N*n+3*n+i];
        }
    }
    else if (NN == num_blocks - 1) { // Last block
        if (i == 0) { // First row of last block
            res[ii] = A.c6*v[N*n+0] +
                A.c4*v[N*n+n]     + A.c5*v[N*n+n+1]   +
                A.c1_2*v[N*n+2*n] + A.c2*v[N*n+2*n+1] + A.c3*v[N*n+2*n+2];
        }
        else if (i == 1) { // Second row of last block
            res[ii] = A.c6*v[N*n+1] +
                A.c5*v[N*n+n]   + A.c4*v[N*n+n+1]     + A.c5*v[N*n+n+2] +
                A.c2*v[N*n+2*n] + A.c1_1*v[N*n+2*n+1] + A.c2*v[N*n+2*n+2] + A.c3*v[N*n+2*n+3];
        }
        else if (i > 1 && i < n-2) { // Mid rows of last block
            res[ii] = A.c6*v[N*n+i] +
                A.c5*v[N*n+n+i-1]   + A.c4*v[N*n+n+i]     + A.c5*v[N*n+n+i+1]   +
                A.c3*v[N*n+2*n+i-2] + A.c2*v[N*n+2*n+i-1] + A.c1_1*v[N*n+2*n+i] + A.c2*v[N*n+2*n+i+1] + A.c3*v[N*n+2*n+i+2];
        }
        else if (i == n-2) { // Penultimate row of last block
            res[ii] = A.c6*v[N*n+i] +
                A.c5*v[N*n+n+i-1]   + A.c4*v[N*n+n+i]     + A.c5*v[N*n+n+i+1]   +
                A.c3*v[N*n+2*n+i-2] + A.c2*v[N*n+2*n+i-1] + A.c1_1*v[N*n+2*n+i] + A.c2*v[N*n+2*n+i+1];
        }
        else if (i == n-1) { // Last row of last block
            res[ii] = A.c6*v[N*n+i] +
                A.c5*v[N*n+n+i-1]   + A.c4*v[N*n+n+i]     +
                A.c3*v[N*n+2*n+i-2] + A.c2*v[N*n+2*n+i-1] + A.c1_1*v[N*n+2*n+i];
        }
    }
}

template<class T>
__global__ void saxpy(const T a, const T * __restrict__ x, T * __restrict__ y, const size_t n) {
    const auto i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < n) {
        y[i] = a * x[i] + y[i];
    }
}

template<class T>
__global__ void saxpy(T * __restrict__ r, const T a, const T * __restrict__ x, const T * __restrict__ y, const size_t n) {
    const auto i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < n) {
        r[i] = a * x[i] + y[i];
    }
}

template<class T>
inline T dot_prod_GPU(T * __restrict__ a, T * __restrict__ b, const size_t n) {
    thrust::device_ptr<T> dev_a(a);
    thrust::device_ptr<T> dev_b(b);
    return thrust::inner_product(dev_a, dev_a + n, dev_b, 0.0f);
}

template<class T>
inline T dot_prod_GPU(T * a, const size_t n) {
    thrust::device_ptr<T> dev_a(a);
    return thrust::inner_product(dev_a, dev_a + n, dev_a, 0.0f);
}

template<class T>
inline T dot_prod(const T * __restrict__ a, const T * __restrict__ b, const size_t n) {
    T res = 0;
    #pragma omp parallel for reduction(+:res)
    for (size_t i = 0; i < n; i++) {
        res += a[i] * b[i];
    }
    return res;
}

template<class T>
inline T dot_prod(const T * a, const size_t n) {
    T res = 0;
    #pragma omp parallel for reduction(+:res)
    for (size_t i = 0; i < n; i++) {
        res += a[i] * a[i];
    }
    return res;
}

float * solve_system_CG_CUDA(BilapSparse<float> & A_host, const float * b) {
    using T = float;
    std::cout << "Solving system with Conjugate Gradient..." << '\n';

    auto start = std::chrono::system_clock::now();

    double matvec_time = 0;
    double saxpy_time = 0;
    double dot_time = 0;

    BilapSparse_CUDA<T> A(A_host);

    const size_t n = A.dim;
    const double threshold = TOLERANCE * sqrt(dot_prod(b, n));

    const dim3 dimBlock(BLOCK_SIZE * BLOCK_SIZE);
    const dim3 dimGrid(ceil(n / dimBlock.x));

    T * x_cuda = nullptr;
    T * r_cuda = nullptr;
    T * p_cuda = nullptr;
    T * ap_cuda = nullptr;

    CUDA_CHECK_RETURN(cudaMalloc(&x_cuda, n * sizeof(T)));
    CUDA_CHECK_RETURN(cudaMalloc(&r_cuda, n * sizeof(T)));
    CUDA_CHECK_RETURN(cudaMalloc(&p_cuda, n * sizeof(T)));
    CUDA_CHECK_RETURN(cudaMalloc(&ap_cuda, n * sizeof(T)));

    // Initial guess -> x = b;
    CUDA_CHECK_RETURN(cudaMemcpy(x_cuda, b, n * sizeof(T), cudaMemcpyHostToDevice));

    // ap = A * x
    prod_vec_bilap_sparse<T><<<dimGrid, dimBlock>>>(A, x_cuda, ap_cuda);

    // r = b - ap = -ap + b = -ap + x;
    saxpy<T><<<dimGrid, dimBlock>>>(r_cuda, -1.0, ap_cuda, x_cuda, n);
    // p = r;
    CUDA_CHECK_RETURN(cudaMemcpy(p_cuda, r_cuda, n * sizeof(T), cudaMemcpyDeviceToDevice));

    // T rsold = dot_prod(r, n);
    T rsold = dot_prod_GPU(r_cuda, n);

    if (sqrt(rsold) < threshold) {
        T * x = new T[n];
        CUDA_CHECK_RETURN(cudaMemcpy(x, x_cuda, n * sizeof(T), cudaMemcpyDeviceToHost));
        return x;
    }

    size_t i;
    // Loop solving the system
    for (i = 0; i < MAX_ITER; i++) {
        auto start = std::chrono::system_clock::now();
        // ap = A * p;
        prod_vec_bilap_sparse<T><<<dimGrid, dimBlock>>>(A, p_cuda, ap_cuda);
        cudaDeviceSynchronize();
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_time = end-start;
        matvec_time += elapsed_time.count();

        start = std::chrono::system_clock::now();
        T alpha = rsold / dot_prod_GPU(p_cuda, ap_cuda, n);
        end = std::chrono::system_clock::now();
        elapsed_time = end-start;
        dot_time += elapsed_time.count();

        start = std::chrono::system_clock::now();
        // x = alpha * p + x;
        saxpy<T><<<dimGrid, dimBlock>>>(alpha, p_cuda, x_cuda, n);
        // r = r - alpha * ap = -alpha * ap + r;
        saxpy<T><<<dimGrid, dimBlock>>>(-alpha, ap_cuda, r_cuda, n);
        cudaDeviceSynchronize();
        end = std::chrono::system_clock::now();
        elapsed_time = end-start;
        saxpy_time += elapsed_time.count();

        start = std::chrono::system_clock::now();
        // rsnew = r * r;
        T rsnew = dot_prod_GPU(r_cuda, n);
        end = std::chrono::system_clock::now();
        elapsed_time = end-start;
        dot_time += elapsed_time.count();

        if (sqrt(rsnew) < threshold) {
            std::cout << "Convergence found in " << i << " iterations." << '\n';
            break;
        }

        T beta = rsnew / rsold;

        start = std::chrono::system_clock::now();
        // p = beta * p + r;
        saxpy<T><<<dimGrid, dimBlock>>>(p_cuda, beta, p_cuda, r_cuda, n);
        end = std::chrono::system_clock::now();
        elapsed_time = end-start;
        saxpy_time += elapsed_time.count();

        rsold = rsnew;
    }
    T * x = new T[n];
    CUDA_CHECK_RETURN(cudaMemcpy(x, x_cuda, n * sizeof(T), cudaMemcpyDeviceToHost));

    CUDA_CHECK_RETURN(cudaFree(x_cuda));
    CUDA_CHECK_RETURN(cudaFree(r_cuda));
    CUDA_CHECK_RETURN(cudaFree(p_cuda));
    CUDA_CHECK_RETURN(cudaFree(ap_cuda));

    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_time = end-start;
    double seconds = elapsed_time.count();
    std::cout << "Conjugate Gradient time: " << seconds << " seconds" << '\n';
    std::cout << "\tTime per iteration: " << seconds / i << " seconds" << '\n';
    std::cout << "\tTime mat-vec product: " << matvec_time << " (" << matvec_time / i << ")" << " seconds" << '\n';
    std::cout << "\tTime dot product: " << dot_time << " (" << dot_time / i << ")" << " seconds" << '\n';
    std::cout << "\tTime saxpy: " << saxpy_time << " (" << saxpy_time / i << ")"  << " seconds" << '\n';

    std::cout << "Number of iterations: " << i << '\n';

    #ifdef MEASURE
    std::cout << "Total;Iterations;Total_per_it" << '\n';
    std::cout << seconds << ";" << i << ";" << seconds / i << '\n';
    #endif

    return x;
}

template<class M, class T>
__device__ __forceinline__ T dx_c(const M & a, const size_t i, const size_t j) {
    return (a.at(i,j+1) - a.at(i,j-1)) * 0.5;
}

template<class M, class T>
__device__ __forceinline__ T dy_c(const M & a, const size_t i, const size_t j) {
    return (a.at(i+1,j) - a.at(i-1,j)) * 0.5;
}

template<class M, class T>
__device__ __forceinline__ T dx_f(const M & a, const size_t i, const size_t j) {
    return a.at(i,j+1) - a.at(i,j);
}

template<class M, class T>
__device__ __forceinline__ T dy_f(const M & a, const size_t i, const size_t j) {
    return a.at(i+1,j) - a.at(i,j);
}

template<class M, class T>
__device__ __forceinline__ T dx_b(const M & a, const size_t i, const size_t j) {
    return a.at(i,j) - a.at(i,j-1);
}

template<class M, class T>
__device__ __forceinline__ T dy_b(const M & a, const size_t i, const size_t j) {
    return a.at(i,j) - a.at(i-1,j);
}

template<class M, class T>
__device__ __forceinline__ T laplacian(const M & a, const size_t i, const size_t j) {
    // 5 point scheme
    return  (a.at(i-1,j) +
            a.at(i,j-1) -
            4 * a.at(i,j) +
            a.at(i+1,j) +
            a.at(i,j+1));
}

template<class T>
__device__ __forceinline__ float norm2_pow2(const Vec3<T> & dx, const Vec3<T> & dy) {
    return dx(0)*dx(0) + dy(0)*dy(0) + dx(1)*dx(1) + dy(1)*dy(1) + dx(2)*dx(2) + dy(2)*dy(2);
}

template<class T>
__device__ __forceinline__ float norm2_pow2(const Vec3_CUDA<T> & dx, const Vec3_CUDA<T> & dy) {
    return dx(0)*dx(0) + dy(0)*dy(0) + dx(1)*dx(1) + dy(1)*dy(1) + dx(2)*dx(2) + dy(2)*dy(2);
}

template<class T>
__device__ __forceinline__ float norm2_pow2(const T & dx, const T & dy) {
    return (dx*dx + dy*dy);
}

template<class T>
__device__ __forceinline__ float norm2(const Vec3<T> & dx, const Vec3<T> & dy) {
    return sqrt(dx(0)*dx(0) + dy(0)*dy(0) + dx(1)*dx(1) + dy(1)*dy(1) + dx(2)*dx(2) + dy(2)*dy(2));
}

template<class T>
__device__ __forceinline__ float norm2(const Vec3_CUDA<T> & dx, const Vec3_CUDA<T> & dy) {
    return sqrt(dx(0)*dx(0) + dy(0)*dy(0) + dx(1)*dx(1) + dy(1)*dy(1) + dx(2)*dx(2) + dy(2)*dy(2));
}

template<class T>
__device__ __forceinline__ float norm2(const T & v) {
    return sqrt(v(0)*v(0) + v(1)*v(1) + v(2)*v(2));
}

template<class T>
__device__ __forceinline__ float norm2(const T & dx, const T & dy) {
    return sqrt(dx*dx + dy*dy);
}

template<class M, class VT>
__device__ __forceinline__ float gradient_norm(const M & a, const size_t i, const size_t j) {
    return norm2(dx_c<M,VT>(a, i, j), dy_c<M,VT>(a, i, j));
}

template<class M, class T, class VT>
__global__ void energy_CUDA(const M a, T * e) {
    const auto i = blockIdx.y * blockDim.y + threadIdx.y;
    const auto j = blockIdx.x * blockDim.x + threadIdx.x;
    const size_t nr = a.rows - 1;
    const size_t nc = a.cols - 1;
    const size_t cols = a.cols;

    if (i > nr || j > nc) {
        return;
    }
    else if (i > 0 && j > 0 && i < nr && j < nc) { // Central cases
        e[i * cols + j] = norm2_pow2(dx_c<M,VT>(a, i, j), dy_c<M,VT>(a, i, j));
    }
    else if (i == 0 && j > 1 && j < nc) { // Upper boundary
        e[0 * cols + j] = norm2_pow2(dx_c<M,VT>(a, 0, j), dy_f<M,VT>(a, 0, j));
    }
    else if (i == nc && j > 1 && j < nc) { // Lower boundary
        e[nr * cols + j] = norm2_pow2(dx_c<M,VT>(a, nr, j), dy_b<M,VT>(a, nr, j));
    }
    else if (j == 0 && i > 1 && i < nr) { // Left boundary
        e[i * cols + 0] = norm2_pow2(dx_f<M,VT>(a, i, 0), dy_c<M,VT>(a, i, 0));
    }
    else if (j == nc && i > 1 && i < nr) { // Right boundary
        e[nr * cols + j] = norm2_pow2(dx_b<M,VT>(a, i, nc), dy_c<M,VT>(a, i, nc));
    }
    else if (i == 0 && j == 0) { // Upper-left corner
        e[0 * cols + 0] = norm2_pow2(dx_f<M,VT>(a, 0, 0), dy_f<M,VT>(a, 0, 0));
    }
    else if (i == 0 && j == nc) { // Upper-right corner
        e[0 * cols + nc] = norm2_pow2(dx_b<M,VT>(a, 0, nc), dy_f<M,VT>(a, 0, nc));
    }
    else if (i == nr && j == 0) { // Lower-left corner
        e[nr * cols + 0] = norm2_pow2(dx_f<M,VT>(a, nr, 0), dy_b<M,VT>(a, nr, 0));
    }
    else if (i == nr && j == nc) { // Lower-right corner
        e[nr * cols + nc] = norm2_pow2(dx_b<M,VT>(a, nr, nc), dy_b<M,VT>(a, nr, nc));
    }

    __syncthreads();
    T _energy = 0.0;

    if ( i < nr && j < nc ) {
        _energy = ( e[i * cols + j] +
                    e[(i+1) * cols + j] +
                    e[i * cols + (j+1)] +
                    e[(i+1) * cols + (j+1)]
                  ) * 0.25;
    }

    __syncthreads();
    e[i * cols + j] = _energy;
}

template<class M, class T, class VT>
inline T energy(const M & a_cuda, thrust::device_vector<T> & e, const dim3 & dimBlock, const dim3 & dimGrid) {
    energy_CUDA<M,T,VT><<<dimGrid, dimBlock>>>(a_cuda, thrust::raw_pointer_cast(&e[0]));
    return thrust::reduce(e.begin(), e.end());
}

template<class M, class T>
__global__ void anisotropic_chromaticity_CUDA(const M a, M r, const float delta) {
    const auto i = blockIdx.y * blockDim.y + threadIdx.y;
    const auto j = blockIdx.x * blockDim.x + threadIdx.x;
    const size_t nr = a.rows - 1;
    const size_t nc = a.cols - 1;


    if (i > nr || j > nc) {
        return;
    }
    else if (i > 1 && j > 1 && i < nr-1 && j < nc-1) { // Central cases
        const T gx1 = dx_c<M,T>(a, i, j+1) / (0 != gradient_norm<M,T>(a, i, j+1) ? gradient_norm<M,T>(a, i, j+1) : 1);
        const T gy1 = dy_c<M,T>(a, i+1, j) / (0 != gradient_norm<M,T>(a, i+1, j) ? gradient_norm<M,T>(a, i+1, j) : 1);

        const T gx2 = dx_c<M,T>(a, i, j-1) / (0 != gradient_norm<M,T>(a, i, j-1) ? gradient_norm<M,T>(a, i, j-1) : 1);
        const T gy2 = dy_c<M,T>(a, i-1, j) / (0 != gradient_norm<M,T>(a, i-1, j) ? gradient_norm<M,T>(a, i-1, j) : 1);

        const T dx = (gx1 - gx2) * 0.5;
        const T dy = (gy1 - gy2) * 0.5;

        const T _div = dx + dy;
        const T res = a.at(i,j) + delta * (_div + a.at(i,j) * gradient_norm<M,T>(a, i, j));
        r.at(i,j) = res / norm2(res);
    }
    else if (i == 0 && j > 0 && j < nc) { // Upper boundary
        const T gx1 = dx_b<M,T>(a, 0, j+1) / (0 != norm2(dx_b<M,T>(a, 0, j+1), dy_f<M,T>(a, 0, j+1)) ? norm2(dx_b<M,T>(a, 0, j+1), dy_f<M,T>(a, 0, j+1)) : 1);
        const T gy1 = dy_c<M,T>(a, 1, j)   / (0 != norm2(dx_c<M,T>(a, 1, j),   dy_c<M,T>(a, 1, j))   ? norm2(dx_c<M,T>(a, 1, j),   dy_c<M,T>(a, 1, j))   : 1);

        const T gx2 = dx_f<M,T>(a, 0, j-1) / (0 != norm2(dx_f<M,T>(a, 0, j-1), dy_f<M,T>(a, 0, j-1)) ? norm2(dx_f<M,T>(a, 0, j-1), dy_f<M,T>(a, 0, j-1)) : 1);
        const T gy  = dy_f<M,T>(a, 0, j)   / (0 != norm2(dx_c<M,T>(a, 0, j),   dy_f<M,T>(a, 0, j))   ? norm2(dx_c<M,T>(a, 0, j),   dy_f<M,T>(a, 0, j))   : 1);

        const T dx = (gx1 - gx2) * 0.5;
        const T dy = gy1 - gy;

        const T _div = dx + dy;
        const T res = a.at(0,j) + delta * (_div + a.at(0,j) * norm2(dx_c<M,T>(a, 0, j), dy_f<M,T>(a, 0, j)));
        r.at(0,j) = res / norm2(res);
    }
    else if (i == nr && j > 0 && j < nc) { // Lower boundary
        const T gx1 = dx_b<M,T>(a, nr, j+1) / (0 != norm2(dx_b<M,T>(a, nr, j+1), dy_b<M,T>(a, nr, j+1)) ? norm2(dx_b<M,T>(a, nr, j+1), dy_b<M,T>(a, nr, j+1)) : 1);
        const T gy  = dy_b<M,T>(a, nr, j)   / (0 != norm2(dx_c<M,T>(a, nr, j),   dy_b<M,T>(a, nr, j))   ? norm2(dx_c<M,T>(a, nr, j),   dy_b<M,T>(a, nr, j))   : 1);

        const T gx2 = dx_f<M,T>(a, nr, j-1) / (0 != norm2(dx_f<M,T>(a, nr, j-1), dy_b<M,T>(a, nr, j-1)) ? norm2(dx_f<M,T>(a, nr, j-1), dy_b<M,T>(a, nr, j-1)) : 1);
        const T gy2 = dy_c<M,T>(a, nr-1, j) / (0 != norm2(dx_c<M,T>(a, nr-1, j), dy_c<M,T>(a, nr-1, j)) ? norm2(dx_c<M,T>(a, nr-1, j), dy_c<M,T>(a, nr-1, j)) : 1);

        const T dx = (gx1 - gx2) * 0.5;
        const T dy = gy - gy2;

        const T _div = dx + dy;
        const T res = a.at(nr,j) + delta * (_div + a.at(nr,j) * norm2(dx_c<M,T>(a, nr, j), dy_b<M,T>(a, nr, j)));
        r.at(nr,j) = res / norm2(res);
    }
    else if (j == 0 && i > 0 && i < nr) { // Left boundary
        const T gx1 = dx_c<M,T>(a, i, 1)   / (0 != norm2(dx_c<M,T>(a, i, 1),   dy_c<M,T>(a, i, 1))   ? norm2(dx_c<M,T>(a, i, 1),   dy_c<M,T>(a, i, 1))   : 1);
        const T gy1 = dy_b<M,T>(a, i+1, 0) / (0 != norm2(dx_f<M,T>(a, i+1, 0), dy_b<M,T>(a, i+1, 0)) ? norm2(dx_f<M,T>(a, i+1, 0), dy_b<M,T>(a, i+1, 0)) : 1);

        const T gx  = dx_f<M,T>(a, i, 0)   / (0 != norm2(dx_f<M,T>(a, i, 0),   dy_c<M,T>(a, i, 0))   ? norm2(dx_f<M,T>(a, i, 0),   dy_c<M,T>(a, i, 0))   : 1);
        const T gy2 = dy_f<M,T>(a, i-1, 0) / (0 != norm2(dx_f<M,T>(a, i-1, 0), dy_f<M,T>(a, i-1, 0)) ? norm2(dx_f<M,T>(a, i-1, 0), dy_f<M,T>(a, i-1, 0)) : 1);

        const T dx = gx1 - gx;
        const T dy = (gy1 - gy2) * 0.5;

        const T _div = dx + dy;
        const T res = a.at(i,0) + delta * (_div + a.at(i,0) * norm2(dx_f<M,T>(a, i, 0), dy_c<M,T>(a, i, 0)));
        r.at(i,0) = res / norm2(res);
    }
    else if (j == nc && i > 0 && i < nr) { // Right boundary
        const T gx  = dx_b<M,T>(a, i, nc)   / (0 != norm2(dx_b<M,T>(a, i, nc),   dy_c<M,T>(a, i, nc))   ? norm2(dx_b<M,T>(a, i, nc),   dy_c<M,T>(a, i, nc))   : 1);
        const T gy1 = dy_b<M,T>(a, i+1, nc) / (0 != norm2(dx_b<M,T>(a, i+1, nc), dy_b<M,T>(a, i+1, nc)) ? norm2(dx_b<M,T>(a, i+1, nc), dy_b<M,T>(a, i+1, nc)) : 1);

        const T gx2 = dx_c<M,T>(a, i, nc-1) / (0 != norm2(dx_c<M,T>(a, i, nc-1), dy_c<M,T>(a, i, nc-1)) ? norm2(dx_c<M,T>(a, i, nc-1), dy_c<M,T>(a, i, nc-1)) : 1);
        const T gy2 = dy_f<M,T>(a, i-1, nc) / (0 != norm2(dx_b<M,T>(a, i-1, nc), dy_f<M,T>(a, i-1, nc)) ? norm2(dx_b<M,T>(a, i-1, nc), dy_f<M,T>(a, i-1, nc)) : 1);

        const T dx = gx - gx2;
        const T dy = (gy1 - gy2) * 0.5;

        const T _div = dx + dy;
        const T res = a.at(i,nc) + delta * (_div + a.at(i,nc) * norm2(dx_b<M,T>(a, i, nc), dy_c<M,T>(a, i, nc)));
        r.at(i,nc) = res / norm2(res);
    }
    else if (i == 0 && j == 0) { // Upper-left corner
        const T gx1 = dx_c<M,T>(a, 0, 1) / (0 != norm2(dx_c<M,T>(a, 0, 1), dy_f<M,T>(a, 0, 1)) ? norm2(dx_c<M,T>(a, 0, 1), dy_f<M,T>(a, 0, 1)) : 1);
        const T gy1 = dy_c<M,T>(a, 1, 0) / (0 != norm2(dx_f<M,T>(a, 1, 0), dy_c<M,T>(a, 1, 0)) ? norm2(dx_f<M,T>(a, 1, 0), dy_c<M,T>(a, 1, 0)) : 1);

        const T gx2 = dx_f<M,T>(a, 0, 0) / (0 != norm2(dx_f<M,T>(a, 0, 0), dy_f<M,T>(a, 0, 0)) ? norm2(dx_f<M,T>(a, 0, 0), dy_f<M,T>(a, 0, 0)) : 1);
        const T gy2 = dy_f<M,T>(a, 0, 0) / (0 != norm2(dx_f<M,T>(a, 0, 0), dy_f<M,T>(a, 0, 0)) ? norm2(dx_f<M,T>(a, 0, 0), dy_f<M,T>(a, 0, 0)) : 1);

        const T dx = gx1 - gx2;
        const T dy = gy1 - gy2;

        const T _div = dx + dy;
        const T res = a.at(0,0) + delta * (_div + a.at(0,0) * norm2(dx_f<M,T>(a, 0, 0), dy_f<M,T>(a, 0, 0)));
        r.at(0,0) = res / norm2(res);
    }
    else if (i == 0 && j == nc) { // Upper-right corner
        const T gx1 = dx_b<M,T>(a, 0, nc) / (0 != norm2(dx_b<M,T>(a, 0, nc), dy_f<M,T>(a, 0, nc)) ? norm2(dx_b<M,T>(a, 0, nc), dy_f<M,T>(a, 0, nc)) : 1);
        const T gy1 = dy_c<M,T>(a, 1, nc) / (0 != norm2(dx_b<M,T>(a, 1, nc), dy_c<M,T>(a, 1, nc)) ? norm2(dx_b<M,T>(a, 1, nc), dy_c<M,T>(a, 1, nc)) : 1);

        const T gx2 = dx_c<M,T>(a, 0, nc-1) / (0 != norm2(dx_c<M,T>(a, 0, nc-1), dy_f<M,T>(a, 0, nc-1)) ? norm2(dx_c<M,T>(a, 0, nc-1), dy_f<M,T>(a, 0, nc-1)) : 1);
        const T gy2 = dy_f<M,T>(a, 0, nc)   / (0 != norm2(dx_b<M,T>(a, 0, nc),   dy_f<M,T>(a, 0, nc))   ? norm2(dx_b<M,T>(a, 0, nc),   dy_f<M,T>(a, 0, nc))   : 1);

        const T dx = gx1 - gx2;
        const T dy = gy1 - gy2;

        const T _div = dx + dy;
        const T res = a.at(0,nc) + delta * (_div + a.at(0,nc) * norm2(dx_b<M,T>(a, 0, nc), dy_f<M,T>(a, 0, nc)));
        r.at(0,nc) = res / norm2(res);
    }
    else if (i == nr && j == 0) { // Lower-left corner
        const T gx1 = dx_c<M,T>(a, nr, 1) / (0 != norm2(dx_c<M,T>(a, nr, 1), dy_b<M,T>(a, nr, 1)) ? norm2(dx_c<M,T>(a, nr, 1), dy_b<M,T>(a, nr, 1)) : 1);
        const T gy1 = dy_b<M,T>(a, nr, 0) / (0 != norm2(dx_f<M,T>(a, nr, 0), dy_b<M,T>(a, nr, 0)) ? norm2(dx_f<M,T>(a, nr, 0), dy_b<M,T>(a, nr, 0)) : 1);

        const T gx2 = dx_f<M,T>(a, nr, 0)   / (0 != norm2(dx_f<M,T>(a, nr, 0),   dy_b<M,T>(a, nr, 0))   ? norm2(dx_f<M,T>(a, nr, 0),   dy_b<M,T>(a, nr, 0))   : 1);
        const T gy2 = dy_c<M,T>(a, nr-1, 0) / (0 != norm2(dx_f<M,T>(a, nr-1, 0), dy_c<M,T>(a, nr-1, 0)) ? norm2(dx_f<M,T>(a, nr-1, 0), dy_c<M,T>(a, nr-1, 0)) : 1);

        const T dx = gx1 - gx2;
        const T dy = gy1 - gy2;

        const T _div = dx + dy;
        const T res = a.at(nr,0) + delta * (_div + a.at(nr,0) * norm2(dx_f<M,T>(a, nr, 0), dy_b<M,T>(a, nr, 0)));
        r.at(nr,0) = res / norm2(res);
    }
    else if (i == nr && j == nc) { // Lower-right corner
        const T gx1 = dx_b<M,T>(a, nr, nc) / (0 != norm2(dx_b<M,T>(a, nr, nc), dy_b<M,T>(a, nr, nc)) ? norm2(dx_b<M,T>(a, nr, nc), dy_b<M,T>(a, nr, nc)) : 1);
        const T gy1 = dy_b<M,T>(a, nr, nc) / (0 != norm2(dx_b<M,T>(a, nr, nc), dy_b<M,T>(a, nr, nc)) ? norm2(dx_b<M,T>(a, nr, nc), dy_b<M,T>(a, nr, nc)) : 1);

        const T gx2 = dx_c<M,T>(a, nr, nc-1) / (0 != norm2(dx_c<M,T>(a, nr, nc-1), dy_b<M,T>(a, nr, nc-1)) ? norm2(dx_c<M,T>(a, nr, nc-1), dy_b<M,T>(a, nr, nc-1)) : 1);
        const T gy2 = dy_c<M,T>(a, nr-1, nc) / (0 != norm2(dx_b<M,T>(a, nr-1, nc), dy_c<M,T>(a, nr-1, nc)) ? norm2(dx_b<M,T>(a, nr-1, nc), dy_c<M,T>(a, nr-1, nc)) : 1);

        const T dx = gx1 - gx2;
        const T dy = gy1 - gy2;

        const T _div = dx + dy;
        const T res = a.at(nr,nc) + delta * (_div + a.at(nr,nc) * norm2(dx_b<M,T>(a, nr, nc), dy_b<M,T>(a, nr, nc)));
        r.at(nr,nc) = res / norm2(res);
    }
    else if (i == 1 && j > 1 && j < nc-1) { // Upper-inner boundary
        const T gx1 = dx_c<M,T>(a, 1, j+1) / (0 != gradient_norm<M,T>(a, 1, j+1) ? gradient_norm<M,T>(a, 1, j+1) : 1);
        const T gy1 = dy_c<M,T>(a, 2, j)   / (0 != gradient_norm<M,T>(a, 2, j)   ? gradient_norm<M,T>(a, 2, j)   : 1);

        const T gx2 = dx_c<M,T>(a, 1, j-1) / (0 != gradient_norm<M,T>(a, 1, j-1) ? gradient_norm<M,T>(a, 1, j-1) : 1);
        const T gy2 = dy_f<M,T>(a, 0, j)   / (0 != norm2(dx_c<M,T>(a, 0, j), dy_f<M,T>(a, 0, j)) ? norm2(dx_c<M,T>(a, 0, j), dy_f<M,T>(a, 0, j)) : 1); // gradient_norm<M,T>(a, 0, j);

        const T dx = (gx1 - gx2) * 0.5;
        const T dy = (gy1 - gy2) * 0.5;

        const T _div = dx + dy;
        const T res = a.at(1,j) + delta * (_div + a.at(1,j) * gradient_norm<M,T>(a, 1, j));
        r.at(1,j) = res / norm2(res);
    }
    else if (i == nr-1 && j > 1 && j < nc-1) { // Lower-inner boundary
        const T gx1 = dx_c<M,T>(a, nr-1, j+1) / (0 != gradient_norm<M,T>(a, nr-1, j+1) ? gradient_norm<M,T>(a, nr-1, j+1) : 1);
        const T gy1 = dy_b<M,T>(a, nr, j)     / (0 != norm2(dx_c<M,T>(a, nr, j), dy_b<M,T>(a, nr, j)) ? norm2(dx_c<M,T>(a, nr, j), dy_b<M,T>(a, nr, j)) : 1);

        const T gx2 = dx_c<M,T>(a, nr-1, j-1) / (0 != gradient_norm<M,T>(a, 1, j-1)  ? gradient_norm<M,T>(a, 1, j-1)  : 1);
        const T gy2 = dy_c<M,T>(a, nr-2, j)   / (0 != gradient_norm<M,T>(a, nr-2, j) ? gradient_norm<M,T>(a, nr-2, j) : 1);

        const T dx = (gx1 - gx2) * 0.5;
        const T dy = (gy1 - gy2) * 0.5;

        const T _div = dx + dy;
        const T res = a.at(nr-1,j) + delta * (_div + a.at(nr-1,j) * gradient_norm<M,T>(a, nr-1, j));
        r.at(nr-1,j) = res / norm2(res);
    }
    else if (j == 1 && i > 1 && i < nr-1) { // Left-inner boundary
        const T gx1 = dx_c<M,T>(a, i, 2)   / (0 != gradient_norm<M,T>(a, i, 2)   ? gradient_norm<M,T>(a, i, 2)   : 1);
        const T gy1 = dy_c<M,T>(a, i+1, 1) / (0 != gradient_norm<M,T>(a, i+1, 1) ? gradient_norm<M,T>(a, i+1, 1) : 1);

        const T gx2 = dx_f<M,T>(a, i, 0)   / (0 != norm2(dx_f<M,T>(a, i, 0), dy_c<M,T>(a, i, 0)) ? norm2(dx_f<M,T>(a, i, 0), dy_c<M,T>(a, i, 0)) : 1);
        const T gy2 = dy_c<M,T>(a, i-1, 1) / (0 != gradient_norm<M,T>(a, i-1, 1) ? gradient_norm<M,T>(a, i-1, 1) : 1);

        const T dx = (gx1 - gx2) * 0.5;
        const T dy = (gy1 - gy2) * 0.5;

        const T _div = dx + dy;
        const T res = a.at(i,1) + delta * (_div + a.at(i,1) * gradient_norm<M,T>(a, i, 1));
        r.at(i,1) = res / norm2(res);
    }
    else if (j == nc-1 && i > 1 && i < nr-1) { // Right inner bonudary
        const T gx1 = dx_b<M,T>(a, i, nc)     / (0 != norm2(dx_b<M,T>(a, i, nc), dy_c<M,T>(a, i, nc)) ? norm2(dx_b<M,T>(a, i, nc), dy_c<M,T>(a, i, nc)) : 1);
        const T gy1 = dy_c<M,T>(a, i+1, nc-1) / (0 != gradient_norm<M,T>(a, i+1, nc-1) ? gradient_norm<M,T>(a, i+1, nc-1) : 1);

        const T gx2 = dx_c<M,T>(a, i, nc-2)   / (0 != gradient_norm<M,T>(a, i, nc-2)   ? gradient_norm<M,T>(a, i, nc-2)   : 1);
        const T gy2 = dy_c<M,T>(a, i-1, nc-1) / (0 != gradient_norm<M,T>(a, i-1, nc-1) ? gradient_norm<M,T>(a, i-1, nc-1) : 1);

        const T dx = (gx1 - gx2) * 0.5;
        const T dy = (gy1 - gy2) * 0.5;

        const T _div = dx + dy;
        const T res = a.at(i,nc) + delta * (_div + a.at(i,nc) * gradient_norm<M,T>(a, i, nc-1));
        r.at(i,nc-1) = res / norm2(res);
    }
    else if (i == 1 && j == 1) { // Upper-left inner corner
        const T gx1 = dx_c<M,T>(a, 1, 2) / (0 != gradient_norm<M,T>(a, 1, 2) ? gradient_norm<M,T>(a, 1, 2) : 1);
        const T gy1 = dy_c<M,T>(a, 2, 1) / (0 != gradient_norm<M,T>(a, 2, 1) ? gradient_norm<M,T>(a, 2, 1) : 1);

        const T gx2 = dx_f<M,T>(a, 1, 0) / (0 != norm2(dx_f<M,T>(a, 1, 0), dy_c<M,T>(a, 1, 0)) ? norm2(dx_f<M,T>(a, 1, 0), dy_c<M,T>(a, 1, 0)) : 1);
        const T gy2 = dy_f<M,T>(a, 0, 1) / (0 != norm2(dx_c<M,T>(a, 0, 1), dy_f<M,T>(a, 0, 1)) ? norm2(dx_c<M,T>(a, 0, 1), dy_f<M,T>(a, 0, 1)) : 1);

        const T dx = (gx1 - gx2) * 0.5;
        const T dy = (gy1 - gy2) * 0.5;

        const T _div = dx + dy;
        const T res = a.at(1,1) + delta * (_div + a.at(1,1) * gradient_norm<M,T>(a, 1, 1));
        r.at(1,1) = res / norm2(res);
    }
    else if (i == 1 && j == nc-1) { // Upper-right inner corner
        const T gx1 = dx_b<M,T>(a, 1, nc)   / (0 != norm2(dx_b<M,T>(a, 1, nc), dy_c<M,T>(a, 1, nc)) ? norm2(dx_b<M,T>(a, 1, nc), dy_c<M,T>(a, 1, nc)) : 1);
        const T gy1 = dy_c<M,T>(a, 2, nc-1) / (0 != gradient_norm<M,T>(a, 2, nc-1) ? gradient_norm<M,T>(a, 2, nc-1) : 1);

        const T gx2 = dx_c<M,T>(a, 1, nc-2) / (0 != gradient_norm<M,T>(a, 1, nc-2) ? gradient_norm<M,T>(a, 1, nc-2) : 1);
        const T gy2 = dy_f<M,T>(a, 0, nc-1) / (0 != norm2(dx_c<M,T>(a, 0, nc-1), dy_f<M,T>(a, 0, nc-1)) ? norm2(dx_c<M,T>(a, 0, nc-1), dy_f<M,T>(a, 0, nc-1)) : 1);

        const T dx = (gx1 - gx2) * 0.5;
        const T dy = (gy1 - gy2) * 0.5;

        const T _div = dx + dy;
        const T res = a.at(1,nc-1) + delta * (_div + a.at(1,nc-1) * gradient_norm<M,T>(a, 1, nc-1));
        r.at(1,nc-1) = res / norm2(res);
    }
    else if (i == nr-1 && j == 1) { // Lower-left inner corner
        const T gx1 = dx_c<M,T>(a, nr-1, 2) / (0 != gradient_norm<M,T>(a, nr-1, 2) ? gradient_norm<M,T>(a, nr-1, 2) : 1);
        const T gy1 = dy_b<M,T>(a, nr, 1)   / (0 != norm2(dx_c<M,T>(a, nr, 1), dy_b<M,T>(a, nr, 1)) ? norm2(dx_c<M,T>(a, nr, 1), dy_b<M,T>(a, nr, 1)) : 1);

        const T gx2 = dx_f<M,T>(a, nr-1, 0) / (0 != norm2(dx_f<M,T>(a, nr-1, 0), dy_c<M,T>(a, nr-1, 0)) ? norm2(dx_f<M,T>(a, nr-1, 0), dy_c<M,T>(a, nr-1, 0)) : 1);
        const T gy2 = dy_c<M,T>(a, nr-2, 1) / (0 != gradient_norm<M,T>(a, nr-2, 1) ? gradient_norm<M,T>(a, nr-2, 1) : 1);

        const T dx = (gx1 - gx2) * 0.5;
        const T dy = (gy1 - gy2) * 0.5;

        const T _div = dx + dy;
        const T res = a.at(nr-1,1) + delta * (_div + a.at(nr-1,1) * gradient_norm<M,T>(a, nr-1,1));
        r.at(nr-1,1) = res / norm2(res);
    }
    else if (i == nr-1 && j == nc-1) { // Lower-right inner corner
        const T gx1 = dx_b<M,T>(a, nr-1, nc) / (0 != norm2(dx_b<M,T>(a, nr-1, nc), dy_c<M,T>(a, nr-1, nc)) ? norm2(dx_b<M,T>(a, nr-1, nc), dy_c<M,T>(a, nr-1, nc)) : 1);
        const T gy1 = dy_b<M,T>(a, nr, nc-1) / (0 != norm2(dx_c<M,T>(a, nr, nc-1), dy_b<M,T>(a, nr, nc-1)) ? norm2(dx_c<M,T>(a, nr, nc-1), dy_b<M,T>(a, nr, nc-1)) : 1);

        const T gx2 = dx_c<M,T>(a, nr-1, nc-2) / (0 != gradient_norm<M,T>(a, nr-1, nc-2) ? gradient_norm<M,T>(a, nr-1, nc-2) : 1);
        const T gy2 = dy_c<M,T>(a, nr-2, nc-1) / (0 != gradient_norm<M,T>(a, nr-2, nc-1) ? gradient_norm<M,T>(a, nr-2, nc-1) : 1);

        const T dx = (gx1 - gx2) * 0.5;
        const T dy = (gy1 - gy2) * 0.5;

        const T _div = dx + dy;
        const T res = a.at(nr-1,nc-1) + delta * (_div + a.at(nr-1,nc-1) * gradient_norm<M,T>(a, nr-1, nc-1));
        r.at(nr-1,nc-1) = res / norm2(res);
    }
}

void chromaticity_anisotropic_CUDA(Matrix<Vec3<float>> & a, const float tolerance, const size_t max_iter) {
    using T = float;
    using VT = Vec3_CUDA<T>;
    using M = Matrix_CUDA<VT>;

    const float delta = 0.01;
    const size_t n = a.rows * a.cols;
    T c_diff = tolerance + 1;
    T c_energy;
    size_t i = 0;
    double flow_time = 0;
    double energy_time = 0;
    double clone_time = 0;

    auto start = std::chrono::system_clock::now();

    VT * a_cuda_ptr;
    VT * r_cuda_ptr;
    thrust::device_vector<T> e(a.rows * a.cols);

    CUDA_CHECK_RETURN(cudaMalloc(&a_cuda_ptr, n * sizeof(VT)));
    CUDA_CHECK_RETURN(cudaMalloc(&r_cuda_ptr, n * sizeof(VT)));

    M a_cuda(a.rows, a.cols, a_cuda_ptr);
    M r_cuda(a.rows, a.cols, r_cuda_ptr);

    CUDA_CHECK_RETURN(cudaMemcpy(a_cuda.values, a.values, n * sizeof(VT), cudaMemcpyHostToDevice));

    const dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
    const dim3 dimGrid(ceil(a.cols / dimBlock.x), ceil(a.rows / dimBlock.y));

    const T i_energy = energy<M,T,VT>(a_cuda, e, dimBlock, dimGrid);

    std::cout << "Anisotropic diffusion:" << '\n';
    std::cout << "\tInitial energy: " << i_energy << '\n';

    for (i = 0; i < max_iter; i++) {
        auto start = std::chrono::system_clock::now();
        anisotropic_chromaticity_CUDA<M,VT><<<dimGrid, dimBlock>>>(a_cuda, r_cuda, delta);
        cudaDeviceSynchronize();
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_time = end-start;
        flow_time += elapsed_time.count();

        start = std::chrono::system_clock::now();
        a_cuda.swap(r_cuda);
        end = std::chrono::system_clock::now();
        elapsed_time = end-start;
        clone_time += elapsed_time.count();

        start = std::chrono::system_clock::now();
        c_energy = energy<M,T,VT>(a_cuda, e, dimBlock, dimGrid);
        cudaDeviceSynchronize();
        end = std::chrono::system_clock::now();
        elapsed_time = end-start;
        energy_time += elapsed_time.count();

        c_diff = c_energy / i_energy;
        std::cout << "\tIter " << i << ",\tenergy: " << c_energy << ",\tdifference: " << c_diff << '\n';
        if (c_diff < tolerance) {
            std::cout << "Convergence found on iteration " << i++ << " (difference = " << c_diff << ")" << '\n';
            break;
        }
    }
    if (i == max_iter && c_diff > tolerance) {
        std::cout << "Convergence not found in " << i << " iterations (difference = " << c_diff << ")" << '\n';
    }
    CUDA_CHECK_RETURN(cudaMemcpy(a.values, a_cuda.values, n * sizeof(VT), cudaMemcpyDeviceToHost));

    CUDA_CHECK_RETURN(cudaFree(a_cuda_ptr));
    CUDA_CHECK_RETURN(cudaFree(r_cuda_ptr));

    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_time = end-start;
    double seconds = elapsed_time.count();
    std::cout << "Anisotropic diffusion time: " << seconds << " seconds" << '\n';
    std::cout << "\tTime per iteration: " << seconds / i << " seconds" << '\n';
    std::cout << "\tTime anisotropic diffusion: " << flow_time << " (" << flow_time / i << ")" << " seconds" << '\n';
    std::cout << "\tTime clone: " << clone_time << " (" << clone_time / i << ")" << " seconds" << '\n';
    std::cout << "\tTime energy: " << energy_time<< " (" << energy_time / i << ")"  << " seconds" << '\n';

    #ifdef MEASURE
    std::cout << "Total;Iterations;Total_per_it;Flow;Energy" << '\n';
    std::cout << seconds << ";" << i << ";" << seconds / i << ";" << flow_time << ";" << energy_time << '\n';
    #endif
}

template<class M, class T>
__global__ void isotropic_chromaticity_CUDA(const M a, M r, const float delta) {
    const auto i = blockIdx.y * blockDim.y + threadIdx.y;
    const auto j = blockIdx.x * blockDim.x + threadIdx.x;
    const size_t nr = a.rows - 1;
    const size_t nc = a.cols - 1;

    if (i >= a.rows || j >= a.cols) {
        return;
    }
    else if (i > 0 && j > 0 && i < nr && j < nc) { // Central cases
        const T dx = dx_c<M,T>(a, i, j);
        const T dy = dy_c<M,T>(a, i, j);
        const T lapl = laplacian<M,T>(a, i, j);
        const T res = a.at(i,j) + delta * (lapl + a.at(i,j) * norm2_pow2(dx,dy));
        r.at(i,j) = res / norm2(res);
    }
    else if (i == 0 && j > 0 && j < nc) { // Upper boundary
        const T dx = dx_c<M,T>(a, 0, j);
        const T dy = dy_f<M,T>(a, 0, j);
        const T lapl =
            // a.at(-1,j) + // this is asumed zero
            a.at(0,j-1) -
            4 * a.at(0,j) +
            a.at(1,j) +
            a.at(0,j+1);
        const T res = a.at(0,j) + delta * (lapl + a.at(0,j) * norm2_pow2(dx, dy));
        r.at(0,j) = res / norm2(res);
    }
    else if (i == nc && j > 0 && j < nc) { // Lower boundary
        const T dx = dx_c<M,T>(a, nr, j);
        const T dy = dy_b<M,T>(a, nr, j);
        const T lapl =
            a.at(nr-1,j) +
            a.at(nr,j-1) -
            4 * a.at(nr,j) +
            // a.at(nr+1,j) + // this is asumed zero
            a.at(nr,j+1);
        const T res = a.at(nr,j) + delta * (lapl + a.at(nr,j) * norm2_pow2(dx, dy));
        r.at(nr,j) = res / norm2(res);
    }
    else if (j == 0 && i > 0 && i < nr) { // Left boundary
        // Left boundary
        const T dx = dx_f<M,T>(a, i, 0);
        const T dy = dy_c<M,T>(a, i, 0);
        const T lapl =
            a.at(i-1,0) +
            // a.at(i,-1) - // this is asumed zero
            4 * a.at(i,0) +
            a.at(i+1,0) +
            a.at(i,1);
        const T res = a.at(i,0) + delta * (lapl + a.at(i,0) * norm2_pow2(dx, dy));
        r.at(i,0) = res / norm2(res);
    }
    else if (j == nc && i > 0 && i < nr) { // Right boundary
        const T dx = dx_b<M,T>(a, i, nc);
        const T dy = dy_c<M,T>(a, i, nc);
        const T lapl =
            a.at(i-1,nc) +
            a.at(i,nc-1) -
            4 * a.at(i,nc) +
            a.at(i+1,nc);
            // a.at(i,nc+1); // this is asumed zero
        const T res = a.at(i,nc) + delta * (lapl + a.at(i,nc) * norm2_pow2(dx, dy));
        r.at(i,nc) = res / norm2(res);
    }
    else if (i == 0 && j == 0) { // Upper-left corner
        // Position 0,0
        const T dx = dx_f<M,T>(a, 0, 0);
        const T dy = dy_f<M,T>(a, 0, 0);
        const T lapl =
            // a.at(-1,0) + // this is asumed zero
            // a.at(0,-1) - // this is asumed zero
            - 4 * a.at(0,0)
            + a.at(1,0)
            + a.at(0,1);
        const T res = a.at(0,0) + delta * (lapl + a.at(0,0) * norm2_pow2(dx, dy));
        r.at(0,0) = res / norm2(res);
    }
    else if (i == 0 && j == nc) { // Upper-right corner
        const T dx = dx_b<M,T>(a, 0, nc);
        const T dy = dy_f<M,T>(a, 0, nc);
        const T lapl =
            // a.at(-1,nc) + // this is asumed zero
            a.at(0,nc-1) - // this is asumed zero
            4 * a.at(0,nc) +
            a.at(1,nc);
            // a.at(0,nc+1); // this is asumed zero
        const T res = a.at(0,nc) + delta * (lapl + a.at(0,nc) * norm2_pow2(dx, dy));
        r.at(0,nc) = res / norm2(res);
    }
    else if (i == nr && j == 0) {
        const T dx = dx_f<M,T>(a, nr, 0);
        const T dy = dy_b<M,T>(a, nr, 0);
        const T lapl =
            a.at(nr-1,0) +
            // a.at(nr,-1) - // this is asumed zero
            4 * a.at(nr,0) +
            // a.at(nr+1,0) + // this is asumed zero
            a.at(nr,1);
        const T res = a.at(nr,0) + delta * (lapl + a.at(nr,0) * norm2_pow2(dx, dy));
        r.at(nr,0) = res / norm2(res);
    }
    else if (i == nr && j == nc) { // Lower-left corner
        const T dx = dx_b<M,T>(a, nr, nc);
        const T dy = dy_b<M,T>(a, nr, nc);
        const T lapl =
            a.at(nr-1,nc) +
            a.at(nr,nc-1) -
            4 * a.at(nr,nc);
            // a.at(nr+1,nc) + // this is asumed zero
            // a.at(nr,nc+1); // this is asumed zero
        const T res = a.at(nr,nc) + delta * (lapl + a.at(nr,nc) * norm2_pow2(dx, dy));
        r.at(nr,nc) = res / norm2(res);
    }
}

void chromaticity_isotropic_CUDA(Matrix<Vec3<float>> & a, const float tolerance, const size_t max_iter) {
    using T = float;
    using VT = Vec3_CUDA<T>;
    using M = Matrix_CUDA<VT>;

    auto start = std::chrono::system_clock::now();

    const float delta = 0.1;
    const size_t n = a.rows * a.cols;
    T c_diff = tolerance + 1;
    T c_energy;
    size_t i = 0;
    double flow_time = 0;
    double energy_time = 0;
    double clone_time = 0;

    VT * a_cuda_ptr;
    VT * r_cuda_ptr;
    thrust::device_vector<T> e(a.rows * a.cols);

    CUDA_CHECK_RETURN(cudaMalloc(&a_cuda_ptr, n * sizeof(VT)));
    CUDA_CHECK_RETURN(cudaMalloc(&r_cuda_ptr, n * sizeof(VT)));

    M a_cuda(a.rows, a.cols, a_cuda_ptr);
    M r_cuda(a.rows, a.cols, r_cuda_ptr);

    CUDA_CHECK_RETURN(cudaMemcpy(a_cuda.values, a.values, n * sizeof(VT), cudaMemcpyHostToDevice));

    const dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
    const dim3 dimGrid(ceil(a.cols / dimBlock.x), ceil(a.rows / dimBlock.y));

    const T i_energy = energy<M,T,VT>(a_cuda, e, dimBlock, dimGrid);

    std::cout << "Isotropic diffusion:" << '\n';
    std::cout << "\tInitial energy: " << i_energy << '\n';

    for (i = 0; i < max_iter; i++) {
        auto start = std::chrono::system_clock::now();
        isotropic_chromaticity_CUDA<M,VT><<<dimGrid, dimBlock>>>(a_cuda, r_cuda, delta);
        cudaDeviceSynchronize();
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_time = end-start;
        flow_time += elapsed_time.count();

        start = std::chrono::system_clock::now();
        a_cuda.swap(r_cuda);
        end = std::chrono::system_clock::now();
        elapsed_time = end-start;
        clone_time += elapsed_time.count();

        start = std::chrono::system_clock::now();
        c_energy = energy<M,T,VT>(a_cuda, e, dimBlock, dimGrid);
        cudaDeviceSynchronize();
        end = std::chrono::system_clock::now();
        elapsed_time = end-start;
        energy_time += elapsed_time.count();

        c_diff = c_energy / i_energy;
        std::cout << "\tIter " << i << ",\tenergy: " << c_energy << ",\tdifference: " << c_diff << '\n';
        if (c_diff < tolerance) {
            std::cout << "Convergence found on iteration " << i++ << " (difference = " << c_diff << ")" << '\n';
            break;
        }
    }
    if (i == max_iter && c_diff > tolerance) {
        std::cout << "Convergence not found in " << i << " iterations (difference = " << c_diff << ")" << '\n';
    }
    CUDA_CHECK_RETURN(cudaMemcpy(a.values, a_cuda.values, n * sizeof(VT), cudaMemcpyDeviceToHost));
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_time = end-start;
    double seconds = elapsed_time.count();
    std::cout << "Isotropic diffusion time: " << seconds << " seconds" << '\n';
    std::cout << "\tTime per iteration: " << seconds / i << " seconds" << '\n';
    std::cout << "\tTime isotropic diffusion: " << flow_time << " (" << flow_time / i << ")" << " seconds" << '\n';
    std::cout << "\tTime clone: " << clone_time << " (" << clone_time / i << ")" << " seconds" << '\n';
    std::cout << "\tTime energy: " << energy_time<< " (" << energy_time / i << ")"  << " seconds" << '\n';

    #ifdef MEASURE
    std::cout << "Total;Iterations;Total_per_it;Flow;Energy" << '\n';
    std::cout << seconds << ";" << i << ";" << seconds / i << ";" << flow_time << ";" << energy_time << '\n';
    #endif

    CUDA_CHECK_RETURN(cudaFree(a_cuda_ptr));
    CUDA_CHECK_RETURN(cudaFree(r_cuda_ptr));
}

template<class M, class T>
__global__ void anisotropic_brightness_CUDA(const M a, M r, const float delta) {
    const auto i = blockIdx.y * blockDim.y + threadIdx.y;
    const auto j = blockIdx.x * blockDim.x + threadIdx.x;
    const size_t nr = a.rows - 1;
    const size_t nc = a.cols - 1;

    if (i > nr || j > nc) {
        return;
    }
    else if (i > 0 && j > 0 && i < nr && j < nc) { // Central cases
        const T Mx = dx_c<M,T>(a, i, j);
        const T My = dy_c<M,T>(a, i, j);
        const T Mxx = a.at(i,j+1) - 2*a.at(i,j) + a.at(i,j-1);
        const T Myy = a.at(i+1,j) - 2*a.at(i,j) + a.at(i-1,j);
        const T Mxy = (a.at(i+1,j+1) - a.at(i+1,j-1) - a.at(i-1,j+1) + a.at(i-1,j-1)) * 0.25;
        const T norm_M = sqrt(Mx*Mx + My*My);
        r.at(i,j) = a.at(i,j) + delta * (cbrt(Mxx*My*My - 2*Mx*My*Mxy + Myy*Mx*Mx) / (1 + norm_M));
    }
    else if (i == 0 && j > 1 && j < nc) { // Upper boundary
        const T Mx = dx_c<M,T>(a, 0, j);
        const T My = dy_f<M,T>(a, 0, j);
        const T Mxx = a.at(0,j+1) - 2*a.at(0,j) + a.at(0,j-1);
        const T Myy = a.at(1,j) - 2*a.at(0,j);
        const T Mxy = (a.at(1,j+1) - a.at(1,j-1)) * 0.25;
        const T norm_M = sqrt(Mx*Mx + My*My);
        r.at(0,j) = a.at(0,j) + delta * (cbrt(Mxx*My*My - 2*Mx*My*Mxy + Myy*Mx*Mx) / (1 + norm_M));
    }
    else if (i == nc && j > 1 && j < nc) { // Lower boundary
        const T Mx = dx_c<M,T>(a, nr, j);
        const T My = dy_b<M,T>(a, nr, j);
        const T Mxx = a.at(nr,j+1) - 2*a.at(nr,j) + a.at(nr,j-1);
        const T Myy = - 2*a.at(nr,j) + a.at(nr-1,j);
        const T Mxy = (- a.at(nr-1,j+1) + a.at(nr-1,j-1)) * 0.25;
        const T norm_M = sqrt(Mx*Mx + My*My);
        r.at(nr,j) = a.at(nr,j) + delta * (cbrt(Mxx*My*My - 2*Mx*My*Mxy + Myy*Mx*Mx) / (1 + norm_M));
    }
    else if (j == 0 && i > 1 && i < nr) { // Left boundary
        const T Mx = dx_f<M,T>(a, i, 0);
        const T My = dy_c<M,T>(a, i, 0);
        const T Mxx = a.at(i,1) - 2*a.at(i,0);
        const T Myy = a.at(i+1,0) - 2*a.at(i,0) + a.at(i-1,0);
        const T Mxy = (a.at(i+1,1) - a.at(i-1,1)) * 0.25;
        const T norm_M = sqrt(Mx*Mx + My*My);
        r.at(i,0) = a.at(i,0) + delta * (cbrt(Mxx*My*My - 2*Mx*My*Mxy + Myy*Mx*Mx) / (1 + norm_M));
    }
    else if (j == nc && i > 1 && i < nr) { // Right boundary
        const T Mx = dx_b<M,T>(a, i, nc);
        const T My = dy_c<M,T>(a, i, nc);
        const T Mxx = - 2*a.at(i,nc) + a.at(i,nc-1);
        const T Myy = a.at(i+1,nc) - 2*a.at(i,nc) + a.at(i-1,nc);
        const T Mxy = (-a.at(i+1,nc-1) + a.at(i-1,nc-1)) * 0.25;
        const T norm_M = sqrt(Mx*Mx + My*My);
        r.at(i,nc) = a.at(i,nc) + delta * (cbrt(Mxx*My*My - 2*Mx*My*Mxy + Myy*Mx*Mx) / (1 + norm_M));
    }
    else if (i == 0 && j == 0) { // Upper-left corner
        const T Mx = dx_f<M,T>(a,0,0);
        const T My = dy_f<M,T>(a,0,0);
        const T Mxx = a.at(0,1) - 2*a.at(0,0);
        const T Myy = a.at(1,0) - 2*a.at(0,0);
        const T Mxy = (a.at(1,1)) * 0.25;
        const T norm_M = sqrt(Mx*Mx + My*My);
        r.at(0,0) = a.at(0,0) + delta * (cbrt(Mxx*My*My - 2*Mx*My*Mxy + Myy*Mx*Mx) / (1 + norm_M));
    }
    else if (i == 0 && j == nc) { // Upper-right corner
        const T Mx = dx_b<M,T>(a,0,nc);
        const T My = dy_f<M,T>(a,0,nc);
        const T Mxx = - 2*a.at(0,nc) + a.at(0,nc-1);
        const T Myy = a.at(1,nc) - 2*a.at(0,nc);
        const T Mxy = (- a.at(1,nc-1)) * 0.25;
        const T norm_M = sqrt(Mx*Mx + My*My);
        r.at(0,nc) = a.at(0,nc) + delta * (cbrt(Mxx*My*My - 2*Mx*My*Mxy + Myy*Mx*Mx) / (1 + norm_M));
    }
    else if (i == nr && j == 0) { // Lower-left corner
        const T Mx = dx_f<M,T>(a,nr,0);
        const T My = dy_b<M,T>(a,nr,0);
        const T Mxx = a.at(nr,1) - 2*a.at(nr,0);
        const T Myy = - 2*a.at(nr,0) + a.at(nr-1,0);
        const T Mxy = (- a.at(nr-1,1)) * 0.25;
        const T norm_M = sqrt(Mx*Mx + My*My);
        r.at(nr,0) = a.at(nr,0) + delta * (cbrt(Mxx*My*My - 2*Mx*My*Mxy + Myy*Mx*Mx) / (1 + norm_M));
    }
    else if (i == nr && j == nc) { // Lower-right corner
        const T Mx = dx_b<M,T>(a,nr,nc);
        const T My = dy_b<M,T>(a,nr,nc);
        const T Mxx = - 2*a.at(nr,nc) + a.at(nr,nc-1);
        const T Myy = - 2*a.at(nr,nc) + a.at(nr-1,nc);
        const T Mxy = (a.at(nr-1,nc-1)) * 0.25;
        const T norm_M = sqrt(Mx*Mx + My*My);
        r.at(nr,nc) = a.at(nr,nc) + delta * (cbrt(Mxx*My*My - 2*Mx*My*Mxy + Myy*Mx*Mx) / (1 + norm_M));
    }
}

void brightness_anisotropic_CUDA(Matrix<float> & a, const float tolerance, const size_t max_iter) {
    using T = float;
    using VT = T;
    using M = Matrix_CUDA<VT>;

    auto start = std::chrono::system_clock::now();

    const float delta = 0.1;
    const size_t n = a.rows * a.cols;
    T c_diff = tolerance + 1;
    T c_energy;
    size_t i = 0;
    double flow_time = 0;
    double energy_time = 0;
    double clone_time = 0;

    VT * a_cuda_ptr;
    VT * r_cuda_ptr;
    thrust::device_vector<T> e(a.rows * a.cols);

    CUDA_CHECK_RETURN(cudaMalloc(&a_cuda_ptr, n * sizeof(VT)));
    CUDA_CHECK_RETURN(cudaMalloc(&r_cuda_ptr, n * sizeof(VT)));

    M a_cuda(a.rows, a.cols, a_cuda_ptr);
    M r_cuda(a.rows, a.cols, r_cuda_ptr);

    CUDA_CHECK_RETURN(cudaMemcpy(a_cuda.values, a.values, n * sizeof(VT), cudaMemcpyHostToDevice));

    const dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
    const dim3 dimGrid(ceil(a.cols / dimBlock.x), ceil(a.rows / dimBlock.y));

    const T i_energy = energy<M,T,VT>(a_cuda, e, dimBlock, dimGrid);

    std::cout << "Anisotropic diffusion:" << '\n';
    std::cout << "\tInitial energy: " << i_energy << '\n';

    for (i = 0; i < max_iter; i++) {
        auto start = std::chrono::system_clock::now();
        anisotropic_brightness_CUDA<M,VT><<<dimGrid, dimBlock>>>(a_cuda, r_cuda, delta);
        cudaDeviceSynchronize();
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_time = end-start;
        flow_time += elapsed_time.count();

        start = std::chrono::system_clock::now();
        a_cuda.swap(r_cuda);
        end = std::chrono::system_clock::now();
        elapsed_time = end-start;
        clone_time += elapsed_time.count();

        start = std::chrono::system_clock::now();
        c_energy = energy<M,T,VT>(a_cuda, e, dimBlock, dimGrid);
        cudaDeviceSynchronize();
        end = std::chrono::system_clock::now();
        elapsed_time = end-start;
        energy_time += elapsed_time.count();

        c_diff = c_energy / i_energy;
        std::cout << "\tIter " << i << ",\tenergy: " << c_energy << ",\tdifference: " << c_diff << '\n';
        if (c_diff < tolerance) {
            std::cout << "Convergence found on iteration " << i++ << " (difference = " << c_diff << ")" << '\n';
            break;
        }
    }
    if (i == max_iter && c_diff > tolerance) {
        std::cout << "Convergence not found in " << i << " iterations (difference = " << c_diff << ")" << '\n';
    }
    CUDA_CHECK_RETURN(cudaMemcpy(a.values, a_cuda.values, n * sizeof(VT), cudaMemcpyDeviceToHost));

    CUDA_CHECK_RETURN(cudaFree(a_cuda_ptr));
    CUDA_CHECK_RETURN(cudaFree(r_cuda_ptr));

    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_time = end-start;
    double seconds = elapsed_time.count();
    std::cout << "Anisotropic diffusion time: " << seconds << " seconds" << '\n';
    std::cout << "\tTime per iteration: " << seconds / i << " seconds" << '\n';
    std::cout << "\tTime anisotropic diffusion: " << flow_time << " (" << flow_time / i << ")" << " seconds" << '\n';
    std::cout << "\tTime clone: " << clone_time << " (" << clone_time / i << ")" << " seconds" << '\n';
    std::cout << "\tTime energy: " << energy_time<< " (" << energy_time / i << ")"  << " seconds" << '\n';

    #ifdef MEASURE
    std::cout << "Total;Iterations;Total_per_it;Flow;Energy" << '\n';
    std::cout << seconds << ";" << i << ";" << seconds / i << ";" << flow_time << ";" << energy_time << '\n';
    #endif
}

template<class M, class T>
__global__ void laplacian_brightness_CUDA(const M a, M r, const float delta) {
    const auto i = blockIdx.y * blockDim.y + threadIdx.y;
    const auto j = blockIdx.x * blockDim.x + threadIdx.x;
    const size_t nr = a.rows - 1;
    const size_t nc = a.cols - 1;

    if (i > nr || j > nc) {
        return;
    }
    else if (i > 0 && j > 0 && i < nr && j < nc) { // Central cases
        r.at(i,j) = a.at(i,j) + delta * laplacian<M,T>(a, i, j);
    }
    else if (i == 0 && j > 1 && j < nc) { // Upper boundary
        const T lapl = (a.at(0,j-1) - 4 * a.at(0,j) + a.at(1,j) + a.at(0,j+1));
        r.at(0,j) = a.at(0,j) + delta * lapl;
    }
    else if (i == nc && j > 1 && j < nc) { // Lower boundary
        const T lapl = (a.at(nr-1,j) + a.at(nr, j-1) - 4 * a.at(nr,j) + a.at(nr,j+1));
        r.at(nr,j) = a.at(nr,j) + delta * lapl;
    }
    else if (j == 0 && i > 1 && i < nr) { // Left boundary
        const T lapl = (a.at(i-1,0) - 4 * a.at(i,0) + a.at(i+1,0) + a.at(i,1));
        r.at(i,0) = a.at(i,0) + delta * lapl;
    }
    else if (j == nc && i > 1 && i < nr) { // Right boundary
        const T lapl = (a.at(i-1,nc) + a.at(i-1,nc-1) - 4 * a.at(i,nc) + a.at(i+1,nc));
        r.at(i,nc) = a.at(i,nc) + delta * lapl;
    }
    else if (i == 0 && j == 0) { // Upper-left corner
        r.at(0,0)  = a.at(0,0)  + delta * (-4 * a.at(0,0) + a.at(1,0) + a.at(0,1));
    }
    else if (i == 0 && j == nc) { // Upper-right corner
        r.at(0,nc) = a.at(0,nc) + delta * (a.at(0,nc-1) - 4 * a.at(0,nc) + a.at(1,nc));
    }
    else if (i == nr && j == 0) { // Lower-left corner
        r.at(nr,0) = a.at(nr,0) + delta * (a.at(nr-1,0) - 4 * a.at(nr,0) + a.at(nr,1));
    }
    else if (i == nr && j == nc) { // Lower-right corner
        r.at(nr,nc) = a.at(nr,nc) + delta * (a.at(nr-1,nc) + a.at(nr,nc-1) - 4 * a.at(nr,nc));
    }
}

void brightness_laplacian_CUDA(Matrix<float> & a, const float tolerance, const size_t max_iter) {
    using T = float;
    using VT = T;
    using M = Matrix_CUDA<VT>;

    auto start = std::chrono::system_clock::now();

    const float delta = 0.1;
    const size_t n = a.rows * a.cols;
    T c_diff = tolerance + 1;
    T c_energy;
    size_t i = 0;
    double flow_time = 0;
    double energy_time = 0;
    double clone_time = 0;

    VT * a_cuda_ptr;
    VT * r_cuda_ptr;
    thrust::device_vector<T> e(a.rows * a.cols);

    CUDA_CHECK_RETURN(cudaMalloc(&a_cuda_ptr, n * sizeof(VT)));
    CUDA_CHECK_RETURN(cudaMalloc(&r_cuda_ptr, n * sizeof(VT)));

    M a_cuda(a.rows, a.cols, a_cuda_ptr);
    M r_cuda(a.rows, a.cols, r_cuda_ptr);

    CUDA_CHECK_RETURN(cudaMemcpy(a_cuda.values, a.values, n * sizeof(VT), cudaMemcpyHostToDevice));

    const dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
    const dim3 dimGrid(ceil(a.cols / dimBlock.x), ceil(a.rows / dimBlock.y));

    const T i_energy = energy<M,T,VT>(a_cuda, e, dimBlock, dimGrid);

    std::cout << "Laplacian flow:" << '\n';
    std::cout << "\tInitial energy: " << i_energy << '\n';

    for (i = 0; i < max_iter; i++) {
        auto start = std::chrono::system_clock::now();
        laplacian_brightness_CUDA<M,VT><<<dimGrid, dimBlock>>>(a_cuda, r_cuda, delta);
        cudaDeviceSynchronize();
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_time = end-start;
        flow_time += elapsed_time.count();

        start = std::chrono::system_clock::now();
        a_cuda.swap(r_cuda);
        end = std::chrono::system_clock::now();
        elapsed_time = end-start;
        clone_time += elapsed_time.count();

        start = std::chrono::system_clock::now();
        c_energy = energy<M,T,VT>(a_cuda, e, dimBlock, dimGrid);
        cudaDeviceSynchronize();
        end = std::chrono::system_clock::now();
        elapsed_time = end-start;
        energy_time += elapsed_time.count();

        c_diff = c_energy / i_energy;
        std::cout << "\tIter " << i << ",\tenergy: " << c_energy << ",\tdifference: " << c_diff << '\n';
        if (c_diff < tolerance) {
            std::cout << "Convergence found on iteration " << i++ << " (difference = " << c_diff << ")" << '\n';
            break;
        }
    }
    if (i == max_iter && c_diff > tolerance) {
        std::cout << "Convergence not found in " << i << " iterations (difference = " << c_diff << ")" << '\n';
    }
    CUDA_CHECK_RETURN(cudaMemcpy(a.values, a_cuda.values, n * sizeof(VT), cudaMemcpyDeviceToHost));

    CUDA_CHECK_RETURN(cudaFree(a_cuda_ptr));
    CUDA_CHECK_RETURN(cudaFree(r_cuda_ptr));

    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_time = end-start;
    double seconds = elapsed_time.count();
    std::cout << "Laplacian diffusion time: " << seconds << " seconds" << '\n';
    std::cout << "\tTime per iteration: " << seconds / i << " seconds" << '\n';
    std::cout << "\tTime laplacian diffusion: " << flow_time << " (" << flow_time / i << ")" << " seconds" << '\n';
    std::cout << "\tTime clone: " << clone_time << " (" << clone_time / i << ")" << " seconds" << '\n';
    std::cout << "\tTime energy: " << energy_time<< " (" << energy_time / i << ")"  << " seconds" << '\n';

    #ifdef MEASURE
    std::cout << "Total;Iterations;Total_per_it;Flow;Energy" << '\n';
    std::cout << seconds << ";" << i << ";" << seconds / i << ";" << flow_time << ";" << energy_time << '\n';
    #endif
}
