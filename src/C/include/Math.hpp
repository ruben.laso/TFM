#ifndef MATH
#define MATH

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <chrono>
#ifdef ENABLE_OPENMP
#include <omp.h>
#endif

#include <fstream>

#include "Matrix.hpp"
#include "BilapSparse.hpp"
#include "Vec3.hpp"

#include "Sparse"
#include "SVD"

#include <opencv2/opencv.hpp>
#include <opencv2/core/eigen.hpp>

const double TOLERANCE = 1e-4;
const size_t MAX_ITER = 200;

template<class T>
T * solve_system(BilapSparse<T> & a, const T * b) {
    std::cout << "Solving system with Eigen::ConjugateGradient..." << '\n';

    Eigen::SparseMatrix<T,Eigen::RowMajor> A(a.dim, a.dim);
    A.reserve(Eigen::VectorXi::Constant(a.dim,a.max_nz));
    Eigen::VectorXf B(a.cols);
    Eigen::VectorXf r(a.cols);

    double time_copy = 0.0;
    double time_solve = 0.0;

    auto start = std::chrono::system_clock::now();
    // If there is enough memory reserved for each row/column
    // and each row/column is filled by only one thread,
    // then it is thread safe to fill the sparse matrix
    std::vector<T> values(a.max_nz);
    #pragma omp parallel for private(values)
    for (size_t i = 0; i < a.rows; i++) {
        std::vector<size_t> nz = a.nz(i, values);
        size_t ii = 0;
        for (auto j : nz) {
            A.insert(i,j) = values[ii++];
        }
        B(i) = T(b[i]);
        values.clear();
    }
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_time = end-start;
    time_copy = elapsed_time.count();
    std::cout << "Time spent copying the matrix: " << time_copy << '\n';
    A.makeCompressed();

    start = std::chrono::system_clock::now();
    Eigen::ConjugateGradient<Eigen::SparseMatrix<T>, Eigen::UpLoType::Lower|Eigen::UpLoType::Upper> solver(A); // Much faster than SimplicialLDLT
    solver.setTolerance(TOLERANCE); // Images work with integers, so low accuracy is required
    r = solver.solve(B);
    end = std::chrono::system_clock::now();
    elapsed_time = end-start;
    time_solve = elapsed_time.count();
    std::cout << "Time spent solving the system: " << time_solve << '\n';
    std::cout << "Number of iterations: " << solver.iterations() << '\n';


    start = std::chrono::system_clock::now();
    T * result = new T[a.cols];
    #pragma omp parallel for
    for (size_t i = 0; i < a.cols; i++) {
        result[i] = r(i);
    }
    end = std::chrono::system_clock::now();
    elapsed_time = end-start;
    const double total = elapsed_time.count() + time_copy + time_solve;

    #ifdef MEASURE
    std::cout << "Total;Iterations;Time_per_it" << '\n';
    std::cout << total << ";" << solver.iterations() << ";" << total / solver.iterations() << '\n';
    #endif

    return result;
}

template<class T>
inline T dot_prod(const T * __restrict__ a, const T * __restrict__ b, const size_t n) {
    T res = 0;
    #pragma omp parallel for reduction(+:res)
    for (size_t i = 0; i < n; i++) {
        res += a[i] * b[i];
    }
    return res;
}

template<class T>
inline T dot_prod(const T * a, const size_t n) {
    T res = 0;
    #pragma omp parallel for reduction(+:res)
    for (size_t i = 0; i < n; i++) {
        res += a[i] * a[i];
    }
    return res;
}

template<class T>
T * solve_system_CG(BilapSparse<T> & A, const T * b) {
    std::cout << "Solving system with Conjugate Gradient..." << '\n';

    auto start = std::chrono::system_clock::now();

    const size_t n = A.dim;
    const double threshold = TOLERANCE * sqrt(dot_prod(b, n));

    T * x = new T[n];
    // Initial guess -> x = b;
    std::copy(b, b + n, x);

    // T * ap = A * x;
    T * ap = new T[n];
    A.prod_vec(x, ap);
    T * r = new T[n];
    T * p = new T[n];
    #pragma omp parallel for
    for (size_t i = 0; i < n; i++) {
        r[i] = b[i] - ap[i];
        p[i] = r[i];
    }

    T rsold = dot_prod(r, n);

    if (sqrt(rsold) < threshold) {
        return x;
    }

    size_t i;
    // Loop solving the system
    for (i = 0; i < MAX_ITER; i++) {
        A.prod_vec(p, ap); // ap = A * p;

        T alpha = rsold / dot_prod(p, ap, n);

        T rsnew = 0;
        #pragma omp parallel for reduction(+:rsnew)
        for (size_t j = 0; j < n; j++) {
            x[j] += alpha * p[j];
            r[j] -= alpha * ap[j];
            rsnew += r[j] * r[j];
        }
        if (sqrt(rsnew) < threshold) {
            std::cout << "Convergence found in " << i << " iterations." << '\n';
            break;
        }

        T beta = rsnew / rsold;
        #pragma omp parallel for
        for (size_t j = 0; j < n; j++) {
            p[j] = r[j] + beta * p[j];
        }
        rsold = rsnew;
    }
    delete[] r;
    delete[] p;
    delete[] ap;
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_time = end-start;

    std::cout << "Number of iterations: " << i << '\n';

    #ifdef MEASURE
    std::cout << "Total;Iterations;Total_per_it" << '\n';
    std::cout << elapsed_time.count() << ";" << i << ";" << elapsed_time.count() / i << '\n';
    #endif

    return x;
}

template<class M, class T>
inline T dx_c(const M & a, const size_t i, const size_t j) {
    return (a.at(i,j+1) - a.at(i,j-1)) * 0.5;
}

template<class M, class T>
inline T dy_c(const M & a, const size_t i, const size_t j) {
    return (a.at(i+1,j) - a.at(i-1,j)) * 0.5;
}

template<class M, class T>
inline T dx_f(const M & a, const size_t i, const size_t j) {
    return a.at(i,j+1) - a.at(i,j);
}

template<class M, class T>
inline T dy_f(const M & a, const size_t i, const size_t j) {
    return a.at(i+1,j) - a.at(i,j);
}

template<class M, class T>
inline T dx_b(const M & a, const size_t i, const size_t j) {
    return a.at(i,j) - a.at(i,j-1);
}

template<class M, class T>
inline T dy_b(const M & a, const size_t i, const size_t j) {
    return a.at(i,j) - a.at(i-1,j);
}

template<class M, class T>
inline T laplacian(const M & a, const size_t i, const size_t j) {
    // 5 point scheme
    return  (a.at(i-1,j) +
            a.at(i,j-1) -
            4 * a.at(i,j) +
            a.at(i+1,j) +
            a.at(i,j+1));
}

inline float norm2_pow2(const cv::Vec3d & dx, const cv::Vec3d & dy) {
    return dx(0)*dx(0) + dy(0)*dy(0) + dx(1)*dx(1) + dy(1)*dy(1) + dx(2)*dx(2) + dy(2)*dy(2);
}

inline float norm2_pow2(const cv::Vec3f & dx, const cv::Vec3f & dy) {
    return dx(0)*dx(0) + dy(0)*dy(0) + dx(1)*dx(1) + dy(1)*dy(1) + dx(2)*dx(2) + dy(2)*dy(2);
}

template<class T>
inline float norm2_pow2(const Vec3<T> & dx, const Vec3<T> & dy) {
    return dx(0)*dx(0) + dy(0)*dy(0) + dx(1)*dx(1) + dy(1)*dy(1) + dx(2)*dx(2) + dy(2)*dy(2);
}

template<class T>
inline float norm2_pow2(const T & dx, const T & dy) {
    return (dx*dx + dy*dy);
}

inline float norm2(const cv::Vec3d & dx, const cv::Vec3d & dy) {
    return sqrt(dx(0)*dx(0) + dy(0)*dy(0) + dx(1)*dx(1) + dy(1)*dy(1) + dx(2)*dx(2) + dy(2)*dy(2));
}

inline float norm2(const cv::Vec3f & dx, const cv::Vec3f & dy) {
    return sqrt(dx(0)*dx(0) + dy(0)*dy(0) + dx(1)*dx(1) + dy(1)*dy(1) + dx(2)*dx(2) + dy(2)*dy(2));
}

template<class T>
inline float norm2(const Vec3<T> & dx, const Vec3<T> & dy) {
    return sqrt(dx(0)*dx(0) + dy(0)*dy(0) + dx(1)*dx(1) + dy(1)*dy(1) + dx(2)*dx(2) + dy(2)*dy(2));
}

template<class T>
inline float norm2(const T & v) {
    return sqrt(v(0)*v(0) + v(1)*v(1) + v(2)*v(2));
}

template<class T>
inline float norm2(const T & dx, const T & dy) {
    return sqrt(dx*dx + dy*dy);
}

template<class M, class VT>
inline float gradient_norm(const M & a, const size_t i, const size_t j) {
    return norm2(dx_c<M,VT>(a, i, j), dy_c<M,VT>(a, i, j));
}

template<class M, class T>
inline void isotropic_diffusion(const M & a, M & r, const float delta) {
    const size_t nr = a.rows - 1;
    const size_t nc = a.cols - 1;
    T dx, dy, lapl, res;

    #pragma omp parallel private(dx, dy, lapl, res) \
        firstprivate(nr, nc, delta) shared(a, r) default(none)
    {
        // Upper boundary
        #pragma omp for nowait
        for (size_t j = 1; j < nc; j++) {
            dx = dx_c<M,T>(a, 0, j);
            dy = dy_f<M,T>(a, 0, j);
            lapl =
                // a.at(-1,j) + // this is asumed zero
                a.at(0,j-1) -
                4 * a.at(0,j) +
                a.at(1,j) +
                a.at(0,j+1);
            res = a.at(0,j) + delta * (lapl + a.at(0,j)*norm2_pow2(dx, dy));
            r.at(0,j) = res / norm2(res);
        }
        // Lower boundary
        #pragma omp for nowait
        for (size_t j = 1; j < nc; j++) {
            dx = dx_c<M,T>(a, nr, j);
            dy = dy_b<M,T>(a, nr, j);
            lapl =
                a.at(nr-1,j) +
                a.at(nr,j-1) -
                4 * a.at(nr,j) +
                // a.at(nr+1,j) + // this is asumed zero
                a.at(nr,j+1);
            res = a.at(nr,j) + delta * (lapl + a.at(nr,j)*norm2_pow2(dx, dy));
            r.at(nr,j) = res / norm2(res);
        }
        // Left boundary
        #pragma omp for nowait
        for (size_t i = 1; i < nr; i++) {
            // Left boundary
            dx = dx_f<M,T>(a, i, 0);
            dy = dy_c<M,T>(a, i, 0);
            lapl =
                a.at(i-1,0) +
                // a.at(i,-1) - // this is asumed zero
                4 * a.at(i,0) +
                a.at(i+1,0) +
                a.at(i,1);
            res = a.at(i,0) + delta * (lapl + a.at(i,0)*norm2_pow2(dx, dy));
            r.at(i,0) = res / norm2(res);
        }
        // Right boundary
        #pragma omp for nowait
        for (size_t i = 1; i < nr; i++) {
            dx = dx_b<M,T>(a, i, nc);
            dy = dy_c<M,T>(a, i, nc);
            lapl =
                a.at(i-1,nc) +
                a.at(i,nc-1) -
                4 * a.at(i,nc) +
                a.at(i+1,nc);
                // a.at(i,nc+1); // this is asumed zero
            res = a.at(i,nc) + delta * (lapl + a.at(i,nc)*norm2_pow2(dx, dy));
            r.at(i,nc) = res / norm2(res);
        }
        // Central cases
        #pragma omp for nowait
        for (size_t i = 1; i < nr; i++) {
            for (size_t j = 1; j < nc; j++) {
                dx = dx_c<M,T>(a, i, j);
                dy = dy_c<M,T>(a, i, j);
                lapl = laplacian<M,T>(a, i, j);
                res = a.at(i,j) + delta * (lapl + a.at(i,j)*norm2_pow2(dx,dy));
                r.at(i,j) = res / norm2(res);
            }
        }
        #pragma omp single
        {
            // Position 0,0
            dx = dx_f<M,T>(a, 0, 0);
            dy = dy_f<M,T>(a, 0, 0);
            lapl =
                // a.at(-1,0) + // this is asumed zero
                // a.at(0,-1) - // this is asumed zero
                - 4 * a.at(0,0)
                + a.at(1,0)
                + a.at(0,1);
            res = a.at(0,0) + delta * (lapl + a.at(0,0)*norm2_pow2(dx, dy));
            r.at(0,0) = res / norm2(res);

            // Position 0,nc
            dx = dx_b<M,T>(a, 0, nc);
            dy = dy_f<M,T>(a, 0, nc);
            lapl =
                // a.at(-1,nc) + // this is asumed zero
                a.at(0,nc-1) - // this is asumed zero
                4 * a.at(0,nc) +
                a.at(1,nc);
                // a.at(0,nc+1); // this is asumed zero
            res = a.at(0,nc) + delta * (lapl + a.at(0,nc)*norm2_pow2(dx, dy));
            r.at(0,nc) = res / norm2(res);

            // Position nr,0
            dx = dx_f<M,T>(a, nr, 0);
            dy = dy_b<M,T>(a, nr, 0);
            lapl =
                a.at(nr-1,0) +
                // a.at(nr,-1) - // this is asumed zero
                4 * a.at(nr,0) +
                // a.at(nr+1,0) + // this is asumed zero
                a.at(nr,1);
            res = a.at(nr,0) + delta * (lapl + a.at(nr,0)*norm2_pow2(dx, dy));
            r.at(nr,0) = res / norm2(res);

            // Position nr,nc
            dx = dx_b<M,T>(a, nr, nc);
            dy = dy_b<M,T>(a, nr, nc);
            lapl =
                a.at(nr-1,nc) +
                a.at(nr,nc-1) -
                4 * a.at(nr,nc);
                // a.at(nr+1,nc) + // this is asumed zero
                // a.at(nr,nc+1); // this is asumed zero
            res = a.at(nr,nc) + delta * (lapl + a.at(nr,nc)*norm2_pow2(dx, dy));
            r.at(nr,nc) = res / norm2(res);
        }
    }
}

template<class M, class T>
inline void anisotropic_diffusion(const M & a, M & r, const float delta) {
    const size_t nr = a.rows - 1;
    const size_t nc = a.cols - 1;

    #pragma omp parallel firstprivate(delta) shared(a, r) default(none)
    {
        // Boundaries
        #pragma omp for nowait
        for (size_t j = 1; j < nc; j++) { // i = 0, upper boundary
            const T gx1 = dx_b<M,T>(a, 0, j+1) / (0 != norm2(dx_b<M,T>(a, 0, j+1), dy_f<M,T>(a, 0, j+1)) ? norm2(dx_b<M,T>(a, 0, j+1), dy_f<M,T>(a, 0, j+1)) : 1);
            const T gy1 = dy_c<M,T>(a, 1, j)   / (0 != norm2(dx_c<M,T>(a, 1, j),   dy_c<M,T>(a, 1, j))   ? norm2(dx_c<M,T>(a, 1, j),   dy_c<M,T>(a, 1, j))   : 1);

            const T gx2 = dx_f<M,T>(a, 0, j-1) / (0 != norm2(dx_f<M,T>(a, 0, j-1), dy_f<M,T>(a, 0, j-1)) ? norm2(dx_f<M,T>(a, 0, j-1), dy_f<M,T>(a, 0, j-1)) : 1);
            const T gy  = dy_f<M,T>(a, 0, j)   / (0 != norm2(dx_c<M,T>(a, 0, j),   dy_f<M,T>(a, 0, j))   ? norm2(dx_c<M,T>(a, 0, j),   dy_f<M,T>(a, 0, j))   : 1);

            const T dx = (gx1 - gx2) * 0.5;
            const T dy = gy1 - gy;

            const T _div = dx + dy;
            const T res = a.at(0,j) + delta * (_div + a.at(0,j) * norm2(dx_c<M,T>(a, 0, j), dy_f<M,T>(a, 0, j)));
            r.at(0,j) = res / norm2(res);
        }
        #pragma omp for nowait
        for (size_t j = 1; j < nc; j++) { // i = nr, lower boundary
            const T gx1 = dx_b<M,T>(a, nr, j+1) / (0 != norm2(dx_b<M,T>(a, nr, j+1), dy_b<M,T>(a, nr, j+1)) ? norm2(dx_b<M,T>(a, nr, j+1), dy_b<M,T>(a, nr, j+1)) : 1);
            const T gy  = dy_b<M,T>(a, nr, j)   / (0 != norm2(dx_c<M,T>(a, nr, j),   dy_b<M,T>(a, nr, j))   ? norm2(dx_c<M,T>(a, nr, j),   dy_b<M,T>(a, nr, j))   : 1);

            const T gx2 = dx_f<M,T>(a, nr, j-1) / (0 != norm2(dx_f<M,T>(a, nr, j-1), dy_b<M,T>(a, nr, j-1)) ? norm2(dx_f<M,T>(a, nr, j-1), dy_b<M,T>(a, nr, j-1)) : 1);
            const T gy2 = dy_c<M,T>(a, nr-1, j) / (0 != norm2(dx_c<M,T>(a, nr-1, j), dy_c<M,T>(a, nr-1, j)) ? norm2(dx_c<M,T>(a, nr-1, j), dy_c<M,T>(a, nr-1, j)) : 1);

            const T dx = (gx1 - gx2) * 0.5;
            const T dy = gy - gy2;

            const T _div = dx + dy;
            const T res = a.at(nr,j) + delta * (_div + a.at(nr,j) * norm2(dx_c<M,T>(a, nr, j), dy_b<M,T>(a, nr, j)));
            r.at(nr,j) = res / norm2(res);
        }
        #pragma omp for nowait
        for (size_t i = 1; i < nr; i++) { // j = 0, left boundary
            const T gx1 = dx_c<M,T>(a, i, 1)   / (0 != norm2(dx_c<M,T>(a, i, 1),   dy_c<M,T>(a, i, 1))   ? norm2(dx_c<M,T>(a, i, 1),   dy_c<M,T>(a, i, 1))   : 1);
            const T gy1 = dy_b<M,T>(a, i+1, 0) / (0 != norm2(dx_f<M,T>(a, i+1, 0), dy_b<M,T>(a, i+1, 0)) ? norm2(dx_f<M,T>(a, i+1, 0), dy_b<M,T>(a, i+1, 0)) : 1);

            const T gx  = dx_f<M,T>(a, i, 0)   / (0 != norm2(dx_f<M,T>(a, i, 0),   dy_c<M,T>(a, i, 0))   ? norm2(dx_f<M,T>(a, i, 0),   dy_c<M,T>(a, i, 0))   : 1);
            const T gy2 = dy_f<M,T>(a, i-1, 0) / (0 != norm2(dx_f<M,T>(a, i-1, 0), dy_f<M,T>(a, i-1, 0)) ? norm2(dx_f<M,T>(a, i-1, 0), dy_f<M,T>(a, i-1, 0)) : 1);

            const T dx = gx1 - gx;
            const T dy = (gy1 - gy2) * 0.5;

            const T _div = dx + dy;
            const T res = a.at(i,0) + delta * (_div + a.at(i,0) * norm2(dx_f<M,T>(a, i, 0), dy_c<M,T>(a, i, 0)));
            r.at(i,0) = res / norm2(res);
        }
        #pragma omp for nowait
        for (size_t i = 1; i < nr; i++) { // j = nc, right boundary
            const T gx  = dx_b<M,T>(a, i, nc)   / (0 != norm2(dx_b<M,T>(a, i, nc),   dy_c<M,T>(a, i, nc))   ? norm2(dx_b<M,T>(a, i, nc),   dy_c<M,T>(a, i, nc))   : 1);
            const T gy1 = dy_b<M,T>(a, i+1, nc) / (0 != norm2(dx_b<M,T>(a, i+1, nc), dy_b<M,T>(a, i+1, nc)) ? norm2(dx_b<M,T>(a, i+1, nc), dy_b<M,T>(a, i+1, nc)) : 1);

            const T gx2 = dx_c<M,T>(a, i, nc-1) / (0 != norm2(dx_c<M,T>(a, i, nc-1), dy_c<M,T>(a, i, nc-1)) ? norm2(dx_c<M,T>(a, i, nc-1), dy_c<M,T>(a, i, nc-1)) : 1);
            const T gy2 = dy_f<M,T>(a, i-1, nc) / (0 != norm2(dx_b<M,T>(a, i-1, nc), dy_f<M,T>(a, i-1, nc)) ? norm2(dx_b<M,T>(a, i-1, nc), dy_f<M,T>(a, i-1, nc)) : 1);

            const T dx = gx - gx2;
            const T dy = (gy1 - gy2) * 0.5;

            const T _div = dx + dy;
            const T res = a.at(i,nc) + delta * (_div + a.at(i,nc) * norm2(dx_b<M,T>(a, i, nc), dy_c<M,T>(a, i, nc)));
            r.at(i,nc) = res / norm2(res);
        }
        #pragma omp task
        { // Upper-left corner
            const T gx1 = dx_c<M,T>(a, 0, 1) / (0 != norm2(dx_c<M,T>(a, 0, 1), dy_f<M,T>(a, 0, 1)) ? norm2(dx_c<M,T>(a, 0, 1), dy_f<M,T>(a, 0, 1)) : 1);
            const T gy1 = dy_c<M,T>(a, 1, 0) / (0 != norm2(dx_f<M,T>(a, 1, 0), dy_c<M,T>(a, 1, 0)) ? norm2(dx_f<M,T>(a, 1, 0), dy_c<M,T>(a, 1, 0)) : 1);

            const T gx2 = dx_f<M,T>(a, 0, 0) / (0 != norm2(dx_f<M,T>(a, 0, 0), dy_f<M,T>(a, 0, 0)) ? norm2(dx_f<M,T>(a, 0, 0), dy_f<M,T>(a, 0, 0)) : 1);
            const T gy2 = dy_f<M,T>(a, 0, 0) / (0 != norm2(dx_f<M,T>(a, 0, 0), dy_f<M,T>(a, 0, 0)) ? norm2(dx_f<M,T>(a, 0, 0), dy_f<M,T>(a, 0, 0)) : 1);

            const T dx = gx1 - gx2;
            const T dy = gy1 - gy2;

            const T _div = dx + dy;
            const T res = a.at(0,0) + delta * (_div + a.at(0,0) * norm2(dx_f<M,T>(a, 0, 0), dy_f<M,T>(a, 0, 0)));
            r.at(0,0) = res / norm2(res);
        }
        #pragma omp task
        { // Upper-right corner
            const T gx1 = dx_b<M,T>(a, 0, nc) / (0 != norm2(dx_b<M,T>(a, 0, nc), dy_f<M,T>(a, 0, nc)) ? norm2(dx_b<M,T>(a, 0, nc), dy_f<M,T>(a, 0, nc)) : 1);
            const T gy1 = dy_c<M,T>(a, 1, nc) / (0 != norm2(dx_b<M,T>(a, 1, nc), dy_c<M,T>(a, 1, nc)) ? norm2(dx_b<M,T>(a, 1, nc), dy_c<M,T>(a, 1, nc)) : 1);

            const T gx2 = dx_c<M,T>(a, 0, nc-1) / (0 != norm2(dx_c<M,T>(a, 0, nc-1), dy_f<M,T>(a, 0, nc-1)) ? norm2(dx_c<M,T>(a, 0, nc-1), dy_f<M,T>(a, 0, nc-1)) : 1);
            const T gy2 = dy_f<M,T>(a, 0, nc)   / (0 != norm2(dx_b<M,T>(a, 0, nc),   dy_f<M,T>(a, 0, nc))   ? norm2(dx_b<M,T>(a, 0, nc),   dy_f<M,T>(a, 0, nc))   : 1);

            const T dx = gx1 - gx2;
            const T dy = gy1 - gy2;

            const T _div = dx + dy;
            const T res = a.at(0,nc) + delta * (_div + a.at(0,nc) * norm2(dx_b<M,T>(a, 0, nc), dy_f<M,T>(a, 0, nc)));
            r.at(0,nc) = res / norm2(res);
        }
        #pragma omp task
        { // Lower-left corner
            const T gx1 = dx_c<M,T>(a, nr, 1) / (0 != norm2(dx_c<M,T>(a, nr, 1), dy_b<M,T>(a, nr, 1)) ? norm2(dx_c<M,T>(a, nr, 1), dy_b<M,T>(a, nr, 1)) : 1);
            const T gy1 = dy_b<M,T>(a, nr, 0) / (0 != norm2(dx_f<M,T>(a, nr, 0), dy_b<M,T>(a, nr, 0)) ? norm2(dx_f<M,T>(a, nr, 0), dy_b<M,T>(a, nr, 0)) : 1);

            const T gx2 = dx_f<M,T>(a, nr, 0)   / (0 != norm2(dx_f<M,T>(a, nr, 0),   dy_b<M,T>(a, nr, 0))   ? norm2(dx_f<M,T>(a, nr, 0),   dy_b<M,T>(a, nr, 0))   : 1);
            const T gy2 = dy_c<M,T>(a, nr-1, 0) / (0 != norm2(dx_f<M,T>(a, nr-1, 0), dy_c<M,T>(a, nr-1, 0)) ? norm2(dx_f<M,T>(a, nr-1, 0), dy_c<M,T>(a, nr-1, 0)) : 1);

            const T dx = gx1 - gx2;
            const T dy = gy1 - gy2;

            const T _div = dx + dy;
            const T res = a.at(nr,0) + delta * (_div + a.at(nr,0) * norm2(dx_f<M,T>(a, nr, 0), dy_b<M,T>(a, nr, 0)));
            r.at(nr,0) = res / norm2(res);
        }
        #pragma omp task
        { // Lower-right corner
            const T gx1 = dx_b<M,T>(a, nr, nc) / (0 != norm2(dx_b<M,T>(a, nr, nc), dy_b<M,T>(a, nr, nc)) ? norm2(dx_b<M,T>(a, nr, nc), dy_b<M,T>(a, nr, nc)) : 1);
            const T gy1 = dy_b<M,T>(a, nr, nc) / (0 != norm2(dx_b<M,T>(a, nr, nc), dy_b<M,T>(a, nr, nc)) ? norm2(dx_b<M,T>(a, nr, nc), dy_b<M,T>(a, nr, nc)) : 1);

            const T gx2 = dx_c<M,T>(a, nr, nc-1) / (0 != norm2(dx_c<M,T>(a, nr, nc-1), dy_b<M,T>(a, nr, nc-1)) ? norm2(dx_c<M,T>(a, nr, nc-1), dy_b<M,T>(a, nr, nc-1)) : 1);
            const T gy2 = dy_c<M,T>(a, nr-1, nc) / (0 != norm2(dx_b<M,T>(a, nr-1, nc), dy_c<M,T>(a, nr-1, nc)) ? norm2(dx_b<M,T>(a, nr-1, nc), dy_c<M,T>(a, nr-1, nc)) : 1);

            const T dx = gx1 - gx2;
            const T dy = gy1 - gy2;

            const T _div = dx + dy;
            const T res = a.at(nr,nc) + delta * (_div + a.at(nr,nc) * norm2(dx_b<M,T>(a, nr, nc), dy_b<M,T>(a, nr, nc)));
            r.at(nr,nc) = res / norm2(res);
        }

        // Inner Boundaries
        #pragma omp for nowait
        for (size_t j = 2; j < nc-1; j++) { // i = 1, upper-inner boundary
            const T gx1 = dx_c<M,T>(a, 1, j+1) / (0 != gradient_norm<M,T>(a, 1, j+1) ? gradient_norm<M,T>(a, 1, j+1) : 1);
            const T gy1 = dy_c<M,T>(a, 2, j)   / (0 != gradient_norm<M,T>(a, 2, j)   ? gradient_norm<M,T>(a, 2, j)   : 1);

            const T gx2 = dx_c<M,T>(a, 1, j-1) / (0 != gradient_norm<M,T>(a, 1, j-1) ? gradient_norm<M,T>(a, 1, j-1) : 1);
            const T gy2 = dy_f<M,T>(a, 0, j)   / (0 != norm2(dx_c<M,T>(a, 0, j), dy_f<M,T>(a, 0, j)) ? norm2(dx_c<M,T>(a, 0, j), dy_f<M,T>(a, 0, j)) : 1); // gradient_norm<M,T>(a, 0, j);

            const T dx = (gx1 - gx2) * 0.5;
            const T dy = (gy1 - gy2) * 0.5;

            const T _div = dx + dy;
            const T res = a.at(1,j) + delta * (_div + a.at(1,j) * gradient_norm<M,T>(a, 1, j));
            r.at(1,j) = res / norm2(res);
        }
        #pragma omp for nowait
        for (size_t j = 2; j < nc-1; j++) { // i = nr-1, lower-inner boundary
            const T gx1 = dx_c<M,T>(a, nr-1, j+1) / (0 != gradient_norm<M,T>(a, nr-1, j+1) ? gradient_norm<M,T>(a, nr-1, j+1) : 1);
            const T gy1 = dy_b<M,T>(a, nr, j)     / (0 != norm2(dx_c<M,T>(a, nr, j), dy_b<M,T>(a, nr, j)) ? norm2(dx_c<M,T>(a, nr, j), dy_b<M,T>(a, nr, j)) : 1);

            const T gx2 = dx_c<M,T>(a, nr-1, j-1) / (0 != gradient_norm<M,T>(a, 1, j-1)  ? gradient_norm<M,T>(a, 1, j-1)  : 1);
            const T gy2 = dy_c<M,T>(a, nr-2, j)   / (0 != gradient_norm<M,T>(a, nr-2, j) ? gradient_norm<M,T>(a, nr-2, j) : 1);

            const T dx = (gx1 - gx2) * 0.5;
            const T dy = (gy1 - gy2) * 0.5;

            const T _div = dx + dy;
            const T res = a.at(nr-1,j) + delta * (_div + a.at(nr-1,j) * gradient_norm<M,T>(a, nr-1, j));
            r.at(nr-1,j) = res / norm2(res);
        }
        #pragma omp for nowait
        for (size_t i = 2; i < nr-1; i++) { // j = 1, left-inner boundary
            const T gx1 = dx_c<M,T>(a, i, 2)   / (0 != gradient_norm<M,T>(a, i, 2)   ? gradient_norm<M,T>(a, i, 2)   : 1);
            const T gy1 = dy_c<M,T>(a, i+1, 1) / (0 != gradient_norm<M,T>(a, i+1, 1) ? gradient_norm<M,T>(a, i+1, 1) : 1);

            const T gx2 = dx_f<M,T>(a, i, 0)   / (0 != norm2(dx_f<M,T>(a, i, 0), dy_c<M,T>(a, i, 0)) ? norm2(dx_f<M,T>(a, i, 0), dy_c<M,T>(a, i, 0)) : 1);
            const T gy2 = dy_c<M,T>(a, i-1, 1) / (0 != gradient_norm<M,T>(a, i-1, 1) ? gradient_norm<M,T>(a, i-1, 1) : 1);

            const T dx = (gx1 - gx2) * 0.5;
            const T dy = (gy1 - gy2) * 0.5;

            const T _div = dx + dy;
            const T res = a.at(i,1) + delta * (_div + a.at(i,1) * gradient_norm<M,T>(a, i, 1));
            r.at(i,1) = res / norm2(res);
        }
        #pragma omp for nowait
        for (size_t i = 2; i < nr-1; i++) { // j = nc-1, right-inner boundary
            const T gx1 = dx_b<M,T>(a, i, nc)     / (0 != norm2(dx_b<M,T>(a, i, nc), dy_c<M,T>(a, i, nc)) ? norm2(dx_b<M,T>(a, i, nc), dy_c<M,T>(a, i, nc)) : 1);
            const T gy1 = dy_c<M,T>(a, i+1, nc-1) / (0 != gradient_norm<M,T>(a, i+1, nc-1) ? gradient_norm<M,T>(a, i+1, nc-1) : 1);

            const T gx2 = dx_c<M,T>(a, i, nc-2)   / (0 != gradient_norm<M,T>(a, i, nc-2)   ? gradient_norm<M,T>(a, i, nc-2)   : 1);
            const T gy2 = dy_c<M,T>(a, i-1, nc-1) / (0 != gradient_norm<M,T>(a, i-1, nc-1) ? gradient_norm<M,T>(a, i-1, nc-1) : 1);

            const T dx = (gx1 - gx2) * 0.5;
            const T dy = (gy1 - gy2) * 0.5;

            const T _div = dx + dy;
            const T res = a.at(i,nc) + delta * (_div + a.at(i,nc) * gradient_norm<M,T>(a, i, nc-1));
            r.at(i,nc-1) = res / norm2(res);
        }
        // Compute divergences
        #pragma omp for nowait
        for (size_t i = 2; i < nr-1; i++) {
            for (size_t j = 2; j < nc-1; j++) {
                const T gx1 = dx_c<M,T>(a, i, j+1) / (0 != gradient_norm<M,T>(a, i, j+1) ? gradient_norm<M,T>(a, i, j+1) : 1);
                const T gy1 = dy_c<M,T>(a, i+1, j) / (0 != gradient_norm<M,T>(a, i+1, j) ? gradient_norm<M,T>(a, i+1, j) : 1);

                const T gx2 = dx_c<M,T>(a, i, j-1) / (0 != gradient_norm<M,T>(a, i, j-1) ? gradient_norm<M,T>(a, i, j-1) : 1);
                const T gy2 = dy_c<M,T>(a, i-1, j) / (0 != gradient_norm<M,T>(a, i-1, j) ? gradient_norm<M,T>(a, i-1, j) : 1);

                const T dx = (gx1 - gx2) * 0.5;
                const T dy = (gy1 - gy2) * 0.5;

                const T _div = dx + dy;
                const T res = a.at(i,j) + delta * (_div + a.at(i,j) * gradient_norm<M,T>(a, i, j));
                r.at(i,j) = res / norm2(res);
            }
        }
        #pragma omp task
        { // Upper-left inner corner
            const T gx1 = dx_c<M,T>(a, 1, 2) / (0 != gradient_norm<M,T>(a, 1, 2) ? gradient_norm<M,T>(a, 1, 2) : 1);
            const T gy1 = dy_c<M,T>(a, 2, 1) / (0 != gradient_norm<M,T>(a, 2, 1) ? gradient_norm<M,T>(a, 2, 1) : 1);

            const T gx2 = dx_f<M,T>(a, 1, 0) / (0 != norm2(dx_f<M,T>(a, 1, 0), dy_c<M,T>(a, 1, 0)) ? norm2(dx_f<M,T>(a, 1, 0), dy_c<M,T>(a, 1, 0)) : 1);
            const T gy2 = dy_f<M,T>(a, 0, 1) / (0 != norm2(dx_c<M,T>(a, 0, 1), dy_f<M,T>(a, 0, 1)) ? norm2(dx_c<M,T>(a, 0, 1), dy_f<M,T>(a, 0, 1)) : 1);

            const T dx = (gx1 - gx2) * 0.5;
            const T dy = (gy1 - gy2) * 0.5;

            const T _div = dx + dy;
            const T res = a.at(1,1) + delta * (_div + a.at(1,1) * gradient_norm<M,T>(a, 1, 1));
            r.at(1,1) = res / norm2(res);
        }
        #pragma omp task
        { // Upper-right inner corner
            const T gx1 = dx_b<M,T>(a, 1, nc)   / (0 != norm2(dx_b<M,T>(a, 1, nc), dy_c<M,T>(a, 1, nc)) ? norm2(dx_b<M,T>(a, 1, nc), dy_c<M,T>(a, 1, nc)) : 1);
            const T gy1 = dy_c<M,T>(a, 2, nc-1) / (0 != gradient_norm<M,T>(a, 2, nc-1) ? gradient_norm<M,T>(a, 2, nc-1) : 1);

            const T gx2 = dx_c<M,T>(a, 1, nc-2) / (0 != gradient_norm<M,T>(a, 1, nc-2) ? gradient_norm<M,T>(a, 1, nc-2) : 1);
            const T gy2 = dy_f<M,T>(a, 0, nc-1) / (0 != norm2(dx_c<M,T>(a, 0, nc-1), dy_f<M,T>(a, 0, nc-1)) ? norm2(dx_c<M,T>(a, 0, nc-1), dy_f<M,T>(a, 0, nc-1)) : 1);

            const T dx = (gx1 - gx2) * 0.5;
            const T dy = (gy1 - gy2) * 0.5;

            const T _div = dx + dy;
            const T res = a.at(1,nc-1) + delta * (_div + a.at(1,nc-1) * gradient_norm<M,T>(a, 1, nc-1));
            r.at(1,nc-1) = res / norm2(res);
        }
        #pragma omp task
        { // Lower-left inner corner
            const T gx1 = dx_c<M,T>(a, nr-1, 2) / (0 != gradient_norm<M,T>(a, nr-1, 2) ? gradient_norm<M,T>(a, nr-1, 2) : 1);
            const T gy1 = dy_b<M,T>(a, nr, 1)   / (0 != norm2(dx_c<M,T>(a, nr, 1), dy_b<M,T>(a, nr, 1)) ? norm2(dx_c<M,T>(a, nr, 1), dy_b<M,T>(a, nr, 1)) : 1);

            const T gx2 = dx_f<M,T>(a, nr-1, 0) / (0 != norm2(dx_f<M,T>(a, nr-1, 0), dy_c<M,T>(a, nr-1, 0)) ? norm2(dx_f<M,T>(a, nr-1, 0), dy_c<M,T>(a, nr-1, 0)) : 1);
            const T gy2 = dy_c<M,T>(a, nr-2, 1) / (0 != gradient_norm<M,T>(a, nr-2, 1) ? gradient_norm<M,T>(a, nr-2, 1) : 1);

            const T dx = (gx1 - gx2) * 0.5;
            const T dy = (gy1 - gy2) * 0.5;

            const T _div = dx + dy;
            const T res = a.at(nr-1,1) + delta * (_div + a.at(nr-1,1) * gradient_norm<M,T>(a, nr-1,1));
            r.at(nr-1,1) = res / norm2(res);
        }
        #pragma omp task
        { // Lower-right inner corner
            const T gx1 = dx_b<M,T>(a, nr-1, nc) / (0 != norm2(dx_b<M,T>(a, nr-1, nc), dy_c<M,T>(a, nr-1, nc)) ? norm2(dx_b<M,T>(a, nr-1, nc), dy_c<M,T>(a, nr-1, nc)) : 1);
            const T gy1 = dy_b<M,T>(a, nr, nc-1) / (0 != norm2(dx_c<M,T>(a, nr, nc-1), dy_b<M,T>(a, nr, nc-1)) ? norm2(dx_c<M,T>(a, nr, nc-1), dy_b<M,T>(a, nr, nc-1)) : 1);

            const T gx2 = dx_c<M,T>(a, nr-1, nc-2) / (0 != gradient_norm<M,T>(a, nr-1, nc-2) ? gradient_norm<M,T>(a, nr-1, nc-2) : 1);
            const T gy2 = dy_c<M,T>(a, nr-2, nc-1) / (0 != gradient_norm<M,T>(a, nr-2, nc-1) ? gradient_norm<M,T>(a, nr-2, nc-1) : 1);

            const T dx = (gx1 - gx2) * 0.5;
            const T dy = (gy1 - gy2) * 0.5;

            const T _div = dx + dy;
            const T res = a.at(nr-1,nc-1) + delta * (_div + a.at(nr-1,nc-1) * gradient_norm<M,T>(a, nr-1, nc-1));
            r.at(nr-1,nc-1) = res / norm2(res);
        }
    }
}

template<class M, class T, class VT>
inline T energy(const M & a, T * e) {
    const size_t nr = a.rows - 1;
    const size_t nc = a.cols - 1;
    T energy = 0.0;
    const size_t cols = a.cols;

    #pragma omp parallel firstprivate(e) shared(a, energy) default(none)
    {
        #pragma omp for nowait
        for (size_t j = 1; j < nc; j++) {
            e[0 * cols + j] = norm2_pow2(dx_c<M,VT>(a, 0, j), dy_f<M,VT>(a, 0, j));
        }
        #pragma omp for nowait
        for (size_t j = 1; j < nc; j++) {
            e[nr * cols + j] = norm2_pow2(dx_c<M,VT>(a, nr, j), dy_b<M,VT>(a, nr, j));
        }
        #pragma omp for nowait
        #pragma ivdep
        for (size_t i = 1; i < nr; i++) {
            e[i * cols + 0] = norm2_pow2(dx_f<M,VT>(a, i, 0), dy_c<M,VT>(a, i, 0));
            #pragma ivdep
            for (size_t j = 1; j < nc; j++) {
                e[i * cols + j] = norm2_pow2(dx_c<M,VT>(a, i, j), dy_c<M,VT>(a, i, j));
            }
            e[i * cols + nc] = norm2_pow2(dx_b<M,VT>(a, i, nc), dy_c<M,VT>(a, i, nc));
        }
        #pragma omp single
        {
            e[0 * cols + 0] = norm2_pow2(
                dx_f<M,VT>(a, 0, 0),
                dy_f<M,VT>(a, 0, 0));
            e[0 * cols + nc] = norm2_pow2(
                dx_b<M,VT>(a, 0, nc),
                dy_f<M,VT>(a, 0, nc));
            e[nr * cols + 0] = norm2_pow2(
                dx_f<M,VT>(a, nr, 0),
                dy_b<M,VT>(a, nr, 0));
            e[nr * cols + nc] = norm2_pow2(
                dx_b<M,VT>(a, nr, nc),
                dy_b<M,VT>(a, nr, nc));
        }

        // compute volume using the mean of the values of the points i,j
        #pragma omp for reduction(+:energy)
        for (size_t i = 0; i < nr; i++) {
            for (size_t j = 0; j < nc; j++) {
                energy += ( e[i * cols + j] +
                            e[(i+1) * cols + j] +
                            e[i * cols + (j+1)] +
                            e[(i+1) * cols + (j+1)]
                          ) * 0.25;
            }
        }
    }

    return energy;
}

template<class M, class T, class VT>
inline void gradient_descent_isotropic(M & a, const float tolerance, const size_t max_iter) {
    T * e = new T[a.rows * a.cols];
    const T i_energy = energy<M,T,VT>(a, e);
    T c_diff = tolerance + 1;
    T c_energy;
    const float delta = 0.1;
    M r(a.rows, a.cols);
    size_t i = 0;
    double flow_time = 0;
    double energy_time = 0;
    double clone_time = 0;

    std::cout << "Isotropic diffusion:" << '\n';
    std::cout << "\tInitial energy: " << i_energy << '\n';

    auto start = std::chrono::system_clock::now();
    for (i = 0; i < max_iter; i++) {
        auto start = std::chrono::system_clock::now();
        isotropic_diffusion<M,VT>(a, r, delta);
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_time = end-start;
        flow_time += elapsed_time.count();

        start = std::chrono::system_clock::now();
        a.swap(r);
        end = std::chrono::system_clock::now();
        elapsed_time = end-start;
        clone_time += elapsed_time.count();

        start = std::chrono::system_clock::now();
        c_energy = energy<M,T,VT>(a, e);
        end = std::chrono::system_clock::now();
        elapsed_time = end-start;
        energy_time += elapsed_time.count();

        c_diff = c_energy / i_energy;
        std::cout << "\tIter " << i << ",\tenergy: " << c_energy << ",\tdifference: " << c_diff << '\n';
        if (c_diff < tolerance) {
            std::cout << "Convergence found on iteration " << i++ << " (difference = " << c_diff << ")" << '\n';
            break;
        }
    }
    if (i == max_iter && c_diff > tolerance) {
        std::cout << "Convergence not found in " << i << " iterations (difference = " << c_diff << ")" << '\n';
    }
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_time = end-start;
    double seconds = elapsed_time.count();
    std::cout << "Isotropic diffusion time: " << seconds << " seconds" << '\n';
    std::cout << "\tTime per iteration: " << seconds / i << " seconds" << '\n';
    std::cout << "\tTime isotropic_diffusion: " << flow_time << " (" << flow_time / i << ")" << " seconds" << '\n';
    std::cout << "\tTime clone: " << clone_time << " (" << clone_time / i << ")" << " seconds" << '\n';
    std::cout << "\tTime energy: " << energy_time<< " (" << energy_time / i << ")"  << " seconds" << '\n';

    #ifdef MEASURE
    std::cout << "Total;Iterations;Total_per_it;Flow;Energy" << '\n';
    std::cout << seconds << ";" << i << ";" << seconds / i << ";" << flow_time << ";" << energy_time << '\n';
    #endif

    delete[] e;
}

template<class M, class T, class VT>
inline void gradient_descent_anisotropic(M & a, const double tolerance, const size_t max_iter) {
    T * e = new T[a.rows * a.cols];
    const T i_energy = energy<M,T,VT>(a, e);
    T c_diff = tolerance + 1;
    T c_energy;
    const float delta = 0.01;
    M r(a.rows, a.cols);
    size_t i = 0;
    double flow_time = 0;
    double energy_time = 0;
    double clone_time = 0;

    std::cout << "Anisotropic diffusion:" << '\n';
    std::cout << "\tInitial energy: " << i_energy << '\n';

    auto start = std::chrono::system_clock::now();
    for (i = 0; i < max_iter; i++) {
        auto start = std::chrono::system_clock::now();
        anisotropic_diffusion<M,VT>(a, r, delta);
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_time = end-start;
        flow_time += elapsed_time.count();

        start = std::chrono::system_clock::now();
        a.swap(r);
        end = std::chrono::system_clock::now();
        elapsed_time = end-start;
        clone_time += elapsed_time.count();

        start = std::chrono::system_clock::now();
        c_energy = energy<M,T,VT>(a, e);
        end = std::chrono::system_clock::now();
        elapsed_time = end-start;
        energy_time += elapsed_time.count();

        c_diff = c_energy / i_energy;
        std::cout << "\tIter " << i << ",\tenergy: " << c_energy << ",\tdifference: " << c_diff << '\n';
        if (c_diff < tolerance) {
            std::cout << "Convergence found on iteration " << i++ << " (difference = " << c_diff << ")" << '\n';
            break;
        }
    }
    if (i == max_iter && c_diff > tolerance) {
        std::cout << "Convergence not found in " << i << " iterations (difference = " << c_diff << ")" << '\n';
    }
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_time = end-start;
    double seconds = elapsed_time.count();
    std::cout << "Anisotropic diffusion time: " << seconds << " seconds" << '\n';
    std::cout << "\tTime per iteration: " << seconds / i << '\n';
    std::cout << "\tTime anisotropic_diffusion: " << flow_time << " (" << flow_time / i << ")" << " seconds" << '\n';
    std::cout << "\tTime clone: " << clone_time << " (" << clone_time / i << ")" << " seconds" << '\n';
    std::cout << "\tTime energy: " << energy_time<< " (" << energy_time / i << ")"  << " seconds" << '\n';

    #ifdef MEASURE
    std::cout << "Total;Iterations;Total_per_it;Flow;Energy" << '\n';
    std::cout << seconds << ";" << i << ";" << seconds / i << ";" << flow_time << ";" << energy_time << '\n';
    #endif

    delete[] e;
}

template<class M, class T>
inline void anisotropic_flow(const M & a, M & r, const float delta) {
    const size_t nr = a.rows-1;
    const size_t nc = a.cols-1;

    #pragma omp parallel firstprivate(nr, nc, delta) shared(r, a) default(none)
    {
        // Upper boundary
        #pragma omp for nowait
        #pragma ivdep
        for (size_t j = 1; j < nc; j++) {
            const T Mx = dx_c<M,T>(a, 0, j);
            const T My = dy_f<M,T>(a, 0, j);
            const T Mxx = a.at(0,j+1) - 2*a.at(0,j) + a.at(0,j-1);
            const T Myy = a.at(1,j) - 2*a.at(0,j);
            const T Mxy = (a.at(1,j+1) - a.at(1,j-1)) * 0.25;
            const T norm_M = sqrt(Mx*Mx + My*My);
            r.at(0,j) = a.at(0,j) + delta * (cbrt(Mxx*My*My - 2*Mx*My*Mxy + Myy*Mx*Mx) / (1 + norm_M));
        }
        // Lower boundary
        #pragma omp for nowait
        #pragma ivdep
        for (size_t j = 1; j < nc; j++) {
            const T Mx = dx_c<M,T>(a, nr, j);
            const T My = dy_b<M,T>(a, nr, j);
            const T Mxx = a.at(nr,j+1) - 2*a.at(nr,j) + a.at(nr,j-1);
            const T Myy = - 2*a.at(nr,j) + a.at(nr-1,j);
            const T Mxy = (- a.at(nr-1,j+1) + a.at(nr-1,j-1)) * 0.25;
            const T norm_M = norm2(Mx, My);
            r.at(nr,j) = a.at(nr,j) + delta * (cbrt(Mxx*My*My - 2*Mx*My*Mxy + Myy*Mx*Mx) / (1 + norm_M));
        }
        // Left boundary
        #pragma omp for nowait
        #pragma ivdep
        for (size_t i = 1; i < nr; i++) {
            const T Mx = dx_f<M,T>(a, i, 0);
            const T My = dy_c<M,T>(a, i, 0);
            const T Mxx = a.at(i,1) - 2*a.at(i,0);
            const T Myy = a.at(i+1,0) - 2*a.at(i,0) + a.at(i-1,0);
            const T Mxy = (a.at(i+1,1) - a.at(i-1,1)) * 0.25;
            const T norm_M = norm2(Mx, My);
            r.at(i,0) = a.at(i,0) + delta * (cbrt(Mxx*My*My - 2*Mx*My*Mxy + Myy*Mx*Mx) / (1 + norm_M));
        }
        // Right boundary
        #pragma omp for nowait
        #pragma ivdep
        for (size_t i = 1; i < nr; i++) {
            const T Mx = dx_b<M,T>(a, i, nc);
            const T My = dy_c<M,T>(a, i, nc);
            const T Mxx = - 2*a.at(i,nc) + a.at(i,nc-1);
            const T Myy = a.at(i+1,nc) - 2*a.at(i,nc) + a.at(i-1,nc);
            const T Mxy = (-a.at(i+1,nc-1) + a.at(i-1,nc-1)) * 0.25;
            const T norm_M = norm2(Mx, My);
            r.at(i,nc) = a.at(i,nc) + delta * (cbrt(Mxx*My*My - 2*Mx*My*Mxy + Myy*Mx*Mx) / (1 + norm_M));
        }
        // Central cases
        #pragma omp for nowait
        for (size_t i = 1; i < nr; i++) {
            #pragma ivdep
            for (size_t j = 1; j < nc; j++) {
                const T Mx = dx_c<M,T>(a, i, j);
                const T My = dy_c<M,T>(a, i, j);
                const T Mxx = a.at(i,j+1) - 2*a.at(i,j) + a.at(i,j-1);
                const T Myy = a.at(i+1,j) - 2*a.at(i,j) + a.at(i-1,j);
                const T Mxy = (a.at(i+1,j+1) - a.at(i+1,j-1) -
                              a.at(i-1,j+1) + a.at(i-1,j-1)) * 0.25;
                const T norm_M = norm2(Mx, My);
                r.at(i,j) = a.at(i,j) + delta * (cbrt(Mxx*My*My - 2*Mx*My*Mxy + Myy*Mx*Mx) / (1 + norm_M));
            }
        }
        #pragma omp single nowait
        {
            // Position 0,0
            const T Mx = dx_f<M,T>(a,0,0);
            const T My = dy_f<M,T>(a,0,0);
            const T Mxx = a.at(0,1) - 2*a.at(0,0);
            const T Myy = a.at(1,0) - 2*a.at(0,0);
            const T Mxy = (a.at(1,1)) * 0.25;
            const T norm_M = norm2(Mx, My);
            r.at(0,0) = a.at(0,0) + delta * (cbrt(Mxx*My*My - 2*Mx*My*Mxy + Myy*Mx*Mx) / (1 + norm_M));
        }
        #pragma omp single nowait
        {
            // Position 0,nc
            const T Mx = dx_b<M,T>(a,0,nc);
            const T My = dy_f<M,T>(a,0,nc);
            const T Mxx = - 2*a.at(0,nc) + a.at(0,nc-1);
            const T Myy = a.at(1,nc) - 2*a.at(0,nc);
            const T Mxy = (- a.at(1,nc-1)) * 0.25;
            const T norm_M = norm2(Mx, My);
            r.at(0,nc) = a.at(0,nc) + delta * (cbrt(Mxx*My*My - 2*Mx*My*Mxy + Myy*Mx*Mx) / (1 + norm_M));
        }
        #pragma omp single nowait
        {
            // Position nr,0
            const T Mx = dx_f<M,T>(a,nr,0);
            const T My = dy_b<M,T>(a,nr,0);
            const T Mxx = a.at(nr,1) - 2*a.at(nr,0);
            const T Myy = - 2*a.at(nr,0) + a.at(nr-1,0);
            const T Mxy = (- a.at(nr-1,1)) * 0.25;
            const T norm_M = norm2(Mx, My);
            r.at(nr,0) = a.at(nr,0) + delta * (cbrt(Mxx*My*My - 2*Mx*My*Mxy + Myy*Mx*Mx) / (1 + norm_M));
        }
        #pragma omp single nowait
        {
            // Position nr,nc
            const T Mx = dx_b<M,T>(a,nr,nc);
            const T My = dy_b<M,T>(a,nr,nc);
            const T Mxx = - 2*a.at(nr,nc) + a.at(nr,nc-1);
            const T Myy = - 2*a.at(nr,nc) + a.at(nr-1,nc);
            const T Mxy = (a.at(nr-1,nc-1)) * 0.25;
            const T norm_M = norm2(Mx, My);
            r.at(nr,nc) = a.at(nr,nc) + delta * (cbrt(Mxx*My*My - 2*Mx*My*Mxy + Myy*Mx*Mx) / (1 + norm_M));
        }
    }
}

template<class M, class T>
inline void brightness_anisotropic_flow(M & a, const double tolerance, const size_t max_iter) {
    T * e = new T[a.rows * a.cols];
    const T i_energy = energy<M,T,T>(a, e);
    T c_diff = tolerance + 1;
    T c_energy;
    const float delta = 0.1;
    M r(a.rows, a.cols);
    size_t i = 0;
    double flow_time = 0;
    double energy_time = 0;
    double clone_time = 0;

    std::cout << "Anisotropic diffusion:" << '\n';
    std::cout << "\tInitial energy: " << i_energy << '\n';

    auto start = std::chrono::system_clock::now();
    for (i = 0; i < max_iter; i++) {
        auto start = std::chrono::system_clock::now();
        anisotropic_flow<M,T>(a, r, delta);
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_time = end-start;
        flow_time += elapsed_time.count();

        start = std::chrono::system_clock::now();
        a.swap(r);
        end = std::chrono::system_clock::now();
        elapsed_time = end-start;
        clone_time += elapsed_time.count();

        start = std::chrono::system_clock::now();
        c_energy = energy<M,T,T>(a, e);
        end = std::chrono::system_clock::now();
        elapsed_time = end-start;
        energy_time += elapsed_time.count();

        c_diff = c_energy / i_energy;
        std::cout << "\tIter " << i << ",\tenergy: " << c_energy << ",\tdifference: " << c_diff << '\n';
        if (c_diff < tolerance) {
            std::cout << "Convergence found on iteration " << i++ << " (difference = " << c_diff << ")" << '\n';
            break;
        }
    }
    if (i == max_iter && c_diff > tolerance) {
        std::cout << "Convergence not found in " << i << " iterations (difference = " << c_diff << ")" << '\n';
    }
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_time = end-start;
    double seconds = elapsed_time.count();
    std::cout << "Anisotropic diffusion time: " << seconds << " seconds" << '\n';
    std::cout << "\tTime per iteration: " << seconds / i << '\n';
    std::cout << "\tTime anisotropic_flow: " << flow_time << " (" << flow_time / i << ")" << " seconds" << '\n';
    std::cout << "\tTime clone: " << clone_time << " (" << clone_time / i << ")" << " seconds" << '\n';
    std::cout << "\tTime energy: " << energy_time<< " (" << energy_time / i << ")"  << " seconds" << '\n';

    #ifdef MEASURE
    std::cout << "Total;Iterations;Total_per_it;Flow;Energy" << '\n';
    std::cout << seconds << ";" << i << ";" << seconds / i << ";" << flow_time << ";" << energy_time << '\n';
    #endif

    delete[] e;
}

template<class M, class T>
inline void laplacian_flow(const M & a, M & r, const float delta) {
    const size_t nr = a.rows - 1;
    const size_t nc = a.cols - 1;

    #pragma omp parallel firstprivate(delta) shared(a, r) default(none)
    {
        #pragma omp for nowait
        for (size_t i = 1; i < nr; i++) {
            const float lapl = (a.at(i-1,0) - 4 * a.at(i,0) + a.at(i+1,0) + a.at(i,1));
            r.at(i,0) = a.at(i,0) + delta * lapl;
        }
        #pragma omp for nowait
        for (size_t i = 1; i < nr; i++) {
            const float lapl = (a.at(i-1,nc) + a.at(i-1,nc-1) - 4 * a.at(i,nc) + a.at(i+1,nc));
            r.at(i,nc) = a.at(i,nc) + delta * lapl;
        }
        #pragma omp for nowait
        for (size_t j = 1; j < nc; j++) {
            const float lapl = (a.at(0,j-1) - 4 * a.at(0,j) + a.at(1,j) + a.at(0,j+1));
            r.at(0,j) = a.at(0,j) + delta * lapl;
        }
        #pragma omp for nowait
        for (size_t j = 1; j < nc; j++) {
            const float lapl = (a.at(nr-1,j) + a.at(nr, j-1) - 4 * a.at(nr,j) + a.at(nr,j+1));
            r.at(nr,j) = a.at(nr,j) + delta * lapl;
        }
        #pragma omp for nowait
        for (size_t i = 1; i < a.rows-1; i++) {
            for (size_t j = 1; j < a.cols-1; j++) {
                r.at(i,j) = a.at(i,j) + delta * laplacian<M,T>(a, i, j);
            }
        }
        #pragma omp single
        {
            r.at(0,0)  = a.at(0,0)  + delta * (-4 * a.at(0,0) + a.at(1,0) + a.at(0,1));
            r.at(0,nc) = a.at(0,nc) + delta * (a.at(0,nc-1) - 4 * a.at(0,nc) + a.at(1,nc));
            r.at(nr,0) = a.at(nr,0) + delta * (a.at(nr-1,0) - 4 * a.at(nr,0) + a.at(nr,1));
            r.at(nr,nc) = a.at(nr,nc) + delta * (a.at(nr-1,nc) + a.at(nr,nc-1) - 4 * a.at(nr,nc));
        }
    }
}

template<class M, class T>
inline void brightness_laplacian_flow(M & a, const double tolerance, const size_t max_iter) {
    T * e = new T[a.rows * a.cols];
    const T i_energy = energy<M,T,T>(a, e);
    T c_diff = tolerance + 1;
    T c_energy;
    const float delta = 0.1;
    M r(a.rows, a.cols);
    size_t i = 0;
    double flow_time = 0;
    double energy_time = 0;
    double clone_time = 0;

    std::cout << "Laplacian flow:" << '\n';
    std::cout << "\tInitial energy: " << i_energy << '\n';

    auto start = std::chrono::system_clock::now();
    for (i = 0; i < max_iter; i++) {
        auto start = std::chrono::system_clock::now();
        laplacian_flow<M,T>(a, r, delta);
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_time = end-start;
        flow_time += elapsed_time.count();

        start = std::chrono::system_clock::now();
        a.swap(r);
        end = std::chrono::system_clock::now();
        elapsed_time = end-start;
        clone_time += elapsed_time.count();

        start = std::chrono::system_clock::now();
        c_energy = energy<M,T,T>(a, e);
        end = std::chrono::system_clock::now();
        elapsed_time = end-start;
        energy_time += elapsed_time.count();

        c_diff = c_energy / i_energy;
        std::cout << "\tIter " << i << ",\tenergy: " << c_energy << ",\tdifference: " << c_diff << '\n';
        if (c_diff < tolerance) {
            std::cout << "Convergence found on iteration " << i++ << " (difference = " << c_diff << ")" << '\n';
            break;
        }
    }
    if (i == max_iter && c_diff > tolerance) {
        std::cout << "Convergence not found in " << i << " iterations (difference = " << c_diff << ")" << '\n';
    }
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_time = end-start;
    double seconds = elapsed_time.count();
    std::cout << "Laplacian flow time: " << seconds << " seconds" << '\n';
    std::cout << "\tTime per iteration: " << seconds / i << " seconds" << '\n';
    std::cout << "\tTime anisotropic_flow: " << flow_time << " (" << flow_time / i << ")" << " seconds" << '\n';
    std::cout << "\tTime clone: " << clone_time << " (" << clone_time / i << ")" << " seconds" << '\n';
    std::cout << "\tTime energy: " << energy_time<< " (" << energy_time / i << ")"  << " seconds" << '\n';

    #ifdef MEASURE
    std::cout << "Total;Iterations;Total_per_it;Flow;Energy" << '\n';
    std::cout << seconds << ";" << i << ";" << seconds / i << ";" << flow_time << ";" << energy_time << '\n';
    #endif

    delete[] e;
}

#endif
