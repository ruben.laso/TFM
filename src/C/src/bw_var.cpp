#include <iostream>
#include <cstdlib>
#include <unistd.h>

#include "Image.hpp"

float LAMBDA = 1.0;
Image::Method METHOD = Image::conj_grad;
bool DISPLAY = true;

char * ORIGINAL_FILE = nullptr;
char * NOISY_FILE = nullptr;

void parse_args(const int argc, char * const argv[]);

int main(int argc, char *argv[]) {
    if (argc < 2) {
        std::cerr << "Usage: $ " << argv[0] << "-n <noisy_image> [optional arguments]" << std::endl;
        return EXIT_FAILURE;
    }
    parse_args(argc, argv);

    #ifdef __CUDA__
    CUDA_warmup();
    #endif

    // Open noisy image
    Image noisy = Image(NOISY_FILE);
    if (!noisy.image.data) {
        return EXIT_FAILURE;
    }

    // Clean noise
    double seconds = 0.0;
    Image denoised = noisy;
    denoised.denoise<float>(LAMBDA, METHOD, seconds);
    std::cout << "Total time spent: " << seconds << " seconds." << '\n';

    // Write denoised image
    denoised.write("denoised.jpeg");

    Image original;

    if (ORIGINAL_FILE != nullptr) {
        // Open original image
        original = Image(ORIGINAL_FILE);
        if (!original.image.data) {
            return EXIT_FAILURE;
        }

        double orig_norm = cv::norm(original.image);

        Image diff_noisy_orig(cv::abs(original.image - noisy.image));
        std::cout << "Norm with noisy image and original: " << diff_noisy_orig.norm() / orig_norm << '\n';
        std::cout << "Noisy PSNR: " << noisy.PSNR(original) << " Db" << '\n';

        Image diff_orig(cv::abs(original.image - denoised.image));
        std::cout << "Norm with original image and denoised: " << diff_orig.norm() / orig_norm << '\n';
        std::cout << "Denoised PSNR: " << denoised.PSNR(original) << " Db" << '\n';
    }

    if (DISPLAY) {
        if (original.image.data != NULL) {
            original.display("Original image");
        }
        noisy.display("Noisy image");
        denoised.display("Denoised image");

        std::cout << "Press ESC to exit the program..." << '\n';
        int key;
        while((key = cv::waitKey(0) & 0xFF) != 27) {
            // do nothing, until ESC key is pressed
        }
    }
    std::cout << "Exiting..." << '\n';

    return EXIT_SUCCESS;
}

void print_help() {
    std::cout << "Options: " << '\n';
    std::cout << "\t-d : disable display of images." << '\n';
    std::cout << "\t-h : print this help message." << '\n';
    std::cout << "\t-l <double> : value of Lambda parameter." << '\n';
    std::cout << "\t-m <int> : select denoising method." << '\n';
    std::cout << "\t-n <string> : path to noisy image." << '\n';
    std::cout << "\t-o <string> : path to original  image." << '\n';
}

void parse_args(const int argc, char * const argv[]) {
    int opt;
    const char * options = "dhl:m:n:o:";
    while ((opt = getopt(argc,argv,options)) != EOF) {
        switch (opt) {
            case 'd':
                DISPLAY = !DISPLAY;
                std::cout << "Display images: " << DISPLAY << '\n';
                break;
            case 'h':
                print_help();
                break;
            case 'l':
                LAMBDA = atof(optarg);
                std::cout << "Lambda = " << LAMBDA << '\n';
                break;
            case 'm':
                METHOD = Image::switch_method(atoi(optarg));
                break;
            case 'n':
                NOISY_FILE = optarg;
                std::cout << "Noisy image: " << NOISY_FILE << '\n';
                break;
            case 'o':
                ORIGINAL_FILE = optarg;
                std::cout << "Original image: " << ORIGINAL_FILE << '\n';
                break;
            default:
                std::cout << "Unkown option. Printing help..." << '\n';
                print_help();
                break;
        }
    }
}
