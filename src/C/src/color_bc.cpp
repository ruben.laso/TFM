#include <iostream>
#include <cstdlib>
#include <unistd.h>

#include "ColorImage_BC.hpp"

float LAMBDA = 1.0;
float THRESHOLD_BRIGHT = 0.3;
float THRESHOLD_CHROMA = 0.1;
size_t B_MAX_ITERATIONS = 200;
size_t C_MAX_ITERATIONS = 200;
ColorImage_BC::Chromaticity CHROMA_METHOD = ColorImage_BC::anisotropic_flow;
ColorImage_BC::Brightness BRIGHT_METHOD = ColorImage_BC::anisotropic;
bool DISPLAY = true;

char * ORIGINAL_FILE = nullptr;
char * NOISY_FILE = nullptr;

void parse_args(const int argc, char * const argv[]);

int main(int argc, char *argv[]) {
    if (argc < 2) {
        std::cerr << "Usage: $ " << argv[0] << " <path_to_image> [optional arguments]" << std::endl;
        return EXIT_FAILURE;
    }
    parse_args(argc, argv);

    #ifdef __CUDA__
    CUDA_warmup();
    #endif

    // Introduce gaussian noise
    ColorImage_BC noisy(NOISY_FILE);
    if (!noisy.image.data) {
        return EXIT_FAILURE;
    }

    // Clean noise
    double seconds = 0.0;
    ColorImage_BC denoised(noisy);
    denoised.denoise(BRIGHT_METHOD, CHROMA_METHOD, LAMBDA, THRESHOLD_BRIGHT, THRESHOLD_CHROMA, B_MAX_ITERATIONS, C_MAX_ITERATIONS, seconds);
    std::cout << "Total time spent: " << seconds << " seconds" << '\n';

    // Write denoised image
    denoised.write("denoised.jpeg");

    ColorImage_BC original;
    if (ORIGINAL_FILE != nullptr) {
        // Open original image
        original = ColorImage_BC(ORIGINAL_FILE);
        if (!original.image.data) {
            return EXIT_FAILURE;
        }

        double orig_norm = cv::norm(original.image);

        Image diff_noisy_orig(cv::abs(original.image - noisy.image));
        std::cout << "Norm with noisy image and original: " << diff_noisy_orig.norm() / orig_norm << '\n';
        std::cout << "Noisy PSNR: " << noisy.PSNR(original) << " Db" << '\n';

        Image diff_orig(cv::abs(original.image - denoised.image));
        std::cout << "Norm with original image and denoised: " << diff_orig.norm() / orig_norm << '\n';
        std::cout << "Denoised PSNR: " << denoised.PSNR(original) << " Db" << '\n';
    }

    if (DISPLAY) {
        if (original.image.data) {
            original.display("Original image");
        }
        noisy.display("Noisy image");
        denoised.display("Denoised image");

        std::cout << "Press ESC to exit the program..." << '\n';
        int key;
        while((key = cv::waitKey(0) & 0xFF) != 27) {
            // do nothing, until ESC key is pressed
        }
    }

    #ifdef MEASURE
    std::cout << "Total;" << '\n';
    std::cout << seconds << '\n';
    #endif

    std::cout << "Exiting..." << '\n';

    return EXIT_SUCCESS;
}

void print_help() {
    std::cout << "Options: " << '\n';
    std::cout << "\t-b <int> : brightess denoising method." << '\n';
    std::cout << "\t-c <int> : chromaticity denoising method." << '\n';
    std::cout << "\t-d : disable display of images." << '\n';
    std::cout << "\t-h : print this help message." << '\n';
    std::cout << "\t-i <int> : max. iterations for brightness method." << '\n';
    std::cout << "\t-j <int> : max. iterations for chromaticity method." << '\n';
    std::cout << "\t-l <double> : value of Lambda parameter." << '\n';
    std::cout << "\t-m <int> : select denoising method." << '\n';
    std::cout << "\t-n <string> : path to noisy image." << '\n';
    std::cout << "\t-o <string> : path to original  image." << '\n';
    std::cout << "\t-s <double> : energy threshold for brightess." << '\n';
    std::cout << "\t-t <double> : energy threshold for chromaticity." << '\n';
}

void parse_args(const int argc, char * const argv[]) {
    int opt;
    const char * options = "b:c:dhi:j:l:n:o:s:t:";
    while ((opt = getopt(argc,argv,options)) != EOF) {
        switch (opt) {
            case 'b':
                BRIGHT_METHOD = ColorImage_BC::switch_brightness(atoi(optarg));
                break;
            case 'c':
                CHROMA_METHOD = ColorImage_BC::switch_chromaticity(atoi(optarg));
                break;
            case 'd':
                DISPLAY = !DISPLAY;
                std::cout << "Display images: " << DISPLAY << '\n';
                break;
            case 'h':
                print_help();
                break;
            case 'i':
                B_MAX_ITERATIONS = atoi(optarg);
                std::cout << "Brightess method max. iterations = " << B_MAX_ITERATIONS << '\n';
                break;
            case 'j':
                C_MAX_ITERATIONS = atoi(optarg);
                std::cout << "Chromaticity method max. iterations = " << C_MAX_ITERATIONS << '\n';
                break;
            case 'l':
                LAMBDA = atof(optarg);
                std::cout << "Lambda = " << LAMBDA << '\n';
                break;
            case 'n':
                NOISY_FILE = optarg;
                std::cout << "Noisy image: " << NOISY_FILE << '\n';
                break;
            case 'o':
                ORIGINAL_FILE = optarg;
                std::cout << "Original image: " << ORIGINAL_FILE << '\n';
                break;
            case 's':
                THRESHOLD_BRIGHT = atof(optarg);
                std::cout << "Steady state threshold for brightess = " << THRESHOLD_BRIGHT << '\n';
                break;
            case 't':
                THRESHOLD_CHROMA = atof(optarg);
                std::cout << "Steady state threshold for chromaticity = " << THRESHOLD_CHROMA << '\n';
                break;
            default:
                std::cout << "Unkown option. Printing help..." << '\n';
                print_help();
                break;
        }
    }
}
