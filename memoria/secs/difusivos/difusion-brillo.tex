\section{Tratamiento del brillo} \label{sec:difusion-brillo}
%En primer lugar, trataremos el problema~\ref{pro:problema-difusion} con la función definida en~\eqref{eq:magnitud}.
En primer lugar, se trata la magnitud de la imagen definida en~\eqref{eq:magnitud}.
Al introducir ruido en una imagen, al menos uno de los canales de color es afectado y, en consecuencia, los vectores que componen la imagen ven alterada su magnitud.
Por eso, el tratamiento del brillo es también importante para conseguir la mejor calidad posible en la reconstrucción.

En~\cite{tang2001color} se proponen varias alternativas para la reducción del ruido en la magnitud. Además de los tipos de flujos de difusión que se exponen en este capítulo, también se propone utilizar el filtro de la mediana $3 \times 3$, que se describe en la sección~\ref{sec:implementacion-difusivos}.% aunque no se le prestará tanta atención en el capítulo~\ref{cha:resultados} como al resto de métodos.

\subsection{Flujo del laplaciano}
De forma general, el flujo del laplaciano viene inspirado por la ecuación del calor, que se define como
\begin{equation*}
	\frac{\partial u}{\partial t} = \alpha \Delta u,
\end{equation*}
donde $\alpha$ representa la difusividad térmica~\cite{koenderink1984structure}. Particularizando para $\alpha = 1$ y aplicándolo a la función de magnitud $M$, se tiene
\begin{equation} \label{eq:brillo-isotropico}
	\frac{\partial M(x, y, t)}{\partial t}
	=
	\frac{\partial^2 M(x,y,t)}{\partial x^2} + \frac{\partial^2 M(x,y,t)}{\partial y^2}.
\end{equation}

Nótese que este flujo es isotrópico, es decir, la difusión es igual en todas las direcciones. Por ello, este tipo de flujos no consigue conservar los bordes de los objetos de la imagen, empobreciendo la calidad de la reconstrucción.


\subsection{Flujo anisotrópico}
En~\cite{perona1990scale} se propone una variación del método, haciendo que la difusión sea anisotrópica, de la forma
\begin{equation*}
	\frac{\partial u}{\partial t} = \dive\left(c(x,y,t) \nabla u\right) = c \Delta u + \nabla c \cdot \nabla u,
\end{equation*}
donde $c: \Omega \times \mathbb{R}^+ \rightarrow \mathbb{R}$ es un término que previene que la difusión sea igual en todas las direcciones. En~\cite{perona1990scale} se propone
\begin{equation*}
	c(x,y,t) = g(\norm{\nabla u(x,y,t)}),
\end{equation*}
donde la función $g$ puede ser una de las siguientes
\begin{align*}
	g_1(\norm{\nabla u}) &= e^{-\left(\frac{\norm{\nabla u}}{K}\right)^2},
	\\
	g_2(\norm{\nabla u}) &= \frac{1}{1 + \left(\frac{\norm{\nabla u}}{K}\right)^2},
\end{align*}
con $K$ constante arbitraria.
Se puede aplicar una ligera variación sobre $g_2$, tomando $K=1$ y eliminando la potencia en el denominador, de forma que quede
\begin{equation} \label{eq:anisotropic-term}
	g(\norm{\nabla u}) = \frac{1}{1 + \norm{\nabla u}}.
\end{equation}

Como evolución de esta forma, en~\cite{sapiro1994experiments} se propone utilizar el siguiente flujo de difusión anisotrópica afín e invariante:
\begin{equation*}
	\frac{\partial u}{\partial t} = c(x,y,t) \norm{\nabla u} \kappa^{\frac{1}{3}},
\end{equation*}
donde la función $c$ corresponde a~\eqref{eq:anisotropic-term} y $\kappa$ (ver~\cite{Alvarez1993}) es
\begin{equation*}
	\kappa = \frac{u_y^2 u_{xx} - 2 u_x u_y u_{xy} + u_x^2 u_{yy}}{\left(u_x^2 + u_y^2\right)^{\frac{3}{2}}}.
\end{equation*}
Finalmente, tomando $u = M$, se tiene que el flujo de difusión anisotrópica para la magnitud de la imagen viene definido de la forma:
\begin{equation} \label{eq:brillo-anisotropico}
	\frac{\partial M}{\partial t} = \frac{\left(M_y^2 M_{xx} - 2 M_x M_y M_{xy}+ M_x^2 M_{yy}\right)^{\frac{1}{3}}}{1 + \norm{\nabla M}}.
\end{equation}
