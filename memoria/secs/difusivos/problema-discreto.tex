\subsection{Cálculo de las derivadas espaciales} \label{sec:problema-discreto}
En esta sección se detallan las aproximaciones realizadas para el cálculo de las derivadas espaciales, para las que se han utilizado esquemas de diferencias finitas.

\subsubsection{Difusión del brillo}
En primera instancia, se describen las aproximaciones discretas de los flujos para la difusión del brillo.

\paragraph{Flujo isotrópico}
Para la discretización de~\eqref{eq:brillo-isotropico}, se puede utilizar un esquema de cinco puntos para la aproximación del laplaciano~\cite{abramowitz+stegun} (ver figura~\ref{fig:lapl-malla}), tal que:
\begin{equation} \label{eq:laplaciano-aprox}
	\Delta u_{i,j,k}
	=
	\frac{u_{i+1,j,k} + u_{i,j+1,k} - 4u_{i,j,k} + u_{i-1,j,k} + u_{i,j-1,k}}{h^2} + \bigO\left(h^2\right),
\end{equation}
como $h=1$, se tiene que
\begin{equation*}
	\Delta u_{i,j,k}
	\simeq
	u_{i+1,j,k} + u_{i,j+1,k} - 4u_{i,j,k} + u_{i-1,j,k} + u_{i,j-1,k}.
\end{equation*}

\input{figs/mesh/lapl-mesh}

Por tanto, para el instante temporal $\delta_{k+1}$ se calculará
\begin{equation*}
	M_{i,j,k+1} = M_{i,j,k} + \delta \left(M_{i+1,j,k} + M_{i,j+1,k} - 4M_{i,j,k} + M_{i-1,j,k} + M_{i,j-1,k}\right).
\end{equation*}

Nótese que en los puntos $\left( x_i,y_j \right) \notin \Omega_h$ se asumirá que $u(x_i, y_j, t_k) = 0$.

\paragraph{Flujo anisotrópico}
En~\eqref{eq:brillo-anisotropico} es necesario aproximar las derivadas parciales primeras y segundas. Las derivadas primeras se aproximan mediante diferencias centradas, tal que
\begin{align}
	\frac{\partial u_{i,j,k}}{\partial x} &= \frac{u_{i+1,j,k} - u_{i-1,j,k}}{2h} + \bigO\left(h^2\right), \label{eq:dx-centered}
	\\
	\frac{\partial u_{i,j,k}}{\partial y} &= \frac{u_{i,j+1,k} - u_{i,j-1,k}}{2h} + \bigO\left(h^2\right). \label{eq:dy-centered}
\end{align}
%siempre que $\left( x_i, y_j \right) \in \Omega_h$.
En el caso en que haya que calcular las derivadas en los puntos $\left( x_i, y_j \right)$ con $i = \left\lbrace 1, N \right\rbrace$ y/o $j = \left\lbrace 1, M \right\rbrace$ se deben realizar las aproximaciones de diferente manera. Cuando $i=1$ o $j=1$, se utilizan diferencias hacia delante,
\begin{align}
	\frac{\partial u_{1,j,k}}{\partial x} &= \frac{u_{2,j,k} - u_{1,j,k}}{h} + \bigO(h),\label{eq:dx-forward} %\quad j = 1, \dots, M, \quad k = 1, \dots , \infty,
	\\
	\frac{\partial u_{i,1,k}}{\partial y} &= \frac{u_{i,2,j} - u_{i,1,k}}{h} + \bigO(h). \label{eq:dy-forward} %\quad i = 1, \dots, N, \quad k = 1, \dots , \infty,
\end{align}
mientras que para $i=N$ o $j=M$ se utilizan diferencias hacia atrás,
\begin{align}
	\frac{\partial u_{N,j,k}}{\partial x} &= \frac{u_{N,j,k} - u_{N-1,j,k}}{h} + \bigO(h), \label{eq:dx-backward} %\quad j = 1, \dots, M, \quad k = 1, \dots , \infty,
	\\
	\frac{\partial u_{i,M,k}}{\partial y} &= \frac{u_{i,M,j} - u_{i,M-1,k}}{h} + \bigO(h). \label{eq:dy-backward}%\quad i = 1, \dots, N, \quad k = 1, \dots , \infty.
\end{align}

Las derivadas segundas se aproximan mediante
\begin{gather*}
	\frac{\partial^2 u_{i,j,k}}{\partial x^2} = \frac{u_{i+1,j,k} - 2u_{i,j,k} + u_{i-1,j,k}}{h^2} + \bigO\left(h^2\right), %\quad i=1,\dots,N, \quad j=1,\dots,M, \quad k=1,\dots,\infty,
	\\
	\frac{\partial^2 u_{i,j,k}}{\partial y^2} = \frac{u_{i,j+1,k} - 2u_{i,j,k} + u_{i,j-1,k}}{h^2} + \bigO\left(h^2\right), %\quad i=1,\dots,N, \quad j=1,\dots,M, \quad k=1,\dots,\infty.
	\\
%\end{gather*}
%y las derivadas parciales de segundo orden se calculan con
%\begin{gather*}
	\frac{\partial^2 u_{i,j,k}}{\partial x \partial y}
	=
	\frac{u_{i+1,j+1,k} - u_{i+1,j-1,k} - u_{i-1,j+1,k} + u_{i-1,j-1,k}}{4h^2} + \bigO\left(h^2\right).
\end{gather*}
Tal y como se comentó para la aproximación del laplaciano, se asume que $u_{i,j,k} = 0$ si $\left( x_i, y_j \right) \notin \Omega_h$.

La idea es utilizar estas aproximaciones, con $h=1$ y despreciando los términos $\bigO(h)$ y $\bigO\left(h^2\right)$, y sustituirlas adecuadamente en~\eqref{eq:brillo-anisotropico} para, en el paso $k+1$, obtener
\begin{equation*}
	M_{i,j,k+1} = M_{i,j,k} + \delta\frac{\partial M_{i,j,k}}{\partial t}.
\end{equation*}

\subsubsection{Difusión de la cromaticidad}
Para el tratamiento de la cromaticidad se utilizan las mismas aproximaciones que en los flujos difusivos del brillo.

\paragraph{Flujo isotrópico}
Para la aproximación del laplaciano en~\eqref{eq:cromaticidad-isotropico} se utiliza el esquema mostrado en~\eqref{eq:laplaciano-aprox} para cada uno de los canales. Por otro lado, para aproximar el gradiente, se emplea la expresión~\eqref{eq:norm-channels} en la que se aproximan las derivadas por las expresiones~\eqref{eq:dx-centered} a~\eqref{eq:dy-backward} siguiendo los criterios descritos en esa misma sección.

\paragraph{Flujo anisotrópico}
Para la discretización de~\eqref{eq:cromaticidad-anisotropico}, se debe recordar la definición de divergencia:
\begin{equation*}
	\dive(u_{i,j,k})
	=
	\frac{\partial u_{i,j,k}}{\partial x} + \frac{\partial u_{i,j,k}}{\partial y},
\end{equation*}
donde las derivadas de primer orden se aproximarán de la misma forma que para los tipos de flujo comentados anteriormente y lo mismo se debe hacer para aproximar los gradientes que aparecen en~\eqref{eq:cromaticidad-anisotropico}.
%Todas estas aproximaciones están condicionadas a si el operador actúa sobre cada uno de los canales por separado o sobre todos a la vez.
%Estas aproximaciones
