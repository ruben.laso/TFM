\section{Resolución numérica} \label{sec:resolucion}
En este trabajo solo nos interesa encontrar la solución numérica de las funciones $M(x,y,t)$ y $\mathbf{D}(x,y,t)$ en un determinado tiempo objetivo. Para encontrar estas soluciones se utilizan múltiples esquemas de diferencias finitas para aproximar las derivadas parciales espaciales combinadas con métodos de tipo Euler o Runge-Kutta para las derivadas temporales.
%Sin embargo, en esta memoria solo describiremos brevemente los dos métodos discutidos en la realización de este trabajo, Euler explícito e implícito.

\subsection{Discretización del dominio} \label{sec:malla-difusivo}
En primer lugar, se debe introducir la discretización del dominio sobre el que se trabaja, que en este caso se define $\Omega_h$ y $\partial\Omega_h$ de la misma forma que en la sección~\ref{sec:malla-variacional}.

Además, en los métodos de difusión se debe realizar una discretización del tiempo, que avanzará a un determinado paso $\delta$, tal que $t_k =  k\delta$ para $k = 0, \dots, \infty$.

De forma general, en esta sección se utiliza la notación
\begin{equation*}
\begin{array}{cl}
(x_i, y_j, t_k) = (ih_x, jh_y, \delta), &\quad i=1, \dots, N, \ j = 1, \dots, M, \ k = 1, \dots, \infty,
\\
u(x_i, y_j, t_k) \simeq u_{i,j,k} , &\quad i=1, \dots, N, \ j = 1, \dots, M, \ k = 1, \dots, \infty.
\end{array}
\end{equation*}


\subsection{Cálculo de las derivadas temporales}
Dado que en este trabajo se buscan tiempos de ejecución bajos, se prioriza el uso de esquemas explícitos siempre que sea posible.
En concreto, se ha utilizado el esquema de Euler explícito. A pesar de que este método es condicionalmente numéricamente estable, \textit{a priori}, parece más eficiente computacionalmente reducir ligeramente el paso de tiempo $\delta$ (a costa de computar más pasos de tiempo) que resolver un sistema de ecuaciones en cada uno de los pasos como sería el caso de Euler implícito.

%\subsubsection{Euler explícito}
El esquema de Euler explícito calcula los valores de una función $\mathbf{u}$ en los instantes de tiempo $t_1, t_2, \dots, t_N$, con un paso $\delta = t_{k+1} - t_k,\ k = 0, \dots, \infty$, de la forma
\begin{equation} \label{eq:euler-explicito}
	\begin{dcases}
	\mathbf{u}(x,y,t_0) = \mathbf{u}(x,y,0),
	\\
	\mathbf{u}(x,t,t_{k+1}) = \mathbf{u}(x,y,t_{k}) + \delta \mathbf{f}(\mathbf{u}(x,y,t_k), t_k), \quad k = 0, \dots, \infty.
	\end{dcases}
\end{equation}

En el algoritmo~\ref{alg:grad-descent-difusivo} se muestra el pseudocódigo utilizado para el tratamiento del brillo y la cromaticidad, donde la función $E(\mathbf{u})$ corresponde a
\begin{equation} \label{eq:energia}
E(\mathbf{u}) = \int_{\Omega} \norm{\nabla \mathbf{u}}^p \di x\di y,
\end{equation}
que se denominará energía en el resto del documento, y será aplicable al brillo $M$ (con $p=2$) y a la cromaticidad $\mathbf{D}$, donde se utilizará el valor de $p$ asociado al tipo de flujo, isotrópico o anisotrópico, según corresponda. Destacar que esta función es un buen indicador de la cantidad ruido de la imagen, pues la norma del gradiente de $\mathbf{u}$ es mayor en aquellas zonas donde haya más ruido.

\input{code/algs/grad-descent}

Para este algoritmo se han escogido dos criterios de parada. Por un lado, un número máximo de iteraciones y, por otro, el método se detendrá cuando la función $E(\mathbf{u}(x,y,t_k))$ sea menor o igual que $\rho$ veces la energía inicial $E(\mathbf{u}(x,y,0))$.
Esta elección viene dada por el hecho de que la función $E(\mathbf{u})$ decrece a medida que $\norm{\nabla \mathbf{u}(x,y,t_k)} \rightarrow 0$ y tiene su mínimo global en $\norm{\nabla \mathbf{u}(x,y,t_k)} = 0, \forall \left( x,y \right) \in \Omega$, que corresponde al momento en que se encuentra la solución estacionaria. Sin embargo, en el problema de difusión de brillo y cromaticidad, llegar a ese mínimo produce una imagen totalmente difuminada. Por ello, se debe detener la ejecución del algoritmo antes de llegar a la solución estacionaria.

\input{secs/difusivos/problema-discreto}
