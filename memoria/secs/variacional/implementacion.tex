\section{Implementación} \label{sec:variacional-implementacion}
Para la resolución de este problema se ha realizado una implementación en C++ que se apoya principalmente en la librería OpenCV~\cite{opencv_library} para la carga, escritura y muestra de las imágenes. La librería Eigen~\cite{eigenweb} también ha sido utilizada para la realización de algunos cálculos, aunque su relevancia en el código final es residual. Por último, se utilizan OpenMP~\cite{Dagum1998openmp} para paralelizar el código en CPU y CUDA~\cite{Nickolls2008cuda} para paralelizarlo en GPU.
El código asociado a este programa, como todo el relacionado con este trabajo se encuentra disponible en un repositorio público en GitLab, ver~\cite{laso2018tfmrepo}.

\subsection{Arquitectura de la aplicación}
La aplicación, dada su naturaleza, necesita de pocas clases C++, y las que hay están relacionadas con las estructuras de datos implementadas \textit{ad-hoc} para este trabajo.

% TODO comentar brevemente el esquema de la aplicacion

%En la figura~\ref{fig:variacional-clases} se muestra un diagrama de clases de la aplicación. Se debe comentar, que se han implementado dos librerías con algoritmos matemáticos en los ficheros \verb|Math.hpp| para OpenMP y \verb|Math_CUDA.hpp| y \verb|Math_CUDA.cu| para CUDA que no aparecen en el diagrama de clases, aunque la clase \verb|Image| hace uso de ellas.

%\input{figs/variacional/clases} % TODO: acabar el diagrama de clases



\subsection{Resolución numérica}
Para la resolución del sistema del problema~\ref{pro:rof-final} se han utilizado diferentes opciones, algunas apoyadas en implementaciones de la librería Eigen y otras implementadas \textit{ad-hoc}.

En cuanto a los métodos implementados por la librería Eigen, primero se utilizó el método de Cholesky, propuesto en~\cite{nuno2018tfg}. Sin embargo, este método se mostró relativamente lento en comparación, por ejemplo al método del gradiente conjugado.

Dada la fuerte componente \textit{sparse} de la matriz (con un máximo de trece elementos no nulos en cada fila), el método del gradiente conjugado obtiene un mejor rendimiento computacional debido a que la parte más importante en cuanto a tiempo de cómputo se encuentra en el producto matriz-vector. En el algoritmo~\ref{alg:conj-gradient} se muestra el conjunto de operaciones a realizar en este método.

\input{code/algs/conj-gradient}

\subsubsection{Estructura \texttt{BilapSparse}}
Para almacenar la matriz $L_h$ del sistema~\eqref{eq:sistema-variacional} y sus coeficientes $c_1, \dots, c_{13}$, mostrados en~\eqref{eq:coeficientes-bilap}, se ha implementado una estructura de datos específica para este formato de matriz dispersa (ver fichero \verb|include/BilapSparse.hpp| en el repositorio~\cite{laso2018tfmrepo}).

Dado que la posición de los elementos no nulos es fija en función del tamaño de la matriz, no es necesario almacenar todos los elementos de la matriz, sino que realizando simples cálculos con los índices se puede conocer el valor del elemento respectivo. De esta forma, además de ahorrar una cantidad considerable de espacio en memoria, se realiza un acceso más efectivo a los elementos pues, en general, buscar datos en memoria suele ser más costoso que efectuar unas pocas operaciones aritméticas. Esto nos ha permitido implementar varias funciones computacionalmente eficientes para el acceso a los elementos de la matriz:

\begin{itemize}
	\item \verb|T at(size_t i, size_t j)|: devuelve el elemento de $L_h$ en la posición $(i,j)$ utilizando operaciones aritméticas sobre los índices $i,j$ en lugar de saltos en memoria.

	\item \verb|std::vector<size_t> nz(size_t row, std::vector<T> &values)|: almacena en \verb|values| los elementos no nulos de la fila \verb|row| de $L_h$ y devuelve un vector con sus índice dentro de dicha fila.

	\item \verb|void prod_vec(const T * restrict v, T * restrict res)|: implementación eficiente del producto matriz-vector, de forma que se obtenga $\mathtt{res} = L_h \mathtt{v}$. Esta implementación solo efectúa un máximo de 13 multiplicaciones y 12 sumas por elemento de \verb|res|, dado que sus expresiones son conocidas previamente.
	% QUESTION: ¿conveniente poner las expresiones de los productos?
\end{itemize}

\subsubsection{Implementación usando Eigen}
A este algoritmo se le pueden incluir elementos como un precondicionador, para mejorar su efectividad, tal y como hace la librería Eigen. El problema de utilizar esta librería es que se han de usar las estructuras de datos de la misma.

De esta forma, para utilizar el \textit{solver} de Eigen es necesario ``rellenar'' su tipo de matriz \textit{sparse} (\verb|Eigen::SparseMatrix|) y su formato de vector (\verb|Eigen::VectorXf|). Para la matriz, podemos utilizar el método \verb|nz| proporcionado por \verb|BilapSparse|, utilizando el número mínimo de operaciones posibles, mientras que para el vector tenemos un acceso secuencial a sus elementos, de forma que ya es eficiente en caché.

Sin embargo, este enfoque tiene varios puntos en contra. Primero, aunque la operación de ``rellenar'' las estructuras de Eigen sea lo más eficiente, sigue teniendo un coste computacional relativamente elevado, al que hay que añadir el proceso inverso al finalizar el cómputo del sistema. Segundo, el método es general, por lo que en el cómputo del producto matriz-vector se realizan $MN$ operaciones por fila de la matriz, donde la mayoría de los elementos serán nulos y no contribuirán nada al cálculo. En este caso, la complejidad computacional del producto matriz-vector es de $\bigO(n^2)$, siendo $n = NM$ el tamaño del vector. Por último, el control sobre la paralelización de esta implementación es prácticamente nulo al tratarse de una librería externa.

En cualquier caso, la mejora en el tiempo de ejecución en comparación a la implementación propuesta en~\cite{nuno2018tfg} es más que considerable.

\subsubsection{Implementación \textit{ad-hoc}}
Para subsanar los problemas de la librería Eigen en el uso del gradiente conjugado, se ha optado por realizar una implementación del método única y específicamente para este problema.

En esta implementación se hará un uso intensivo de la función \verb|prod_vec()| de la clase \verb|BilapSparse|. Esta función nos permite realizar un máximo de trece multiplicaciones y doce sumas por elemento del vector resultado, en lugar de $NM$. De esta forma, el producto matriz-vector solo tiene complejidad $\bigO(25n)$, disminuyendo el número de operaciones y ahorrando tiempo de cálculo.

En esta implementación no se incluye un precondicionador, pues no se ha encontrado que su uso reduzca el número de iteraciones del método y, por ende, el tiempo de ejecución.

Con estos cambios, la implementación \textit{ad-hoc}, además de ahorrar espacio en memoria, consigue tiempos de ejecución considerablemente mejores que los conseguidos por la implementación que utiliza Eigen (ver capítulo~\ref{cha:resultados}).

\subsection{Paralelización}
Todas las implementaciones realizadas para este método de reducción de ruido han sido paralelizadas con el objetivo de aprovechar las arquitecturas modernas de CPU y GPU. En esta sección prestaremos atención a las partes de los programas más relevantes a la hora de aplicar el paralelismo.

% TODO
