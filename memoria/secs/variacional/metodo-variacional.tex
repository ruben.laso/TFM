\section{Método variacional}
En primer lugar, se ha de notar que este método está pensado para el tratamiento de imágenes en escala de grises. Por ello, se define la imagen con ruido como $f: \Omega \rightarrow \mathbb{R}$.

El método variacional aquí propuesto toma como punto de partida la búsqueda de una función $u: \Omega \rightarrow \mathbb{R}$ que represente la imagen original (sin ruido). Así se puede parametrizar la superficie de $u$ tal que:
\begin{equation*}
	\Phi(x,y) = (x, y, u(x,y)), \quad (x, y) \in \Omega
\end{equation*}

A partir de aquí, es posible utilizar la curvatura media de la superficie $\Phi(x,y)$ como la función de regularización de~\eqref{eq:rof-general}, tal y como se propone en~\cite{zhu2012image}.

Primero, se deben introducir las definiciones tanto de formas fundamentales de una superficie~\cite{weisstein2014wolfram-first-fund-form, weisstein2014wolfram-secnd-fund-form} como de curvatura media~\cite{ye1996gaussian}.

\begin{definition}
	Sea $S(u,v)$ una superficie regular y $\mathbf{v}_p, \mathbf{w}_p$ puntos en el espacio tangente $S_p$ de $S$. Entonces, su primera forma fundamental viene dada por el producto escalar de los vectores tangentes:
	\begin{equation*}
		\mathrm{I}(\mathbf{v}_p, \mathbf{w}_p) = \mathbf{v}_p \cdot \mathbf{w}_p.
	\end{equation*}
	Y, para un vector $\mathbf{x}$ tangente a $S$ cuyas derivadas respecto de $u$ y $v$ son $\mathbf{x}_u$ y $\mathbf{x}_v$ respectivamente, satisface:
	\begin{equation*}
		\mathrm{I}(a \mathbf{x}_u + b \mathbf{x}_v, a \mathbf{x}_u + b \mathbf{x}_v) = E a^2 + 2 F a b + G b^2,
	\end{equation*}
	donde
	\begin{equation*}
		E = \norm{\mathbf{x}_u}^2, \quad
		F = \mathbf{x}_u \cdot \mathbf{x}_v, \quad
		G = \norm{\mathbf{x}_v}^2.
	\end{equation*}
\end{definition}

\begin{definition}
	Sea $S(u,v)$ una superficie regular y $\mathbf{v}_p, \mathbf{w}_p$ puntos en el espacio tangente $S_p$ de $S$. Para $S \in \mathbb{R}^3$, la segunda forma fundamental viene dada por:
	\begin{equation*}
		\mathrm{II}(\mathbf{v}_p, \mathbf{w}_p) = S_h(\mathbf{v}_p) \cdot \mathbf{w}_p,
	\end{equation*}
	siendo $S_h$ el operador de forma. Así, para un vector $\mathbf{x}$ tangente a $S$ cuyas derivadas respecto de $u$ y $v$ son $\mathbf{x}_u$ y $\mathbf{x}_v$ respectivamente, la segunda forma fundamental cumple que
	\begin{equation*}
		\mathrm{II}(a \mathbf{x}_u + b \mathbf{x}_v, a \mathbf{x}_u + b \mathbf{x}_v) = e a^2 + 2 f a b + g b^2,
	\end{equation*}
	%para cualquier vector tangente a $S$ no nulo y
	donde
	\begin{equation*}
		e = \mathbf{n} \cdot \mathbf{x}_{uu}, \quad
		f = \mathbf{n} \cdot \mathbf{x}_{uv}, \quad
		g = \mathbf{n} \cdot \mathbf{x}_{vv},
	\end{equation*}
	siendo $\mathbf{n}$ el vector unitario normal a $S$.
\end{definition}

\begin{definition}
%	Sea $S$ una superficie y $\kappa_1$ y $\kappa_2$ sus curvaturas principales, la curvatura media $H$ de $S$ en un punto $p \in S$ vendrá dada por:
%	\begin{equation*}
%		H = \frac{1}{2} \left(\kappa_1 + \kappa_2\right).
%	\end{equation*}
%	Teniendo $E, F, G$ y $e, f, g$ como los coeficientes de la primera y segunda forma fundamental respectivamente, podemos llegar a:
%	\begin{equation} \label{eq:mean-curv}
%		H = \frac{1}{2} \frac{eG - 2fF + gE}{EG -F^2}.
%	\end{equation}
	Para una superficie $S$, su curvatura media $H$ en un punto $p \in S$ viene dada por:
	\begin{equation} \label{eq:mean-curv}
		H = \frac{1}{2} \frac{eG - 2fF + gE}{EG -F^2},
	\end{equation}
	donde $E, F, G$ y $e, f, g$ corresponden a los coeficientes de la primera y segunda forma fundamental, respectivamente, definidos anteriormente.
\end{definition}

Para calcular la curvatura media, primero se debe definir el vector unitario normal a la superficie $\Phi(x,y)$.

\begin{definition}
	Sea $S(u,v)$ una superficie regular, su vector unitario normal $\mathbf{n}$ viene dado por
	\begin{equation*}
		\mathbf{n}(u,v) = \frac{S_u \times S_v}{\norm{S_u \times S_v}}.
	\end{equation*}
\end{definition}

Para la superficie $\Phi(x,y)$ se tiene:
\begin{align*}
	\Phi_x &= \left(1, 0, u_x\right),
	\\
	\Phi_y &= \left(0, 1, u_y\right),
	\\
	\Phi_{xx} &= \left(0, 0, u_{xx}\right),
	\\
	\Phi_{yy} &= \left(0, 0, u_{yy}\right),
	\\
	\Phi_{xy} &= \Phi_{yx} = \left(0, 0, u_{xy}\right).
\end{align*}
Con esto puede calcularse $\mathbf{n}(x,y)$:
\begin{equation*}
	\mathbf{n}(x,y) = \frac{\Phi_x \times \Phi_y}{\norm{\Phi_x \times \Phi_y}} = \frac{\left(-u_x, -u_y, 1\right)}{\left(u_x^2 + u_y^2 + 1\right)^{\frac{1}{2}}}.
\end{equation*}
Los coeficientes de la primera y la segunda forma fundamental de $\Phi(x,y)$ son:
\begin{align*}
	E &= \norm{\Phi_x}^2 = 1 + u_x^2,
	\\
	F &= \Phi_x \cdot \Phi_y = u_{xy},
	\\
	G &= \norm{\Phi_y}^2 = 1 + u_y^2,
	\\
	e &= \mathbf{N} \cdot \Phi_{xx} = \frac{u_{xx}}{\left(u_x^2 + u_y^2 + 1\right)^{\frac{1}{2}}},
	\\
	f &= \mathbf{N} \cdot \Phi_{xy} = \frac{u_{xy}}{\left(u_x^2 + u_y^2 + 1\right)^{\frac{1}{2}}},
	\\
	g &= \mathbf{N} \cdot \Phi_{yy} = \frac{u_{yy}}{\left(u_x^2 + u_y^2 + 1\right)^{\frac{1}{2}}}.
\end{align*}
Sustituyendo en~\eqref{eq:mean-curv}, se llega a que la curvatura media es
\begin{equation*} \label{eq:mean-curv-u}
	H_u
	=
	\frac{1}{2} \frac{eG - 2fF + gE}{EG -F^2}
	=
	\frac{1}{2} \frac{\left(1 + u_y^2\right) u_{xx} - 2 u_x u_y u_{xy} + \left(1 + u_x^2\right) u_{yy}}{\left(1 + u_x^2 + u_y^2\right)^{\frac{3}{2}}}.
\end{equation*}

A partir de aquí, suponiendo que $\abs{\nabla{u}} \ll 1$ y las derivadas parciales de segundo orden están uniformemente acotadas, se puede demostrar que
\begin{equation*}
	H_u \simeq L_u = \frac{1}{2} \left(u_{xx} + u_{yy}\right) = \frac{1}{2} \Delta{u}.
\end{equation*}

\begin{proof}
	Si $\abs{\nabla{u}} \ll 1$ y las derivadas parciales de segundo orden están uniformemente acotadas, se tiene que $H_u \simeq L_u$.

	Para demostrar esto, se calcula la diferencia de $H_u$ y $L_u$:
	\begin{gather*}
		\abs{H_u - L_u}
		=
		\frac{1}{2}
		\abs{
		\frac{
				\left(1 + u_y^2\right) u_{xx} - 2 u_x u_y u_{xy} + \left(1 + u_x^2\right) u_{yy}
			}
			{
				\left(1 + u_x^2 + u_y^2\right)^{\frac{3}{2}}
			}
			-
			\left(u_{xx} + u_{yy}\right)
		}
		\\
		=
		\frac{1}{2}
		\abs{
		\frac{
				\left(1 + u_y^2\right) u_{xx}
				-
				2 u_x u_y u_{xy}
				+
				\left(1 + u_x^2\right) u_{yy}
				- \left(u_{xx} + u_{yy}\right)\left(1 + u_x^2 + u_y^2\right)^{\frac{3}{2}}
			}
			{
				\left(1 + u_x^2 + u_y^2\right)^{\frac{3}{2}}
			}
		}.
	\end{gather*}
	Aplicando que $\abs{\nabla{u}} \ll 1$, se obtiene la siguiente igualdad aproximada:
	\begin{gather*}
		\abs{H_u - L_u}
		\simeq
		\frac{1}{2}
		\abs{
			\left(1 + u_y^2\right) u_{xx}
			-
			2 u_x u_y u_{xy}
			+
			\left(1 + u_x^2\right) u_{yy}
			- \left(u_{xx} + u_{yy}\right)
		}.
	\end{gather*}
	Conociendo que $\abs{u_\alpha} \leq \abs{\nabla{u}} \ll 1$
	para $\alpha \in \left\lbrace x, y\right\rbrace$, se tiene que:
	\begin{gather*}
		\abs{H_u - L_u}
		\simeq
		\frac{1}{2}
		\abs{
			u_{xx} + u_{yy} - u_{xx} - u_{yy}
		}
		=
		0.
	\end{gather*}
\end{proof}

%Concretando~\eqref{eq:rof-general}, podemos plantear el siguiente problema:
A continuación, se plantea un caso particular del problema general~\ref{pro:rof-general}:
\begin{problem} \label{pro:rof-variacional}
	Hallar $u: \Omega \rightarrow \mathbb{R}$ tal que:
	\begin{equation} \label{eq:rof-variacional}
		\min_u E(u) = \frac{\lambda}{2} \int_{\Omega} \abs{\Delta u}^2\di x\di y + \frac{1}{2} \int_{\Omega} (f - u)^2\di x\di y,
	\end{equation}
	donde $\lambda > 0$  es un parámetro de ajuste, la primera integral representa el término de regularización y la segunda el de fidelidad.
\end{problem}

Suponiendo que $u$, solución del problema~\ref{pro:rof-variacional}, es una función regular, puede demostrarse que es una solución de la ecuación en derivadas parciales de Euler-Lagrange.

\begin{proof}
	Se parte de una función regular $v: \Omega \rightarrow \mathbb{R}$, con soporte compacto en $\Omega$, de forma que $v$ y sus derivadas de primer orden se anulan en $\Gamma = \partial \Omega$, y de la función $i: \mathbb{R} \rightarrow \mathbb{R}$ tal que
	\begin{equation*}
		i(\tau) = E(u + \tau v).
	\end{equation*}
	Asumiendo que $u$ es un mínimo de~\eqref{eq:rof-variacional}, entonces $i(\tau)$ tiene un mínimo relativo en $\tau = 0$:
	\begin{equation*}
		i'(\tau)_{\mid \tau = 0} = 0.
	\end{equation*}
%
	Así pues, es necesario calcular $i'(\tau)$. Primero se desarrolla $i(\tau)$:
	\begin{align*}
		i(\tau)
		=&
		E(u + \tau v)
		=
		\frac{\lambda}{2} \int_{\Omega} \left(\Delta(u + \tau v)\right)^2\di x\di y
		+
		\frac{1}{2} \int_{\Omega} (f - (u + \tau v))^2\di x\di y
		\\
		=&
		\frac{\lambda}{2}
		\int_{\Omega} \left(\frac{\partial^2 (u + \tau v)}{\partial x^2} + \frac{\partial^2 (u + \tau v)}{\partial y^2} \right)^2 \di x\di y
		+
		\frac{1}{2} \int_{\Omega} (f - (u + \tau v))^2\di x\di y
		\\
		=&
		\frac{\lambda}{2}
		\int_{\Omega}
			\left(\frac{\partial^2 \left(u + \tau v\right)}{\partial x^2} \right)^2
			+
			\left(\frac{\partial^2 \left(u + \tau v\right)}{\partial y^2} \right)^2
			+
			\\
			&
			2
			\left(
				\left( \frac{\partial^2 \left(u + \tau v\right)}{\partial x^2} \right)
				\left( \frac{\partial^2 \left(u + \tau v\right)}{\partial y^2} \right)
			\right)
		\di x\di y
		+
		\frac{1}{2} \int_{\Omega} (f - (u + \tau v))^2\di x\di y,
	\end{align*}
	\begin{align*}
		i(\tau)
		=&
		\frac{\lambda}{2}
		\int_{\Omega}
			\left(
				\frac{\partial^2 u}{\partial x^2}
				+
				\tau \frac{\partial^2 v}{\partial x^2}
			\right)^2
			+
			\left(
				\frac{\partial^2 u}{\partial x^2}
				+
				\tau \frac{\partial^2 v}{\partial y^2}
			\right)^2
			+
			\\
			&
			2
			\left(
				\left( \frac{\partial^2 u}{\partial x^2} + \tau \frac{\partial^2 v}{\partial x^2} \right)
				\left( \frac{\partial^2 u}{\partial y^2} + \tau \frac{\partial^2 v}{\partial y^2} \right)
			\right)
		\di x\di y
		+
		\\
		&
		\frac{1}{2} \int_{\Omega} (f - (u + \tau v))^2\di x\di y.
	\end{align*}
	Derivando $i(\tau)$ se obtiene:
	\begin{align*}
		i'(\tau)
		=&
		\lambda
		\int_{\Omega}
			\left(
				\frac{\partial^2 v}{\partial x^2}
				\left(
					\frac{\partial^2 u}{\partial x^2}
					+
					\tau \frac{\partial^2 v}{\partial x^2}
				\right)
				+
				\frac{\partial^2 v}{\partial y^2}
				\left(
					\frac{\partial^2 u}{\partial y^2}
					+
					\tau \frac{\partial^2 v}{\partial y^2}
				\right)
			\right)
		\di x\di y
		+
		\\&
		\lambda
		\int_{\Omega}
			\left(
				\frac{\partial^2 v}{\partial x^2}
				\left(
					\frac{\partial^2 u}{\partial y^2}
					+
					\tau \frac{\partial^2 v}{\partial y^2}
				\right)
				+
				\frac{\partial^2 v}{\partial y^2}
				\left(
					\frac{\partial^2 u}{\partial x^2}
					+
					\tau \frac{\partial^2 v}{\partial x^2}
				\right)
			\right)
		\di x\di y
		-
		\\&
		\int_{\Omega}
			\left(f - u - \tau v\right)v
		\di x\di y.
	\end{align*}
	Si se evalúa la función $i'(\tau)$ en $\tau = 0$, se cumple que:
	\begin{align*}
		i'(0)
		=&
		\lambda \int_{\Omega}
			\left(
				\frac{\partial^2 u}{\partial x^2} \frac{\partial^2 v}{\partial x^2}
				+
				\frac{\partial^2 u}{\partial y^2} \frac{\partial^2 v}{\partial y^2}
				+
				\frac{\partial^2 u}{\partial x^2} \frac{\partial^2 v}{\partial y^2}
				+
				\frac{\partial^2 u}{\partial y^2} \frac{\partial^2 v}{\partial x^2}
			\right)
		\di x\di y
		-
		\\&
		\int_{\Omega}
			\left(f - u \right)v
		\di x\di y.
	\end{align*}
	En este punto, se puede aplicar el teorema de Green de forma repetida sobre todos los sumandos (teniendo en cuenta las condiciones de $v$ y de sus derivadas sobre $\Gamma$). Por ejemplo, para el primer sumando se calcula:
	\begin{align*}
		\int_{\Omega}
			\frac{\partial^2 u}{\partial x^2}
			\frac{\partial^2 v}{\partial x^2}
		\di x \di y
		=&
		-\int_{\Omega}
			\frac{\partial^3 u}{\partial x^3}
			\frac{\partial v}{\partial x}
		\di x\di y
		+
		\int_{\Gamma}
			\frac{\partial^2 u}{\partial x^2}
			\frac{\partial v}{\partial x}
			n_x
		\di \gamma
		\\
		=&
		\int_{\Omega}
			\frac{\partial^4 u}{\partial x^4} v
		\di x\di y
		-
		\int_{\Gamma}
			\frac{\partial^3 u}{\partial x^3} v
			n_x
		\di \gamma
		\\
		=&
		\int_{\Omega}
			\frac{\partial^4 u}{\partial x^4} v
		\di x\di y,
	\end{align*}
	donde $\mathbf{n} = \left(n_x, n_y\right)$ denota al vector normal unitario exterior a $\Gamma$ y $\di \gamma$ su elemento diferencial. Así, se llega a la expresión simplificada:
	\begin{equation} \label{eq:i-prima-cero}
		i'(0)
		=
		\lambda
		\int_{\Omega}
			\left(
				\frac{\partial^4 u}{\partial x^4}
				+
				\frac{\partial^4 u}{\partial y^4}
				+
				2\frac{\partial^4 u}{\partial x^2 y^2}
			\right)
			v
		\di x\di y
		-
		\int_{\Omega}
			\left(f - u\right) v
		\di x\di y
		=
		0.
	\end{equation}
	Teniendo en cuenta que
	\begin{equation*}
		\Delta^2 u
		=
		\frac{\partial^4 u}{\partial x^4}
		+
		\frac{\partial^4 u}{\partial y^4}
		+
		2\frac{\partial^4 u}{\partial x^2 y^2},
	\end{equation*}
	se puede reescribir~\eqref{eq:i-prima-cero}:
	\begin{equation*}
		0
		=
		\lambda\int_{\Omega}
			\Delta^2 u v
		\di x\di y
		-
		\int_{\Omega}
			\left(f - u\right) v
		\di x\di y,
	\end{equation*}
	para toda función regular $v$ con soporte compacto en $\Omega$, verificando la ecuación de Euler-Lagrange:
	\begin{equation*}
		\lambda \Delta^2 u - (f - u) = 0 \text{ en } \Omega.
	\end{equation*}
\end{proof}

Con todo ello, se obtiene el problema:
\begin{problem} \label{pro:rof-final}
	Hallar $u: \Omega \rightarrow \mathbb{R}$ tal que:
	\begin{equation*}
		\begin{dcases}
		\lambda \Delta^2 u + u = f \text{ en } \Omega,
		\\
		u = 0 \text{ sobre } \Gamma,
		\\
		\frac{\partial u}{\partial \mathbf{n}} \text{ sobre } \Gamma,
		\end{dcases}
	\end{equation*}
	donde $\displaystyle \frac{\partial u}{\partial \mathbf{n}} = n_x u_x + n_y u_y = \nabla{u} \cdot \mathbf{n}$.
\end{problem}
