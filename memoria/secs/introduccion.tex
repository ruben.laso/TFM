\chapter{Introducción} \label{cha:introduccion}
En este primer capítulo se realiza una breve introducción a los conceptos de imagen digital empleados en este Trabajo de Fin de Máster, con la finalidad de que el lector, en caso de no estar familiarizado con la materia, pueda comprender fácilmente los términos y conceptos utilizados en el resto del documento.
%Así pues, se comentarán los tipos de imágenes que se tratarán en este trabajo, los tipos de ruido que se pueden dar en estar imágenes y un breve listado de los métodos de reducción de ruido más habituales.

\section{Imágenes digitales}
Una imagen digital rasterizada puede ser entendida como una matriz~\cite{gonzalez2008digital}, $\mathbf{I} \in \mathcal{M}_{M\times N}(\mathbb{R}^c)$, donde cada entrada (que denotaremos como píxel) tiene $c$ componentes (o canales). En el caso $c = 1$ se tiene una matriz que, generalmente, representa una imagen en escala de grises. En cambio, si $c \geq 2$, la matriz corresponde a una imagen en color, multi o hiperespectral, con transparencias, animadas, etc.

Habitualmente, cada uno de los canales de información se codifica con enteros de $n$ bits, siendo $n$ la profundidad del color, de forma que toman valores comprendidos entre $0$ y $2^n-1$.

En este TFM, trabajaremos sobre los siguientes tipos de imágenes:
\begin{itemize}
	\item Escala de grises: estas imágenes contienen un único canal ($c = 1$) de información asociado al brillo o intensidad de cada uno de los píxeles. En estas imágenes, el valor $0$ representaría el negro más profundo, mientras que $2^n - 1$ sería completamente blanco.

	\item RGB: se asocia cada canal a uno de los tres colores primarios (\textit{Red, Green, Blue}). Dado que cuenta con tres canales de color ($c = 3$), cada píxel vendrá representado por un vector de tres componentes, de forma que con $n$ bits por canal podemos tener hasta $2^{3n}$ colores diferentes.
\end{itemize}

\section{Ruido en imágenes digitales} \label{sec:ruido}
El ruido en imágenes digitales se puede definir como las perturbaciones en el color que sufre una imagen. Estas perturbaciones pueden venir de múltiples fuentes, como el propio ruido del sensor, que la imagen se haya tomado en condiciones de baja iluminación, compresión deficiente de la imagen, etc.

En general, se puede modelar una imagen ruidosa $\mathbf{\hat{I}} \in \mathcal{M}_{M \times N} (\mathbb{R}^c)$ como
\begin{equation} \label{eq:ruido}
	\mathbf{\hat{I}} = \mathbf{I} + \mathbf{N},
\end{equation}
siendo $\mathbf{I} \in \mathcal{M}_{M \times N} (\mathbb{R}^c)$ la imagen original y $\mathbf{N} \in \mathcal{M}_{M \times N} (\mathbb{R}^c)$ el ruido que presenta.

En función de la forma del ruido, este puede ser clasificado. Los dos tipos más destacables de ruidos son:
\begin{itemize}
	\item Ruido gaussiano (figura~\ref{fig:cameraman-gaussian}): también conocido como AWGN (\textit{Additive White Gaussian Noise}). Este tipo de ruido se da típicamente en el proceso de captura de la imagen y afecta a cada píxel de forma aditiva, individual e independientemente de la intensidad de la señal. Además, este tipo de ruido sigue una distribución próxima a la gaussiana,
	\begin{equation*}
		p(x,\mu,\sigma) = \frac{1}{\sigma \sqrt{2\pi}} e^{-\frac{(x-\mu)^2}{2\sigma^2}},
	\end{equation*}
	con media $\mu$ (generalmente centrada en $0$), desviación típica $\sigma$ y una variable $x$ que, en este caso, representa el valor del píxel.

	\item \textit{Salt \& pepper} (figura~\ref{fig:cameraman-salt-pepper}): este ruido provoca una desviación extrema del valor del píxel o de, al menos, uno de sus canales a $0$ o a $2^n - 1$. Habitualmente, este tipo de ruido se produce en el proceso de transmisión o compresión de la imagen.

%	\item Ruido de disparo: también denominado ruido de Poisson, pues sigue esta distribución de la forma
%	\begin{equation*}
%		p(\lambda, k) = e^{-\lambda} \frac{\lambda^k}{k!}, \quad k =0, 1, 2, \dots
%	\end{equation*}
%	que nos dará la probabilidad de que sucedan $k$ eventos, donde $\lambda$ mide el número medio de eventos dentro de un intervalo.
%	Este tipo de ruido se da en las zonas más oscuras de la imagen durante el proceso de captura debido a que menos fotones llegan al sensor en el momento de tomar la imagen.
\end{itemize}

\begin{figure}[htbp]
	\centering
	\begin{subfigure}[t]{.3\textwidth}
		\centering
		\includegraphics[width=\linewidth]{figs/noise_types/cameraman}
		\caption{Original.}
		\label{fig:cameraman-orig}
	\end{subfigure}
%	\qquad
	\begin{subfigure}[t]{.3\textwidth}
		\centering
		\includegraphics[width=\linewidth]{figs/noise_types/cameraman_gaussian}
		\caption{Ruido gaussiano.}
		\label{fig:cameraman-gaussian}
	\end{subfigure}
%	\\
%	\begin{subfigure}[t]{.45\textwidth}
%		\centering
%		\includegraphics[width=\linewidth]{figs/noise_types/cameraman_poisson}
%		\caption{Ruido de disparo.}
%		\label{fig:cameraman-poisson}
%	\end{subfigure}
%	\qquad
	\begin{subfigure}[t]{.3\textwidth}
		\centering
		\includegraphics[width=\linewidth]{figs/noise_types/cameraman_salt_pepper}
		\caption{Ruido \textit{salt \& pepper}.}
		\label{fig:cameraman-salt-pepper}
	\end{subfigure}
	\caption{Ejemplos de ruido sobre la imagen \texttt{cameraman.tif}.}
	\label{fig:cameraman-noise-types}
\end{figure}

Este trabajo se centra en la eliminación del ruido gaussiano, pues es el más habitual en fotografía convencional y más complicado de eliminar que el \textit{salt \& pepper}, por ejemplo. En cualquier caso, las técnicas descritas en este documento también se pueden utilizar para la reducción de otros tipos de ruido.

\section{Métodos para la reducción de ruido}
Debido a las diferentes características de cada modelo de ruido, se proponen diversos métodos en la literatura.
Al tratar una señal deben tenerse en cuenta ciertos factores para seleccionar el método a utilizar, tales como:
\begin{itemize}
	\item La adecuación del método para tratar el modelo de ruido que se maneja.

	\item La capacidad computacional disponible para tratar la imagen y el método escogidos.

	\item El equilibrio entre la reducción del ruido y la posible pérdida de información en la imagen tratada.
\end{itemize}
%
Los métodos más populares son:
\begin{itemize}
	\item Filtros lineales basados en convoluciones: estos métodos, generalmente, se basan en ``suavizar'' la imagen, de forma que cada píxel toma un valor medio (ponderado o no) de sí mismo y sus vecinos. Este tipo de tratamiento suele provocar un efecto de borrosidad en la imagen debido al suavizado.

	\item Filtros no lineales: como el de la mediana, donde cada píxel toma como valor la mediana de los valores de sus vecinos. Este tipo de filtro es especialmente útil ante ruidos como el \textit{salt \& pepper}.

	\item Transformaciones mediante \textit{wavelets}: en el dominio de los \textit{wavelets}, el ruido de la imagen se distribuye uniformemente en los coeficientes de la transformación, mientras que la información de la imagen se concentra en unos pocos muy significativos. Quedándose solo con aquellos coeficientes más relevantes, puede reducirse el ruido de la imagen.

	\item \textit{Block-matching algorithms}: estos algoritmos buscan porciones de la imagen similares entre sí, superponiéndolos y aplicando una media ponderada. Actualmente, este tipo de filtro es el estado del arte en reducción de ruido en imágenes.

	\item Métodos numéricos: se propone un problema cuya solución es la nueva imagen. Según el tipo de problema que se resuelva, pueden ser de los dos tipos siguientes:
	%Principalmente se dividen en dos categorías:
	\begin{itemize}
		\item Métodos variacionales: se resuelve un problema de minimización de un funcional que es resuelto mediante formulaciones variacionales.

		\item Métodos de difusión: la eliminación de ruido se puede modelar como un problema evolutivo de difusión utilizando ecuaciones en derivadas parciales (acopladas o no), de forma similar a como se haría para un problema de calor evolutivo.
	\end{itemize}
\end{itemize}

Principalmente, este trabajo se ha centrado en los métodos numéricos para la reducción del ruido. El objetivo será realizar una implementación de estas técnicas pensada en el aprovechamiento de las arquitecturas actuales de CPU y GPUs, para comparar tanto la calidad de su reconstrucción como su eficiencia computacional.

\section{Formato numérico de los resultados}
Todos los datos incluidos en este documento están
mostrados de forma que la coma~ (,) separa las unidades de millar y el punto~(.) es el separador decimal.
