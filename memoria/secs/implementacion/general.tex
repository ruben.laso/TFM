\section{Características generales}
En este trabajo se han implementado tres programas para los diferentes métodos considerados en este trabajo:
\begin{itemize}
	\item \verb|bw_var|: implementación del tratamiento de imágenes en blanco y negro por el método variacional (capítulo~\ref{cha:metodo-variacional}).

	\item \verb|color_var|: implementación del tratamiento de imágenes en color por el método variacional (sección~\ref{sec:variacional-color}).

	\item \verb|color_bc|: implementación del tratamiento de imágenes en color mediante métodos de difusión (capítulo~\ref{cha:metodos-difusivos}).
\end{itemize}
Adicionalmente, en la fase de compilación se obtendrán dos ejecutables de cada uno de los programas, uno que utiliza OpenMP~\cite{Dagum1998openmp} y otro que utiliza CUDA (al que se le añade el sufijo \verb|_cuda| para su identificación). Por otro lado, los programas destinados al Intel Xeon Phi KNL requieren de ciertas opciones de compilación singulares, por ello se generan ejecutables específicos de esta máquina (denotados con el sufijo~\verb|_knl|).

El lenguaje de programación utilizado ha sido C++ debido a su compatibilidad, alto rendimiento, a que permite una fácil integración de las tecnologías de cálculo paralelo y a que es un lenguaje orientado a objetos con soporte para la librería OpenCV~\cite{opencv_library}, que se utilizará para las operaciones de entrada/salida de las imágenes.

Por último, cabe mencionar que se tratarán las imágenes como matrices de \verb|float| en lugar de \verb|unsigned char| como es habitual. Esto se debe a que con \verb|unsigned char| se pierde precisión, puesto que trunca los decimales. La elección de \verb|float| en lugar de \verb|double| viene dada por la eficiencia asociada al menor espacio que ocupan los \verb|float|, 32~bits, en oposición a los 64~bits de un \verb|double|. De esta forma se maximiza el uso de las unidades vectoriales de las CPUs y los registros aritméticos de las GPUs, teniendo una precisión suficiente para los propósitos de este trabajo.

\subsection{Arquitectura de las aplicaciones}
Las aplicaciones, dada su naturaleza acotada, necesitan de un número reducido de clases, y las existentes están relacionadas con las estructuras de datos implementadas \textit{ad-hoc} para este trabajo.

En la figura~\ref{fig:diagrama-clases} se muestra un diagrama de clases simplificado de las aplicaciones\footnote{En este diagrama no se incluyen los métodos asociados a cada clase por cuestiones de espacio.}. Estas clases son:
\begin{itemize}
	\item \verb|Image|: clase que da soporte a las operaciones principales sobre las imágenes. Se encarga de las operaciones de entrada y salida e invocar a las rutinas necesarias para su tratamiento. Originalmente está pensada para el tratamiento de imágenes en escala de grises por el método variacional.

	\item \verb|ImageColor|: subclase de \verb|Image|. Sobreescribe los métodos para adecuarlos al tratamiento de imágenes en color por el método variacional.

	\item \verb|ImageColor_BC|: subclase de \verb|Image|. Sobreescribe los métodos para adecuarlos al tratamiento de imágenes en color por métodos difusivos.

	\item \verb|Matrix|: clase que da soporte a las operaciones sobre matrices utilizadas en este trabajo. Permite a las demás clases y librerías abstraerse de la estructura (en el sentido computacional) de la matriz.

	\item \verb|BilapSparse|: clase para el almacenamiento de la matriz del sistema del método variacional (ver problema~\ref{pro:rof-final-discreto}). Sus detalles se comentan en la sección~\ref{sec:bilap-sparse}.

	\item \verb|Vec3|: clase para la representación de vectores de tres elementos, pensada para el manejo de las tres componentes de una imagen en color.
\end{itemize}
Algunas de estas clases tienen una implementación específica para CUDA, aunque las diferencias se limitan al uso de determinadas \textit{keywords} propias de CUDA y comentarlas no aportaría contenido a este documento.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.82\textwidth]{figs/clases/clases}
	\caption{Diagrama de clases (simplificado) de las aplicaciones.}
	\label{fig:diagrama-clases}
\end{figure}

A mayores, se han implementado las librerías \verb|Math| y \verb|Math_CUDA| que incluyen el conjunto de rutinas necesarias y en las que las clases \verb|Image|, \verb|ColorImage| y \verb|Color_BC| se apoyan para realizar el tratamiento de imágenes.

\subsection{Compilación de los programas}
Entre los ficheros disponibles en el repositorio del código se encuentra un \verb|Makefile| para automatizar el proceso de compilación de los códigos. Nótese que para la compilación de los programas en CUDA se requiere que el compilador NVCC esté instalado en el sistema.
Otro requisito a la hora de compilar (y ejecutar) los programas es tener instalado OpenCV, utilizado para las operaciones de entrada/salida con imágenes.

\subsection{Ejecución de los programas}
Los programas se ejecutan en plataformas Linux y macOS de la forma habitual en estos sistemas, \verb|./ejecutable -n <imagen_ruidosa> [opciones]|. La lista de opciones de los programas se incluyen en el apéndice~\ref{ap:opciones-programas}, siendo la única obligatoria la opción \verb|-n| indicando el \textit{path} del fichero con la imagen a tratar.

Se debe mencionar que, utilizando la opción \verb|-o| y especificando la ruta al fichero con la imagen original, el programa imprime en pantalla el PSNR (definido en el capítulo~\ref{cha:resultados}) de la imagen reconstruida y la original.
