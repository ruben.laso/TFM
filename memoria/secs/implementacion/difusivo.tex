\section{Implementación de los métodos difusivos} \label{sec:implementacion-difusivos}
En esta sección se comentan algunos detalles sobre la implementación y paralelización del programa \verb|color_bc|, que realiza el tratamiento de imágenes explicado en el capítulo~\ref{cha:metodos-difusivos}.

\subsection{Arquitectura de la aplicación}
El programa \verb|color_bc| utiliza principalmente la clase \verb|ColorImage_BC| y las librerías \verb|Math| y \verb|Math_CUDA|. También para este programa se han implementado las clases \verb|Matrix| y \verb|Vec3| para el manejo de las matrices de brillo y de cromaticidad.

\subsection{Resolución} \label{sec:implementacion-difusivo}
%Como se comenta en el capítulo~\ref{cha:metodos-difusivos}, se utiliza una variación del método de descenso del gradiente (ver algoritmo~\ref{alg:grad-descent-difusivo}) para resolver el problema de difusión.
En el algoritmo~\ref{alg:grad-descent-difusivo} se muestran las operaciones necesarias para resolver los problemas de difusión introducidos en el capítulo~\ref{cha:metodos-difusivos}.
En este algoritmo existen dos operaciones principales, el cálculo de $E(\mathbf{u})$ y de $\mathbf{u}(x,y,t_{k+1})$.

Recordemos que
\begin{equation*}
	E(\mathbf{u}) = \int_{\Omega} \norm{\nabla \mathbf{u}}^p \di x\di y.
\end{equation*}
En esta expresión aparecen una integral y la norma de un gradiente que deben ser discretizadas. Para la aproximación del gradiente se utilizan diferencias finitas, tal y como se comenta en la sección~\ref{sec:problema-discreto} y para la aproximación de la integral se aprovecha la estructura de las imágenes y la definición del dominio $\Omega_h$. Así, la integral se aproxima como
\begin{multline} \label{eq:integracion}
	E(\mathbf{u}) = \int_{\Omega} \norm{\nabla \mathbf{u}}^p \di x\di y
	\\
	\simeq
	\sum_{i=1}^{N-1} \sum_{j=1}^{M-1} h_x h_y %A(u(x_i, y_j, t))
	\frac{\norm{\nabla\mathbf{u}_{i,j,k}}^p + \norm{\nabla\mathbf{u}_{i+1,j,k}}^p + \norm{\nabla\mathbf{u}_{i,j+1,k}}^p + \norm{\nabla\mathbf{u}_{i+1,j+1,k}}^p}{4}.
\end{multline}
Visualmente, se divide el dominio y la superficie de $\mathbf{u}(x,y,t)$ en pequeños paralelepípedos de dimensiones $h_x \times h_y \times a$, donde la altura $a$ del paralelepípedo viene dada por la media de la norma a la $p$-ésima potencia del gradiente de $\left\lbrace \mathbf{u}_{i,j,k}, \mathbf{u}_{i+1,j,k}, \mathbf{u}_{i,j+1,k}, \mathbf{u}_{i+1,j+1,k} \right\rbrace$, $i=1,\dots,N-1$, $j=1,\dots,M-1$. En la figura~\ref{fig:integration} se muestra un ejemplo de este tipo de aproximación.

\begin{figure}[htbp]
	\centering
	\begin{subfigure}[t]{.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{figs/difusivo/integration/integration}
		\caption{Aproximación de $E(\mathbf{u})$.}
		\label{fig:integration-omega}
	\end{subfigure}
 	\qquad
	\begin{subfigure}[t]{.45\textwidth}
		\centering
		\includegraphics[width=0.9\textwidth]{figs/difusivo/parallelepiped/parallelepiped}
		\caption{Paralelepípedo de dimensiones $h_x \times h_y \times a$.}
		\label{fig:parallelepipedo}
	\end{subfigure}
	\caption{Representación de la discretización de la integral.}
	\label{fig:integration}
\end{figure}

Es importante mencionar que no es necesaria una gran precisión en el cálculo de $E(\mathbf{u})$, puesto que solo se utilizará como un valor orientativo de la reducción del ruido en la imagen. Por ello, se prefiere utilizar este esquema pues proporciona un equilibrio entre precisión y eficiencia computacional, en detrimento de otras formas de integración más precisas aunque más demandantes en el cálculo.

Por otro lado, como se ha comentado en la sección~\ref{sec:resolucion}, se utiliza un esquema explícito para el cálculo de $\mathbf{u}(x, y, t_{k+1})$, dado que compensa reducir ligeramente el paso temporal para no tener que resolver un sistema de ecuaciones.
Debido a la elección de utilizar Euler explícito, es necesario tener dos matrices para el almacenamiento de $\mathbf{u}(x, y, t_k)$ y $\mathbf{u}(x, y, t_{k+1})$, cuyos punteros se intercambian consecutivamente para ahorrar copias de datos en memoria.

Los cálculos de las derivadas temporales tienen un gran efecto en términos de localidad, pues se recorren las matrices por filas, aprovechando así la ubicación de los datos en memoria caché. Esto palia el problema de utilizar diferencias finitas en este trabajo, puesto que el cálculo de cada elemento de $\mathbf{u}(x,y,t_{k+1})$ requiere el acceso a varios elementos de $\mathbf{u}(x,y,t_k)$. Si estos datos están ya en caché, no es necesario acceder a memoria para realizar los cálculos, lo que penalizaría significativamente el rendimiento.

En cuanto a la implementación de los métodos de diferencias finitas, el único caso en el que se pueden presentar problemas es en el uso del flujo anisotrópico para la cromaticidad. Si en~\eqref{eq:cromaticidad-anisotropico} se tiene que la norma del gradiente es cero, habrá una división entre cero que provocará una excepción de punto flotante o un NaN (\textit{Not a Number}). Por ello, se debe introducir una comprobación para evitar un fallo en la ejecución de la forma que se muestra en el código~\ref{cod:croma-aniso}.
%
\lstinputlisting[caption={Casos centrales del flujo anisotrópico para la cromaticidad.}, label={cod:croma-aniso}]{code/croma-aniso.cpp}
%
En esta sección de código se debe destacar el uso del condicional ternario~\cite{ternary-conditional}, mucho más eficiente y compacto que la sentencia condicional \verb|if| tradicional. Aún así, la introducción de este salto condicional supondrá una penalización en el rendimiento del flujo anisotrópico para la cromaticidad.

Por último, en~\cite{tang2001color} también se propone utilizar un filtro de la mediana para tratar la cromaticidad. La idea de este filtro es que el valor de cada píxel venga determinado por la mediana del valor de sus vecinos.
En el código~\ref{cod:median-filter} se muestra una sección de código correspondiente a este filtro.
%
\lstinputlisting[caption={Implementación del filtro de la mediana.}, label={cod:median-filter}]{code/median-filter.cpp}
%
Como esta es una propuesta secundaria, sólo realizaremos la implementación en OpenMP de este filtro.

\subsection{Paralelización en OpenMP}
La estructura regular de los esquemas de diferencias finitas empleados en los métodos difusivos facilita la aplicación de paralelismo al código. Aún así, se deben tener en cuenta ciertos detalles que permiten mejorar el rendimiento.

La definición correcta de la privacidad de las variables en la región paralela es importante para la ejecución de un código.
%Las directivas más relevantes a este respecto en OpenMP son:
%\begin{itemize}
%	\item \verb|shared|: los \textit{threads} comparten la variable, es decir, acceden a la misma posición de memoria para leer/escribir dicha variable.
%
%	\item \verb|private|: cada \textit{thread} tiene una copia privada de la variable en cuestión.
%
%	\item \verb|firstprivate|: las copias privadas de la variable vienen inicializadas al valor que esta tenía antes de entrar en la región paralela.
%\end{itemize}
Un ejemplo de uso de estas directivas se muestra en el código~\ref{cod:croma-iso}.
%
\lstinputlisting[caption={Ejemplo de uso de las directivas de privacidad de OpenMP.}, label={cod:croma-iso}]{code/croma-iso.cpp}
%
En este código debemos resaltar que \verb|a| y \verb|r| son compartidas, puesto que son referencias a objetos C++ y todos los \textit{threads} deben actualizar el mismo objeto. Las variables \verb|nr|, \verb|nc| y \verb|delta| deben ser \verb|firstprivate| para que cada hilo tenga su copia, aunque deben conservar el valor que tienen en las líneas~2,~3 y~4. Las variables \verb|dx|, \verb|dy|, \verb|lapl| y \verb|res| deben ser privadas puesto que cada \textit{thread} las utilizará para almacenar resultados parciales, y si fuesen compartidas se repsentarían problemas de carrera crítica que implicarían resultados incorrectos.

También cabe destacar en el código~\ref{cod:croma-iso} el uso de la directiva \verb|nowait| (línea~11). Con esta directiva se evita la sincronización al final del lazo o sección paralela, de forma que si un hilo acaba su trabajo en una sección paralela empezará el trabajo de la siguiente sección sin esperar a que los demás \textit{threads} acaben.
Esto es especialmente útil debido a que hay varios lazos paralelos consecutivos puesto que en los métodos de diferencias finitas se debe tener en cuenta que, por ejemplo, las derivadas en las fronteras se aproximan de forma diferente. Así se evita una gran cantidad de sincronizaciones que penalizarían el rendimiento del programa.

Por otro lado, para la aproximación de $E(\mathbf{u})$ se implementa un cálculo en dos pasos. En primera instancia calcularemos la norma del gradiente de $\mathbf{u}(x,y,t)$ en cada píxel de forma paralela. Posteriormente, se efectúa la reducción asociada a~\eqref{eq:integracion}.

\subsection{Paralelización en CUDA}
En CUDA no es necesario considerar la privacidad de las variables, puesto que todas aquellas variables declaradas dentro del \textit{kernel} serán privadas (salvo se ubiquen explícitamente en memoria compartida).

El cómputo de $\mathbf{u}(x,y,t)$ es similar al realizado en OpenMP, sin ninguna diferencia destacable más allá de la traducción del código habitual.

Por otro lado, el cálculo de $E(\mathbf{u})$ se realiza de forma diferente, en tres etapas. En la primera etapa se calcula $\norm{\nabla{\mathbf{u}(x,y,t)}}^p$. Al finalizar, se sincronizan los \textit{threads} CUDA con la función \verb|__syncthreads()| para realizar el cálculo de las alturas de cada uno de los paralelepípedos utilizados para aproximar la integral. Finalmente, se realiza una reducción de los volúmenes de estos paralelepípedos. Las dos primeras etapas se calculan en el mismo \textit{kernel} y para la reducción se utiliza la librería Thrust. % En el código~\ref{cod:cuda-energy} se muestra cómo se calcula la energía de la imagen.
%
%\lstinputlisting[caption={Cálculo de $E(u)$ en CUDA.}, label={cod:cuda-energy}]{code/cuda-energy.cpp}
