\section{Implementación del método variacional}
En esta sección se tratan los detalles de la implementación de los programas \verb|bw_var| y \verb|color_var| para la reducción de ruido en imágenes mediante el método variacional.

\subsection{Arquitectura de la aplicación}
Los programas \verb|bw_var| y \verb|color_var| utilizan las clases \verb|Image| y \verb|ColorImage| respectivamente, que se apoyan en la librería \verb|Math| (o \verb|Math_CUDA|) para el tratamiento de las imágenes. El punto más destacado es que se utiliza la clase \verb|BilapSparse| (o \verb|BilapSparse_CUDA|) para la resolución eficiente del sistema del problema~\ref{pro:rof-final-discreto}.

\subsection{Resolución numérica}
Para la resolución del sistema del problema~\ref{pro:rof-final} se han estudiado diferentes opciones, algunas utilizando implementaciones de la librería Eigen y otras implementadas \textit{ad-hoc}.

En cuanto a los métodos implementados por la librería Eigen, primero se utilizó el método de Cholesky, propuesto en~\cite{nuno2018tfg}. Sin embargo, este método se mostró relativamente lento en comparación, por ejemplo, al método del gradiente conjugado.

Dada la fuerte componente dispersa de la matriz (con un máximo de trece elementos no nulos en cada fila independientemente del tamaño de la imagen), el método del gradiente conjugado obtiene un mejor rendimiento computacional debido a que la parte más importante en cuanto a tiempo de cómputo se encuentra en el producto matriz-vector. En el algoritmo~\ref{alg:conj-gradient} se muestra el conjunto de operaciones a realizar en este método.

\input{code/algs/conj-gradient}

\subsubsection{Clase \texttt{BilapSparse}} \label{sec:bilap-sparse}
Para almacenar la matriz $(\lambda L_h + I)$ de~\eqref{eq:sistema-variacional} y sus coeficientes $c_1, \dots, c_{13}$, mostrados en~\eqref{eq:coeficientes-bilap}, se ha implementado una estructura de datos específica para este formato de matriz dispersa (ver fichero \verb|include/BilapSparse.hpp| en el repositorio~\cite{laso2018tfmrepo}).

Dado que la posición de los elementos no nulos es conocida en función del tamaño de la matriz, no es necesario almacenar todos los elementos de la matriz, sino que se pueden realizar simples cálculos con los índices para conocer el valor del elemento correspondiente. De esta forma, además de ahorrar una cantidad considerable de espacio en memoria, se realiza un acceso más eficiente a los elementos pues, en general, buscar datos en memoria suele ser más costoso que efectuar unas pocas operaciones aritméticas. Este hecho ha permitido implementar varias funciones computacionalmente eficientes para el acceso a los elementos de la matriz:

\begin{itemize}
	\item \verb|T at(size_t i, size_t j)|: devuelve el elemento de $(\lambda L_h + I)$ en la posición $(i,j)$ utilizando operaciones aritméticas sobre los índices $i,j$ en lugar de accessos a memoria.

	\item \verb|std::vector<size_t> nz(size_t row, std::vector<T> &values)|: almacena en \verb|values| los elementos no nulos de la fila \verb|row| de $(\lambda L_h + I)$ y devuelve un vector con sus índice dentro de dicha fila.

	\item \verb|void prod_vec(const T * restrict v, T * restrict res)|: implementa eficientemente del producto matriz-vector, de forma que se obtenga $\mathtt{res} = (\lambda L_h + I) \mathtt{v}$. Esta implementación solo efectúa un máximo de 13 multiplicaciones y 12 sumas por elemento de \verb|res|, dado que sus expresiones son conocidas previamente.
\end{itemize}

El trabajo con más impacto en términos de rendimiento es la implementación de la función \verb|prod_vec|. Dada la estructura singular de la matriz, en el producto $r = (\lambda L_h + I) v$ podemos conocer \textit{a priori} la expresión de un cierto elemento de $r$ por las dimensiones de $L_h$ y la imagen.
Debido a la cantidad de casuísticas en este producto matriz-vector, todas similares entre sí, sólo comentaremos los casos de los elementos centrales de $r$, que corresponden a los píxeles $\left( i, j \right)$ tal que $i \in \left\lbrace 3, \dots, N-2 \right\rbrace,\ j \in \left\lbrace 3, \dots, M-2 \right\rbrace$.
En el código completo se han implementado todos los casos (ver ficheros \verb|BilapSparse.hpp| y \verb|Math_CUDA| en~\cite{laso2018tfmrepo}).

Por ejemplo, para calcular el elemento $k$-ésimo de $r$ con $k = (j-1) N + i$, donde $N$ denota la dimensión de cada bloque de la matriz $L_h$ y $n = \frac{j}{N}-2$, tendremos que:
\begin{align*}
	r_k
	=
	\lambda
	(
		&
		c_1 v_{[nN+i]} +
		c_2 v_{[n(N+1)+i-1]} + c_3 v_{[n(N+1)+i]} + c_2 v_{[n(N+1)+i+1]} +
		\\&
		c_1 v_{[n(N+2)+i-2]} + c_3 v_{[n(N+2)+i-1]} + (c_7 + 1) v_{[n(N+2)+i]} + \\& c_3 v_{[n(N+2)+i+1]} + c_1 v_{[n(N+2)+i+2]} +
		\\&
		c_2 v_{[n(N+3)+i-1]} + c_3 v_{[n(N+3)+i]} + c_2 v_{[n(N+3)+i+1]} +
		c_1 v_{[n(N+4)+i]}
	).
\end{align*}
%Para obtener el resto de expresiones para el cálculo de $r = \lambda L_h v$

\subsubsection{Implementación usando Eigen}
A este algoritmo se le pueden incluir elementos como un precondicionador, para mejorar su eficiencia, tal y como hace la librería Eigen. El problema de utilizar esta librería es que se han de usar necesariamente las estructuras de datos de la misma.

De esta forma, para utilizar el \textit{solver} de Eigen es necesario ``rellenar'' su tipo de matriz \textit{sparse} (\verb|Eigen::SparseMatrix|) y su formato de vector (\verb|Eigen::VectorXf|). Para la matriz, podemos utilizar el método \verb|nz| proporcionado por \verb|BilapSparse|, utilizando el número mínimo de operaciones posibles, mientras que para el vector tenemos un acceso secuencial a sus elementos, de forma que ya es eficiente en la memoria caché.

Sin embargo, este enfoque tiene varios puntos negativos. Primero, aunque la operación de ``rellenar'' las estructuras de Eigen fuese lo más eficiente posible, sigue teniendo un coste computacional relativamente elevado, al que hay que añadir el proceso inverso al finalizar el cálculo del sistema. Segundo, el método es general, por lo que en el cómputo del producto matriz-vector no se asume ninguna estructura concreta de la matriz y se utiliza un método para matrices \textit{sparse} general.
% En este caso, la complejidad computacional del producto matriz-vector es de $\bigO(n^2)$, siendo $n = NM$ el tamaño del vector.
Por último, el control sobre la paralelización de esta implementación es prácticamente nulo al tratarse de una librería externa.

% En cualquier caso, la mejora en el tiempo de ejecución en comparación a la implementación propuesta en~\cite{nuno2018tfg} es más que considerable.

\subsubsection{Implementación \textit{ad-hoc}}
Para subsanar los problemas de la librería Eigen en el uso del gradiente conjugado, se ha optado por realizar una implementación del método única y específicamente para este problema.

En esta implementación se hace un uso intensivo de la función \verb|prod_vec()| de la clase \verb|BilapSparse|. Esta función permite realizar un máximo de trece multiplicaciones y doce sumas por elemento del vector resultado. De esta forma, el producto matriz-vector solo tiene complejidad $\bigO(25n)$, disminuyendo el número de operaciones y ahorrando tiempo de cálculo.

En esta implementación no se incluye un precondicionador, pues no se ha encontrado que su uso reduzca el número de iteraciones del método y, por ende, el tiempo de ejecución.

Con estos cambios, la implementación \textit{ad-hoc}, además de ahorrar espacio en memoria, consigue tiempos de ejecución considerablemente mejores que los conseguidos por la implementación que utiliza Eigen como se expone en el capítulo~\ref{cha:resultados}.

\subsection{Paralelización en OpenMP}
%Todas las implementaciones realizadas para este método de reducción de ruido han sido paralelizadas con el objetivo de aprovechar las arquitecturas modernas de CPU y GPU. En esta sección prestaremos atención a las partes de los programas más relevantes a la hora de aplicar el paralelismo.

El algoritmo de gradiente conjugado se apoya fuertemente en tres operaciones: el producto matriz-vector, el producto escalar y la suma de vectores.
Comenzaremos comentando estas dos últimas operaciones. En el código~\ref{cod:reduction} se muestra un ejemplo donde se realizan las operaciones $x = x + \alpha p$, $r = r - \alpha a_p$ y $r_\text{new} = r^\intercal \cdot r$.
%
\lstinputlisting[caption={Ejemplo de suma de vectores y producto escalar de vectores.}, label={cod:reduction}]{code/reduction.cpp}
%
En esta sección de código se pueden destacar las siguientes características
\begin{itemize}
	\item Línea~1: directiva de OpenMP para la ejecución en paralelo de un bucle y la ejecución de una operación de reducción de suma sobre \verb|rsnew|. Las \verb|n| iteraciones del lazo se dividirán equitativamente entre los hilos disponibles. Para la reducción, cada \textit{thread} almacena un resultado parcial en una copia privada de \verb|rsnew| que, cuando finalicen todos los hilos, se combinarán en el resultado final.

	\item Líneas 4 y 5: dado que en la línea 4 se opera sobre elementos de \verb|r|, estos estarán en caché, por lo que se puede aprovechar la localidad de estos datos para computar \verb|rsnew| sin penalizaciones de memoria.
\end{itemize}

Para el producto matriz-vector, se exponen los códigos~\ref{cod:matrix-vec-upper} y~\ref{cod:matrix-vec-mid}.
%
\lstinputlisting[caption={Operaciones de la primera fila de bloques en el producto $r=(\lambda L_h + 1) v$.}, label={cod:matrix-vec-upper}]{code/matrix-vec-upper.cpp}
%
\lstinputlisting[caption={Operaciones del las filas 2 a $M-2$ de bloques en el producto $r=(\lambda L_h + 1) v$.}, label={cod:matrix-vec-mid}]{code/matrix-vec-mid.cpp}
%
En las filas $1, 2, M-1$ y $M$ de bloques de la matriz, se paraleliza un lazo similar al de la línea~8 del código~\ref{cod:matrix-vec-upper}, donde cada hilo calcula un elemento de \verb|res|. Sin embargo, en el código~\ref{cod:matrix-vec-mid} podemos aplicar el paralelismo en dos lazos. Si paralelizamos el lazo de la línea~2, se repartirá el trabajo a nivel de fila de bloques de $L_h$, mientras
que paralelizando el de la línea~14 se repartiría a nivel de elemento de \verb|res|.
Paralelizar el lazo de la línea~14 implicaría efectuar un reparto de trabajo muy fino entre los hilos y efectuar $\mathtt{num\_blocks}-4$ sincronizaciones en la región paralela, penalizando el rendimiento.
Por ello, se aplica paralelismo en el nivel más alto. En casos en que tengamos un número de hilos mayor que $\mathtt{num\_blocks-4}$ se podría aplicar paralelismo anidado, aunque recordemos que el número de bloques viene determinado por el número de columnas de la imagen que será, presumiblemente, mucho mayor que el número de \textit{cores} de un procesador.


\subsection{Paralelización en CUDA} \label{sec:implementacion-var-cuda}
En primer lugar, se debe comentar que no se ha utilizado CUDA con la librería Eigen debido a dos motivos. Por un lado, para utilizar CUDA con Eigen se deben utilizar versiones específicas para que sean compatibles. Por otro, con la paralelización en OpenMP ya se ha mostrado que se puede implementar el producto matriz--vector del problema~\ref{pro:rof-final-discreto} de una forma eficiente aprovechando la estructura singular de la matriz $L_h$.

Sobre la implementación del algoritmo~\ref{alg:conj-gradient} en CUDA, cabe destacar que la suma de vectores es una operación realmente rápida si se atiende a la coalescencia de las lecturas en la memoria. Además, la mayoría de operaciones del algoritmo se pueden aplicar como casos particulares de $s = \alpha x + y$. En los códigos~\ref{cod:saxpy-2v} y~\ref{cod:saxpy-3v} se muestran las implementaciones de la operación SAXPY.
%
\lstinputlisting[caption={Implementación de $y = \alpha x + y$ en CUDA.}, label={cod:saxpy-2v}]{code/saxpy-2v.cpp}
%
\lstinputlisting[caption={Implementación de $s = \alpha x + y$ en CUDA.}, label={cod:saxpy-3v}]{code/saxpy-3v.cpp}
%
Nótese que, de utilizar sólo la implementación~\ref{cod:saxpy-3v} se estaría perdiendo una oportunidad de optimización del compilador. En el código~\ref{cod:saxpy-2v}, el compilador puede reutilizar el registro de \verb|y[i]|, efectuando menos operaciones de carga en memoria y reduciendo el tiempo de cálculo.

Para el producto escalar en CUDA, se hace uso de la librería Thrust~\cite{hoberock2010thrust} pues tiene versiones optimizadas de esta operación según la arquitectura de la GPU. Nótese que el producto escalar implica efectuar una operación de reducción, altamente costosa en GPU en comparación a otras, como SAXPY. Por ello, se ha decidido utilizar esta librería.
Por último, en Thrust se deben emplear las estructuras propias de esta librería, aunque en nuestro caso es suficiente con copiar la dirección de los datos del vector, sin tener que realizar la copia de los elementos del vector. En el código~\ref{cod:thrust} se muestra la rutina para el producto escalar en CUDA.
%
\lstinputlisting[caption={Producto escalar usando Thrust.}, label={cod:thrust}]{code/thrust.cpp}
%
La paralelización del producto matriz--vector es más sencilla en CUDA, pues el trabajo se reparte a nivel de elementos del vector resultado. En el código~\ref{cod:matrix-vec-cuda} se puede ver una sección del \textit{kernel}.
%
\lstinputlisting[caption={Producto matriz--vector en CUDA.}, label={cod:matrix-vec-cuda}]{code/matrix-vec-cuda.cpp}
%
% NECESARIA ESTA PORCIÓN DE CÓDIGO???
