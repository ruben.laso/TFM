\section{Imágenes en escala de grises} \label{sec:resultados-bn}
En estas pruebas se evalúan el método variacional y los de difusión tratando únicamente el brillo.
Sobre las imágenes \verb|cameraman|, \verb|florencia|, \verb|lena| y \verb|shapes| se han aplicado ruido gaussiano y \textit{salt \& pepper}.

\subsection{Rendimiento computacional}
Para el análisis del rendimiento se han seleccionado las imágenes \verb|lena| y \verb|florencia|, pues tienen dimensiones muy diferentes que permiten evaluar el rendimiento de los algoritmos e implementaciones en diferentes situaciones.

\subsubsection{Método variacional}
En primer lugar se compara el rendimiento del programa del método variacional (ver detalles de implementación en el capítulo~\ref{cha:implementacion}) con el \textit{script} de MATLAB realizado en~\cite{nuno2018tfg}. En la tabla~\ref{tab:comparativa-matlab} se exponen los tiempos de ejecución de estos métodos utilizando la imagen \verb|lena| con ruido gaussiano.
%
\input{tabs/results/comparativa-matlab}

En estos resultados puede observarse cómo, con la simple traducción del código a C++ utilizando la librería Eigen, se ha mejorado el tiempo de ejecución en dos órdenes de magnitud. Adicionalmente, utilizando la implementación optimizada con la clase \verb|BilapSparse|, se reduce este tiempo hasta tres órdenes de magnitud.
Nótese que en este caso se utiliza una imagen de pequeñas dimensiones. Al aumentar el tamaño de la imagen, las diferencias son aún mayores.

A causa del reducido tamaño de la imagen, las implementaciones para CPU rinden ligeramente mejor que aquellas para GPU. La implementación en CUDA consigue realizar el cómputo muy rápidamente, pero es insuficiente para compensar la necesaria copia de los datos desde la memoria principal del computador a la de la GPU.
En el caso de las CPUs, la ejecución en el Intel Xeon Phi KNL es más lenta que en Intel Core i5 puesto que los núcleos del KNL trabajan a una frecuencia mucho más baja. Sin embargo, con un tamaño del problema suficientemente grande, la gran cantidad de \textit{cores} del KNL compensará que estos sean más lentos.

Debido a los tiempos de ejecución tan pequeños, no es posible sacar conclusiones válidas del \textit{speedup} de las implementaciones. Por ello, en la tabla~\ref{tab:comparativa-var-florencia} se muestran los tiempos de ejecución en el tratamiento de la imagen \verb|florencia|, de mayores dimensiones que \verb|lena|. En la tabla~\ref{tab:speedup-comparativa-var-florencia} y en la figura~\ref{fig:speedup-comparativa-var-florencia} se muestran los datos de aceleración de estas ejecuciones. En estos resultados no se incluye el tiempo de ejecución de la implementación en MATLAB debido a que sería demasiado alto (para imágenes de dimensiones $1024 \times 1024$ ya necesita unas cinco horas y media, aproximadamente). % 19417.030059 seconds

\input{tabs/results/comparativa-var-florencia}

\input{tabs/results/speedup-comparativa-var-florencia}

\input{figs/results/graficas/speedup-comparativa-var-florencia}

En estos datos se puede ver que tanto la implementación de Eigen como la \textit{ad-hoc} tienen un \textit{speedup} sensiblemente inferior al ideal. En el caso de la implementación que utiliza Eigen se debe principalmente al proceso de construcción de la matriz del sistema, que requiere de un ancho de banda de memoria mayor del disponible. En el caso de la implementación \textit{ad-hoc}, se debe al bajo tiempo de cada una de las ejecuciones del método de gradiente conjugado, donde los procesos de sincronización de \textit{threads} enmascaran el menor tiempo de cómputo.

Debe mencionarse que, una vez que el tamaño del problema es suficientemente grande, el alto grado de paralelismo del Intel Xeon Phi KNL consigue mejorar los tiempos del Intel Core i5, como cabía esperar, e incluso los de la implementación en CUDA.

Finalmente, resulta interesante fijarse en cómo ha aumentado el tiempo de ejecución una vez que ha aumentado el tamaño del problema. Mientras que las implementaciones para CPU han aumentado el tiempo hasta dos órdenes de magnitud, la de CUDA sólo se ha incrementado en un orden, mostrando un mejor comportamiento en este sentido.

\subsubsection{Métodos de difusión del brillo}
Para evaluar los métodos de difusión del brillo, se ha utilizado directamente la imagen \verb|florencia|. En la tabla~\ref{tab:comparativa-brightness-florencia} se muestran los tiempos de ejecución de 30 pasos de tiempo de los flujos isotrópicos y anisotrópicos. Además, en la tabla~\ref{tab:speedup-comparativa-brightness-florencia} y figura~\ref{fig:speedup-comparativa-brightness-florencia} se muestran los datos relativos al \textit{speedup}.

\input{tabs/results/comparativa-brightness-florencia}

\input{tabs/results/speedup-comparativa-brightness-florencia}

\input{figs/results/graficas/speedup-comparativa-brightness-florencia}

En estos resultados puede observarse que el flujo anisotrópico tiene una considerable penalización en rendimiento en el Intel Core i5.
% Sin embargo, esto no se ve en el resto de casos, por lo que seguramente los compiladores NVCC e ICC realicen optimizaciones que el GCC no ha hecho.
Este problema de rendimiento está ligado a la llamada a la función \verb|cbrt()| para calcular la raíz cúbica presente en este flujo, ver~\eqref{eq:brillo-anisotropico}.
Algunas arquitecturas incorporan instrucciones específicas para reducir el número de ciclos necesarios en este tipo de cálculos, que son muy costosos en computación. Así pues, se hace evidente que las unidades aritméticas en coma flotante del Intel Core~i5 no soportan estas instrucciones.

Salvo en ese caso, el rendimiento de ambos tipos de flujo es muy similar.
Además, la implementación en el Intel Xeon Phi es la más rápida, mejorando a la GPU cuando se utilizan más de 32 \textit{threads}.

El \textit{speedup} de estas soluciones es notablemente bueno, escalando hasta los 68 \textit{threads} en el Intel Xeon Phi. Para 136 y 272 hilos, el tiempo de ejecución no mejora (e incluso empeora), por lo que el código no es capaz de aprovechar el \textit{hyperthreading}~\cite{marr2002hyperthreading}, seguramente por una demanda de ancho de banda de memoria demasiado alta.

Finalmente, es interesante comparar el tiempo transcurrido en el cálculo del flujo y en el de la energía, mostrado en la tabla~\ref{tab:desglose-brightness-florencia}.
En estos datos se confirma que el elemento que perjudica al rendimiento en el flujo anisotrópico es la función \verb|cbrt()| pues, especialmente en el Intel Core i5, los tiempos de cómputo del flujo son considerablemente mayores que los de la energía.
Sin embargo, en la ejecución en CUDA, el tiempo de cómputo del flujo de difusión es menor que el de la energía. Esto se debe a que en este cálculo está incluida una operación de reducción, muy costosa en CUDA, tal y como se comentó en la sección~\ref{sec:implementacion-var-cuda}. Por otro lado, en el Intel Xeon Phi, ambas operaciones se comportan prácticamente de la misma forma en cuanto a tiempo de ejecución y \textit{speedup}.
%
\input{tabs/results/desglose-brightness-florencia}
%


\subsection{Calidad en la reconstrucción}
Para realizar la comparación en la eficacia de los métodos se ha aplicado ruido sobre las imágenes \verb|cameraman| y \verb|shapes|, siendo especialmente interesante esta última, pues las formas geométricas nos permitirán apreciar, en detalle, el comportamiento de los métodos propuestos.

En la figura~\ref{fig:denoise-cameraman} se muestra un ejemplo de tratamiento de la imagen \verb|cameraman|, en escala de grises, con ruido gaussiano. En la tabla~\ref{tab:denoise-cameraman-psnr} se muestra el PSNR de la reconstrucción para varios tipos de ruido.

\input{figs/results/cameraman}

\input{tabs/results/cameraman}

Sobre los resultados visuales cabe destacar que los métodos consiguen reconstrucciones muy similares, aunque es cierto que con el método variacional (figura~\ref{fig:cameraman-var}) se produce un efecto de difuminación en la imagen evidente, y un marco negro en los bordes de la misma. La difuminación está causada por las condiciones de regularidad impuestas a la solución del problema~\ref{pro:rof-final-discreto} y el marco negro es debido a las condiciones de contorno del mismo.

Atendiendo al PSNR, se puede comprobar que el ruido \textit{salt \& pepper} provoca mayores distorsiones en la imagen, que los métodos variacional y difusivos no son capaces de subsanar satisfactoriamente. Sin embargo, es especialmente destacable la calidad en la reconstrucción del filtro de la mediana ante este tipo de ruido (figura~\ref{fig:cameraman-salt-pepper-median}), que visualmente consigue un resultado prácticamente impecable.

\input{figs/results/cameraman-sp}

En la figura~\ref{fig:denoise-shapes-bn} se muestran los resultados al tratar el fichero \verb|shapes|, en concreto, se muestra una esquina del cuadrado situado en la parte superior izquierda de la imagen.

\input{figs/results/shapes-crop}

\input{tabs/results/shapes}

En esta imagen, el filtro de la mediana~(figura~\ref{fig:shapes-median-bn}) funciona especialmente bien, atendiendo al PSNR (ver tabla~\ref{tab:denoise-shapes}), debido a que la imagen sólo toma valores blanco o negro, aunque visualmente no es tan destacable. En las reconstrucciones podemos advertir que el método variacional (figura~\ref{fig:shapes-var-bn}) tiene un efecto de borrosidad más homogéneo que el flujo isotrópico~(figura~\ref{fig:shapes-iso-bn}) que presenta algunos artefactos en el borde del objeto. Por otro lado, el flujo anisotrópico (figura~\ref{fig:shapes-aniso-bn}) consigue preservar casi a la perfección los bordes en las direcciones paralelas a los ejes, aunque tiene problemas en esquinas pronunciadas, que tiende a redondear.

Por último, en la tabla~\ref{tab:denoise-florencia} se muestra el PSNR conseguido tras el tratamiento de la imagen \verb|florencia|. Para estas pruebas se ha escogido un factor $\rho_b = 0.1$ pues se ha encontrado una mejora notable respecto a usar el valor por defecto $\rho_b = 0.3$.
\input{tabs/results/florencia}
De estos datos es interesante comentar que los métodos parecen aportar mejores resultados cuanto mayor es la dimensión de la imagen. Además, tanto el método variacional como el flujo isotrópico proporcionan las mejores reconstrucciones, dejando a un lado el filtro de la mediana en el tratamiento del ruido \textit{salt \& pepper}.
