\section{Imágenes en color}
Para el tratamiento de imágenes en color se han utilizado los programas \verb|color_var| y \verb|color_bc|, en sus implementaciones para CPU y GPU. Debe comentarse que en esta sección sólo se exponen los resultados de la implementación \textit{ad-hoc} del método variacional, pues ya se ha mostrado en la sección~\ref{sec:resultados-bn} que sus tiempos de ejecución son considerablemente menores a los de la implementación con la librería Eigen.

\subsection{Rendimiento computacional}
Para la evaluación de rendimiento de las reconstrucciones de imágenes se ha utilizado únicamente la imagen \verb|florencia_color|.

\subsubsection{Método variacional}
En la tabla~\ref{tab:florencia-variacional} se pueden ver los tiempos de ejecución del método variacional para imágenes en color, y su \textit{speedup} en la tabla~\ref{tab:speedup-florencia-variacional} y figura~\ref{fig:speedup-florencia-variacional}.

\input{tabs/results/florencia-variacional}
% \input{tabs/results/florencia-final}

\input{tabs/results/speedup-florencia-variacional}

\input{figs/results/graficas/speedup-florencia-variacional}

En estos resultados se puede comprobar que el tiempo de ejecución al tratar una imagen en color es de, aproximadamente, el triple que al tratar una imagen en escala de grises de las mismas dimensiones. Para el \textit{speedup}, se tiene el mismo comportamiento que en el tratamiento de imágenes en escala de grises. Esto era de esperar, pues se aplica exactamente el mismo método a tres matrices en lugar de hacerlo sobre una única matriz.

\subsubsection{Métodos de difusión de la cromaticidad}
En la tabla~\ref{tab:comparativa-chroma-florencia} se muestran los tiempos de ejecución de 30 pasos de tiempo en el tratamiento de la cromaticidad de la imagen \verb|florencia_color| y en la tabla~\ref{tab:speedup-chroma-florencia} y la figura~\ref{fig:speedup-chroma-florencia} se muestran los respectivos datos de \textit{speedup}.

\input{tabs/results/comparativa-chroma-florencia}

\input{tabs/results/speedup-chroma-florencia}

\input{figs/results/graficas/speedup-chroma-florencia}

La diferencia en tiempo de ejecución entre el flujo isotrópico y el anisotrópico es notable y está causada por la necesaria comprobación de que los denominadores no son cero, ver código~\ref{cod:croma-aniso} en la sección~\ref{sec:implementacion-difusivo}. La ejecución en el Intel i5 7600 es la más perjudicada por la introducción de estas comprobaciones, y la diferencia entre ambos tipos es de un factor 5 aproximadamente. En el Intel Xeon Phi, la diferencia es de un factor 4, y del doble en la implementación en CUDA.
Estas diferencias se deben principalmente a las optimizaciones del compilador, mucho más efectivas en los compiladores de NVIDIA e Intel que en el de GNU.

El \textit{speedup} de estas implementaciones es prácticamente ideal. En el Intel Xeon Phi, el rendimiento del programa escala hasta los 68 \textit{threads}, estancándose a partir de ahí debido a que el requisito de ancho de banda de memoria es demasiado alto cuando se utiliza \textit{hyperthreading}.

Los resultados de la tabla~\ref{tab:desglose-chroma-florencia} confirman la diferencia en el tiempo de ejecución entre los tipos de flujo isotrópico y anisotrópico. Nótese que el tiempo de cómputo de la energía es mayor en el tratamiento de la cromaticidad puesto que pasamos de tener matrices con elementos en $\mathbb{R}$ a matrices con elementos en $\mathbb{R}^3$, algo que aumenta el número de cálculos a efectuar y perjudica al efecto de localidad de los datos en las memorias caché.

\input{tabs/results/desglose-chroma-florencia}

Por último, cabe comentar que los tiempos de ejecución del método variacional son menores que los de los métodos difusivos en ejecuciones secuenciales, sobre todo en comparación al flujo anisotrópico.
%Sin embargo, debido a su pobre \textit{speedup} se ve ligeramente perjudicado en las ejecuciones paralelas donde la difusión anisotrópica obtiene tiempos de cómputo similares y el método de difusión utilizando flujo isotrópico supera el rendimiento del método variacional.
Incluso a pesar de su pobre \textit{speedup}, este método consigue un mejor rendimiento en el Intel Xeon Phi KNL, aunque esto no sucede en la GTX 1050~Ti y en el Intel Core~i5, donde el enfoque difusivo tiene un rendimiento ligeramente superior en algunos casos.
%
Adicionalmente, cabe destacar que el método variacional tiene la ventaja de que trabaja sobre brillo y cromaticidad simultáneamente, a diferencia de los métodos difusivos que lo hacen por separado.


\subsection{Calidad en la reconstrucción}
Para medir la calidad en la reconstrucción hemos utilizado las imágenes \verb|lena_color|, \verb|peppers| y \verb|shapes|.

En la figura~\ref{fig:chroma-shapes} se muestra un ejemplo de las reconstrucciones obtenidas para la imagen \verb|shapes_color| con ruido gaussiano utilizando flujos de difusión para la cromaticidad (sin tratar el brillo).

\input{figs/results/chroma-shapes}

De los resultados obtenidos debemos mencionar que el flujo anisotrópico consigue una calidad en la reconstrucción ligeramente mejor a nivel visual, esto se puede ver en las figuras~\ref{fig:shapes-iso-crop} y~\ref{fig:shapes-aniso-crop}. En estas figuras se ha saturado el color para apreciar mejor que, en la zona que debiera ser blanca, el flujo isotrópico presenta desviaciones cromáticas mayores que el anisotrópico.

Sobre el PSNR (tabla~\ref{tab:chroma-shapes}), ambos consiguen resultados prácticamente idénticos para el ruido gaussiano, aunque el flujo anisotrópico es ligeramente mejor para el \textit{salt \& pepper}.

\input{tabs/results/chroma-shapes}

En la figura~\ref{fig:lena} y en la tabla~\ref{tab:lena} se pueden ver los resultados de tratar la imagen \verb|lena_color| con el método variacional (ampliado al tratamiento de imágenes en RGB) y los métodos difusivos\footnote{Por cuestiones de espacio, en la figura~\ref{fig:lena} no se muestran todas las combinaciones posibles.} (tratando brillo y cromaticidad).

\input{figs/results/lena}

\input{tabs/results/lena}

Las combinaciones de los flujos de difusión consiguen resultados ligeramente mejores que el método variacional. Esto se debe principalmente a que las condiciones de regularidad del método variacional son muy estrictas, provocando que no se consigan conservar los bordes de los objetos en las imágenes.

Uno de los problemas del método variacional generalizado para el tratamiento de imágenes en color es que, si el ruido se agrupa en uno de los canales (y el usuario no puede saber en cuál), el método los tratará todos por igual, provocando que se pierdan detalles en la imagen y que la reconstrucción no sea óptima. 

En la figura~\ref{fig:peppers} y la tabla~\ref{tab:peppers} se muestra el resultado del tratamiento de la imagen \verb|peppers| a la que sólo se ha añadido ruido en el canal correspondiente al color verde.

Los flujos de difusión de la cromaticidad presentan un mejor comportamiento al tratar el color como vectores de tres componentes en lugar de tratar las bandas RGB de forma independiente como hace el método variacional. En concreto, la difusión anisotrópica es la que mejor resultado obtiene, tanto en PSNR como visualmente. Entre la difusión isotrópica y el método variacional, las diferencias son muy sutiles, aunque favorables al enfoque difusivo.

\input{figs/results/peppers}

\input{tabs/results/peppers}


%El tratamiento mediante flujos anisotrópicos es el que mejor funciona, especialmente a nivel visual. El método variacional apenas aumenta el PSNR, aunque visualmente sí consigue mejorar ligeramente. Igualmente, los flujos de difusión isotrópica consiguen un resultado ligeramente mejor que el variacional, aunque sigue estando lejos de los flujos de difusión anisotrópica.
