\section{Arquitectura CUDA} \label{secs:gpus}
La arquitectura CUDA (\textit{Compute Unified Device Architecture}) es transversal al software y al hardware. En los paradigmas de computación ``tradicionales'' todo el código se ejecuta en la CPU. Sin embargo, en GPGPU (\textit{General Purpose computing on Graphics Processing Units}) se combina el uso de CPUs y GPUs para maximizar el rendimiento de los programas.

\subsection{Software CUDA}
En cuanto al software, CUDA proporciona un compilador y un conjunto de herramientas y librerías para utilizar la GPU a través de una extensión de los lenguajes de programación C y C++\footnote{A través de \textit{wrappers}, también es posible usar CUDA en otros lenguajes de programación como Fortran, Java o Python}.

El compilador, el NVCC (\textit{NVIDIA CUDA Compiler})~\cite{NVCCguide}, está basado en LLVM (\textit{Low Level Virtual Machine})~\cite{lattner2004llvm}. Brevemente, NVCC separa el código del \textit{host} (CPU) y el del \textit{device} (GPU) y los compila de forma separada. El código del \textit{host} se compila utilizando cualquier compilador de C/C++ como ICC o GCC, mientras que el del \textit{device} lo compila el propio NVCC.

El código CUDA se diferencia del código del \textit{host} por el uso de ciertas extensiones. Las funciones ejecutadas en la GPU se conocen como \textit{kernels}. Un ejemplo de \textit{kernel} se muestra en el código~\ref{lst:kernel-example}, el cual implementa el código SAXPY ($y = \alpha x + y$, $x,y \in \mathbb{R}^n, \alpha \in \mathbb{R}$).

\input{code/kernel-example}

Un \textit{kernel} se define utilizando la \textit{keyword} \verb|__global__| (\verb|__device__| se reserva a funciones llamadas desde un \textit{kernel}) y debe ser invocado de una forma singular, utilizando \verb|<<<dimGrid,dimBlock>>>|, donde \verb|dimGrid| indica la dimensión del \textit{grid} y \verb|dimBlock| la dimensión de cada bloque.

\input{code/kernel-call-example}

\subsection{Hardware CUDA}
En el hardware de CUDA es importante conocer que existe una jerarquía de \textit{threads} y otra de memoria. La mayor diferencia entre una CPU y una GPU a nivel de arquitectura es la jerarquía de \textit{threads}, mucho más simple en una CPU. Por otro lado, el uso efectivo de la jerarquía de memoria es más crítica en la GPU debido a que sus unidades de control, \textit{prefetchers}, etc. son menos sofisticadas que en una CPU.

\subsubsection{Jerarquía de \textit{threads}}
La arquitectura hardware de CUDA está construida alrededor de un conjunto de SMs (\textit{Streaming Multiprocessors}) que se encargarán de la creación, \textit{scheduling} y ejecución de \textit{warps} (grupos de 32 \textit{threads}).

Cuando se invoca un \textit{grid} de \textit{kernels} desde la CPU, se lanza un conjunto de bloques, y cada bloque contiene un grupo de \textit{threads} organizados en \textit{warps}, en el que cada hilo ejecutará las instrucciones del \textit{kernel}.
Se debe notar que, por conveniencia y simplicidad en el manejo de los hilos, tanto el \textit{grid} como cada uno de los bloques puede entenderse como una entidad tridimensional. En la figura~\ref{fig:cuda-thread-hierarchy} se muestra un ejemplo de \textit{grid} de dimensión $3 \times 2 \times 1$ con bloques de dimensión $4 \times 3 \times 1$.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.75\textwidth]{figs/arquitectura/cuda-thread-hierarchy/thread-hierarchy}
	\caption{Jerarquía de \textit{threads} en CUDA.}
	\label{fig:cuda-thread-hierarchy}
\end{figure}

Debido a que la unidad mínima de ejecución en CUDA es el \textit{warp}, los 32 hilos que lo componen efectuarán las mismas instrucciones sobre operandos potencialmente diferentes, introduciendo así el paralelismo SIMT (\textit{Single Instruction, Multiple Threads}), como variante del paradigma SIMD (\textit{Single Instruction, Multiple Data})~\cite{duncan1990survey}.

\subsubsection{Jerarquía de memoria}
La jerarquía de memoria de las GPUs de NVIDIA es cada vez más similar a la de una CPU, y aunque siguen existiendo diferentes tipos de memorias (global, local, de texturas, cachés, etc.), su manejo se simplifica de cara al programador a medida que el compilador adquiere más responsabilidades en la fase de optimización de código.

De forma simplificada, cada \textit{thread} cuenta con una pequeña memoria local. Cada bloque cuenta con una memoria compartida a nivel de bloque, que perdura tanto como el propio bloque. Finalmente, todos los threads tienen acceso a la memoria global del \textit{device}.

Adicionalmente, existen dos memorias \textit{read-only}, la memoria de constantes y la de texturas y superficies. La memoria de constantes está especialmente diseñada para el almacenamiento de constantes utilizadas durante la ejecución de un \textit{kernel} con una latencia mucho menor que la memoria global. La memoria de texturas y superficies proporciona diferentes formas de direccionamiento, además de algunos filtros para los datos que almacena.

Internamente, en las últimas GPUs de NVIDIA se incluyen memorias caché de primer y segundo nivel, aunque su manejo escapa de las manos del programador más allá de conocer que existen e intentar sacar provecho de las mismas mejorando la localidad de sus códigos.

\subsection{Rendimiento de las arquitecturas CUDA}
En el paradigma de computación paralela GPGPU se deben tener en cuenta dos factores principales, la coalescencia en el acceso a memoria y la ocupancia de los multiprocesadores.

Como se recoge en~\cite{harris2013coalesce}, al realizar accesos ``coalescentes'' se saca el mayor provecho posible del ancho de banda de la memoria de la GPU, reduciendo la latencia y mejorando el rendimiento del \textit{kernel} en cuestión. Para conseguir realizar accesos coalescentes se deben ubicar los datos consecutivamente dentro de la memoria. Por ejemplo, si el hilo $n$-ésimo accede al dato $i$-ésimo, el hilo $(n+1)$-ésimo debe acceder al dato $(i+1)$-ésimo.

% Sobre la ocupancia, debemos comentar que no siempre una mayor ocupancia implica un mayor rendimiento, aunque si la ocupancia es baja, el rendimiento lo será también.
La ocupancia se define como la relación entre los \textit{warps} activos en un SM y el máximo número de \textit{warps} que pueden estar activos en el mismo.
Si la ocupancia es baja, obtendremos un rendimiento bajo, pues no hay suficientes \textit{warps} para enmascarar la latencia entre instrucciones dependientes. Si es demasiado alta, es posible llegar a degradar el rendimiento debido a la reducción de los recursos por \textit{thread}.
Por ello, no siempre una mayor ocupancia implica un mayor rendimiento~\cite{volkov2015occupancy}, aunque si la ocupancia es baja, generalmente el rendimiento lo será también.
