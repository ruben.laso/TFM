\section{Intel Xeon Phi \textit{Knights Landing}} \label{secs:intel-xeon-phi}
Las arquitecturas \textit{manycore} como la del Intel Xeon Phi \textit{Knights Landing} (KNL) proporcionan entornos con un alto número de \textit{cores} dentro de un mismo chip, que permiten explotar las capacidades de cálculo paralelo de los algoritmos ejecutados en estos procesadores.%, principalmente utilizados en el ámbito científico.

\subsection{Arquitectura del Intel Xeon Phi KNL}
El Intel Xeon Phi es un procesador \textit{manycore} que basa su arquitectura en \textit{tiles} o celdas~\cite{jeffers2016intel}. Cada una de estas celdas contiene dos núcleos y una memoria caché de segundo nivel de 1 MB. A su vez, cada \textit{core} es capaz de ejecutar hasta 4 hilos de forma simultánea, dispone de dos VPUs (\textit{Vector Processing Units}), ver figura~\ref{fig:knl-tiles}, y dos cachés de primer nivel de 32 KB para datos e instrucciones respectivamente.
\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.5\linewidth]{figs/arquitectura/knl-tiles/knl-tiles}
	\caption{Topología de un \textit{tile} del Intel Xeon Phi KNL.}
	\label{fig:knl-tiles}
\end{figure}
%
Los \textit{tiles} están replicados físicamente en el procesador 38 veces, aunque, como máximo, sólo estarán en funcionamiento 36 de ellos, que se organizarían tal y como se muestra en la figura~\ref{fig:knl-topology}. Así, el KNL contaría con hasta 72 \textit{cores} y 144 VPUs.
\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.7\linewidth]{figs/arquitectura/knl-topology}
	\caption{Arquitectura del Intel Xeon Phi KNL. (CHA:~\textit{caching/home agent}; DDR MC: \textit{DDR memory controller}; EDC: \textit{\MCDRAM{} controllers}; IIO: \textit{integrated I/O controller}; \MCDRAM{}: \textit{Multi-Channel DRAM}).}
	\label{fig:knl-topology}
\end{figure}

\subsection{\textit{Cores} y VPUs}
Los \textit{cores} utilizados en el Intel Xeon Phi KNL están basados en el Intel Atom, al que se le han aplicado ciertas modificaciones necesarias para su uso en HPC. Entre estas modificaciones se encuentra el soporte para hasta 4 hilos de ejecución (por lo que podemos tener un máximo de 288 hilos ejecutándose de forma concurrente en la máquina\footnote{El modelo utilizado en este trabajo dispone de 68~\textit{cores}, con un máximo de 272~hilos.}), que provocarán un reparto de los recursos del núcleo. Así se definen tres modos en función de los hilos ubicados en cada núcleo: \textit{single-thread} cuando hay un único hilo activo, \textit{dual-thread} cuando dos cualesquiera estén en activo, y \textit{quad-core} cuando tres o cuatro hilos se estén ejecutando~\cite{sodani2016knights}.

Como novedad en el Intel Xeon Phi, se han introducido las instrucciones vectoriales AVX-512, que permiten efectuar, en una única instrucción, 8~operaciones de multiplicación y suma en doble precisión~\cite{reinders2013avx}. Además, se incorpora un mecanismo para efectuar la lectura/escritura de datos dispersos en memoria mediante \textit{gathers} y \textit{scatters} automáticos.

\subsection{Clustering}
Adicionalmente, se puede modificar la afinidad entre los \textit{cores}, los directorios y la memoria, localizando el tráfico de datos en la malla de interconexión. Así se dispone de tres modos diferentes:
\begin{itemize}
	\item \textit{All-to-all}: no establece ninguna afinidad.

	\item \textit{Quadrant}: divide el chip del KNL en cuatro regiones (cuadrantes) de forma que quedan ligados directorios y memorias, pero los \textit{tiles} siguen sin asociación. Cada directorio sólo accederá a las memorias de su cuadrante, pero los \textit{tiles} pueden acceder a cualquier directorio.

	\item \SNC{}: extiende al modo anterior, de forma que cada \textit{tile} se asocia a un directorio y cada directorio a una memoria. Esto hace que el chip se comporte como una máquina NUMA (\textit{Non-Uniform Memory Access}).
\end{itemize}
De estos modos, el más interesante en el sentido de que permite un mayor margen de mejora en el rendimiento con optimizaciones de código es el \SNC{}~\cite{laso2017tfg}, pues provoca que el tráfico del chip sea más localizado y así puedan ejecutarse más transacciones de forma concurrente dentro de la malla de interconexión. En contraposición, su programación eficiente requiere de un mayor esfuerzo del programador.

\subsection{Memoria \MCDRAM{}}
El Intel Xeon Phi KNL incorpora en el propio chip una memoria de alto rendimiento conocida como \MCDRAM{} (\textit{Multi-Channel DRAM}). Esta memoria tiene una capacidad de 16~GB y tiene un ancho de banda de unos 450~GB/s (aproximadamente 4 veces más que una memoria DDR4 convencional). Esta memoria puede ser configurada de tres formas diferentes, cambiando totalmente su papel dentro de la jerarquía de memoria (ver figura~\ref{fig:cache-vs-flat-mcdram}):
\begin{itemize}
	\item \textit{Cache} (figura~\ref{fig:mcdram-cache}): la \MCDRAM{} se configura como una caché para la totalidad de la memoria DDR principal. Esta caché funciona con mapeado directo y las etiquetas son ubicadas en los bits que se reservan para corrección de errores en los otros modos. Debido a la política de reemplazo directo, los conflictos en este nivel no causan una gran penalización en el rendimiento, aunque sigue existiendo un pequeño \textit{overhead} en caso de fallo en la caché. En este modo no es necesario realizar modificaciones en el código para utilizar la \MCDRAM{}~\cite{asai2016mcdram}.

%	\begin{figure}[htbp]
%		\centering
%		\includegraphics[width=0.7\linewidth]{figs/arquitectura/mcdram-cache/mcdram-cache}
%		\caption{MCDRAM como memoria caché L3.}
%		\label{fig:mcdram-cache}
%	\end{figure}

	\item \textit{Flat} (figura~\ref{fig:mcdram-flat}): en este modo, la \MCDRAM{} funciona como otra memoria principal, con un rendimiento mayor (y una capacidad menor) que una DDR. Para beneficiarse de este modo se han de ubicar los datos del programa explícitamente en la \MCDRAM{}, mediante la librería \verb|hbw_malloc.h| incluida en \verb|memkind|~\cite{cantalupo2015memkind}.

%	\begin{figure}[htbp]
%		\centering
%		\includegraphics[width=0.7\linewidth]{figs/arquitectura/mcdram-flat/mcdram-flat}
%		\caption{MCDRAM como memoria principal.}
%		\label{fig:mcdram-flat}
%	\end{figure}

	\item \textit{Hybrid}: se divide en un reparto 50\%-50\% ó 25\%-75\% la memoria \MCDRAM{} en modo caché y \textit{flat} respectivamente.
\end{itemize}

\begin{figure}[htbp]
	\centering
	\begin{subfigure}[t]{.45\textwidth}
		\centering
		\includegraphics[width=\linewidth]{figs/arquitectura/mcdram-cache/mcdram-cache}
		\caption{\MCDRAM{} como memoria caché L3 (modo \textit{cache}).}
		\label{fig:mcdram-cache}
	\end{subfigure}
	\qquad
	\begin{subfigure}[t]{.45\textwidth}
		\centering
		\includegraphics[width=\linewidth]{figs/arquitectura/mcdram-flat/mcdram-flat}
		\caption{\MCDRAM{} como memoria principal (modo \textit{flat}).}
		\label{fig:mcdram-flat}
	\end{subfigure}
	\caption{Jerarquía de memoria en función del modo de la \MCDRAM{}.}
	\label{fig:cache-vs-flat-mcdram}
\end{figure}

\subsection{Rendimiento del Intel Xeon Phi KNL}
Teóricamente, este procesador tiene un rendimiento pico de 3~\TFlops{}. Sin embargo, el rendimiento real de un procesador queda lejos de estas magnitudes teóricas debido a diversos factores, como la propia complejidad de los códigos, las latencias de las memorias, las dependencias de datos, etc.

En diversos trabajos se ha estudiado el Intel Xeon Phi KNL y su rendimiento en diferentes tipos de aplicaciones \cite{rosales2016comparative, heinecke2016high, doerfler2016applying, ramos2017capability}. Como se ha mencionado anteriormente, el modo \SNC{} es el más interesante en términos de rendimiento potencial, donde es crítico utilizar la jerarquía de memoria del procesador de una forma eficiente para poder sacar partido de las unidades vectoriales y obtener los mejores tiempos de ejecución~\cite{laso2018knl}.

Entre los modos \textit{flat} y \textit{cache} para la MCDRAM no suele haber una diferencia notable, por lo que se ha optado por realizar una implementación más general que pueda ser usada en otros procesadores y que utilice en el modo \textit{cache} del Intel Xeon Phi \textit{Knights Landing}.
% por lo que no priorizaremos el uso de ninguno de ellos en especial.
% Nótese que, al usar el modo \textit{flat} deberemos ejecutar los programas de forma que se ubiquen los datos en la MCDRAM para sacar provecho del alto ancho de banda de esta memoria.
