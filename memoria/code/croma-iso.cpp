template<class M, class T>
inline void isotropic_diffusion(const M & a, M & r, const float delta) {
    const size_t nr = a.rows - 1;
    const size_t nc = a.cols - 1;
    T dx, dy, lapl, res;

    #pragma omp parallel private(dx, dy, lapl, res) \
        firstprivate(nr, nc, delta) shared(a, r) default(none)
    {
        // Upper boundary
        #pragma omp for nowait
        for (size_t j = 1; j < nc; j++) {
            dx = dx_c<M,T>(a, 0, j);
            dy = dy_f<M,T>(a, 0, j);
            ...
        }
        ...
    }
    ...
}        
