for (size_t i = 2; i < nr-1; i++) {
    for (size_t j = 2; j < nc-1; j++) {
        const T gx1 = dx_c<M,T>(a, i, j+1) / (0 != gradient_norm<M,T>(a, i, j+1) ? gradient_norm<M,T>(a, i, j+1) : 1);
        const T gy1 = dy_c<M,T>(a, i+1, j) / (0 != gradient_norm<M,T>(a, i+1, j) ? gradient_norm<M,T>(a, i+1, j) : 1);

        const T gx2 = dx_c<M,T>(a, i, j-1) / (0 != gradient_norm<M,T>(a, i, j-1) ? gradient_norm<M,T>(a, i, j-1) : 1);
        const T gy2 = dy_c<M,T>(a, i-1, j) / (0 != gradient_norm<M,T>(a, i-1, j) ? gradient_norm<M,T>(a, i-1, j) : 1);

        const T dx = (gx1 - gx2) * 0.5;
        const T dy = (gy1 - gy2) * 0.5;

        const T _div = dx + dy;
        const T res = a.at(i,j) + delta * (_div + a.at(i,j) * gradient_norm<M,T>(a, i, j));
        r.at(i,j) = res / norm2(res);
    }
}
