#pragma omp parallel for reduction(+:rsnew)
for (size_t j = 0; j < n; j++) {
    x[j] += alpha * p[j];
    r[j] -= alpha * ap[j];
    rsnew += r[j] * r[j];
}
