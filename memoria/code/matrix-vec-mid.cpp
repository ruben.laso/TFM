// MEDIUM ROWS OF BLOCKS [2, N-2)
#pragma omp parallel for private (N, i)
for (N = 0; N < num_blocks-4; N++) {
    res[(N+2)*n] = c6*v[N*n] +
        c4*v[N*n+n]     + c5*v[N*n+n+1]   +
        c1_1*v[N*n+2*n] + c2*v[N*n+2*n+1] + c3*v[N*n+2*n+2] +
        c4*v[N*n+3*n]   + c5*v[N*n+3*n+1] +
        c6*v[N*n+4*n];
    res[(N+2)*n+1] = c6*v[N*n+1] +
        c5*v[N*n+n]   + c4*v[N*n+n+1]   + c5*v[N*n+n+2]   +
        c2*v[N*n+2*n] + c1*v[N*n+2*n+1] + c2*v[N*n+2*n+2] + c3*v[N*n+2*n+3] +
        c5*v[N*n+3*n] + c4*v[N*n+3*n+1] + c5*v[N*n+3*n+2] +
        c6*v[N*n+4*n+1];
    for (i = 2; i < n-2; i++) {
        res[(N+2)*n+i] = c6*v[N*n+i] +
            c5*v[N*n+n+i-1]   + c4*v[N*n+n+i]     + c5*v[N*n+n+i+1]   +
            c3*v[N*n+2*n+i-2] + c2*v[N*n+2*n+i-1] + c1*v[N*n+2*n+i]   + c2*v[N*n+2*n+i+1] + c3*v[N*n+2*n+i+2] +
            c5*v[N*n+3*n+i-1] + c4*v[N*n+3*n+i]   + c5*v[N*n+3*n+i+1] +
            c6*v[N*n+4*n+i];
    }
    i = n - 2;
    res[(N+2)*n+i] = c6*v[N*n+i] +
        c5*v[N*n+n+i-1]   + c4*v[N*n+n+i]     + c5*v[N*n+n+i+1]   +
        c3*v[N*n+2*n+i-2] + c2*v[N*n+2*n+i-1] + c1*v[N*n+2*n+i]   + c2*v[N*n+2*n+i+1] +
        c5*v[N*n+3*n+i-1] + c4*v[N*n+3*n+i]   + c5*v[N*n+3*n+i+1] +
        c6*v[N*n+4*n+i];
    i++;
    res[(N+2)*n+i] = c6*v[N*n+i] +
        c5*v[N*n+n+i-1]   + c4*v[N*n+n+i]     +
        c3*v[N*n+2*n+i-2] + c2*v[N*n+2*n+i-1] + c1_1*v[N*n+2*n+i] +
        c5*v[N*n+3*n+i-1] + c4*v[N*n+3*n+i]   +
        c6*v[N*n+4*n+i];
}
