template<class T>
__global__ void saxpy(const T a, const T * __restrict__ x, T * __restrict__ y, const size_t n) {
    const auto i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < n) {
        y[i] = a * x[i] + y[i];
    }
}
