template<class M, class T, class VT>
__global__ void energy_CUDA(const M a, T * e) {
    const auto i = blockIdx.y * blockDim.y + threadIdx.y;
    const auto j = blockIdx.x * blockDim.x + threadIdx.x;
    const size_t nr = a.rows - 1;
    const size_t nc = a.cols - 1;
    const size_t cols = a.cols;

    if (i > nr || j > nc) {
        return;
    }
    else if (i > 0 && j > 0 && i < nr && j < nc) { // Central cases
        e[i * cols + j] = norm2_pow2(dx_c<M,VT>(a, i, j), dy_c<M,VT>(a, i, j));
    }
    else if (i == 0 && j > 1 && j < nc) { // Upper boundary
        e[0 * cols + j] = norm2_pow2(dx_c<M,VT>(a, 0, j), dy_f<M,VT>(a, 0, j));
    }
    else if (i == nc && j > 1 && j < nc) { // Lower boundary
        e[nr * cols + j] = norm2_pow2(dx_c<M,VT>(a, nr, j), dy_b<M,VT>(a, nr, j));
    }
    else if (j == 0 && i > 1 && i < nr) { // Left boundary
        e[i * cols + 0] = norm2_pow2(dx_f<M,VT>(a, i, 0), dy_c<M,VT>(a, i, 0));
    }
    else if (j == nc && i > 1 && i < nr) { // Right boundary
        e[nr * cols + j] = norm2_pow2(dx_b<M,VT>(a, i, nc), dy_c<M,VT>(a, i, nc));
    }
    else if (i == 0 && j == 0) { // Upper-left corner
        e[0 * cols + 0] = norm2_pow2(dx_f<M,VT>(a, 0, 0), dy_f<M,VT>(a, 0, 0));
    }
    else if (i == 0 && j == nc) { // Upper-right corner
        e[0 * cols + nc] = norm2_pow2(dx_b<M,VT>(a, 0, nc), dy_f<M,VT>(a, 0, nc));
    }
    else if (i == nr && j == 0) { // Lower-left corner
        e[nr * cols + 0] = norm2_pow2(dx_f<M,VT>(a, nr, 0), dy_b<M,VT>(a, nr, 0));
    }
    else if (i == nr && j == nc) { // Lower-right corner
        e[nr * cols + nc] = norm2_pow2(dx_b<M,VT>(a, nr, nc), dy_b<M,VT>(a, nr, nc));
    }

    __syncthreads();
    T _energy = 0.0;

    if ( i < nr && j < nc ) {
        _energy = ( e[i * cols + j] +
                    e[(i+1) * cols + j] +
                    e[i * cols + (j+1)] +
                    e[(i+1) * cols + (j+1)]
                  ) * 0.25;
    }

    __syncthreads();
    e[i * cols + j] = _energy;
}

template<class M, class T, class VT>
inline T energy(const M & a_cuda, thrust::device_vector<T> e, const dim3 dimBlock, const dim3 dimGrid) {
    energy_CUDA<M,T,VT><<<dimGrid, dimBlock>>>(a_cuda, thrust::raw_pointer_cast(&e[0]));
    return thrust::reduce(e.begin(), e.end());
}
