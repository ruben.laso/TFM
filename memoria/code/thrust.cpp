template<class T>
inline T dot_prod_GPU(T * a, const size_t n) {
    thrust::device_ptr<T> dev_a(a);
    return thrust::inner_product(dev_a, dev_a + n, dev_a, 0.0f);
}
