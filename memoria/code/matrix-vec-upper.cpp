// FIRST ROW OF BLOCKS OF ROWS
res[0] = c1_2*v[0] + c2*v[1] + c3*v[2] +
    c4*v[n] + c5*v[n+1] +
    c6*v[2*n];
res[1] = c2*v[0] + c1_1*v[1] + c2*v[2] + c3*v[3] +
    c5*v[n] + c4*v[n+1] + c5*v[n+2] +
    c6*v[2*n+1];
#pragma omp parallel for private(i)
for (i = 2; i < n-2; i++) {
    res[i] = c3*v[i-2] + c2*v[i-1] + c1_1*v[i] + c2*v[i+1] + c3*v[i+2] +
        c5*v[n+i-1] + c4*v[n+i] + c5*v[n+i+1] +
        c6*v[2*n+i];
}
i = n-2;
res[i] = c3*v[i-2] + c2*v[i-1] + c1_1*v[i] + c2*v[i+1] +
    c5*v[n+i-1] + c4*v[n+i] + c5*v[n+i+1] +
    c6*v[2*n+i];
i++;
res[i] = c3*v[i-2] + c2*v[i-1] + c1_2*v[i] +
    c5*v[n+i-1] + c4*v[n+i] +
    c6*v[2*n+i];
