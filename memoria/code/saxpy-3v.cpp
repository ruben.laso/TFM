template<class T>
__global__ void saxpy(T * __restrict__ s, const T a, const T * __restrict__ x, const T * __restrict__ y, const size_t n) {
    const auto i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < n) {
        s[i] = a * x[i] + y[i];
    }
}
