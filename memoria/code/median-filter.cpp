for (size_t i = ws; i < im.rows - ws; i++) {
    for (size_t j = ws; j < im.cols - ws; j++) {
        std::vector<T> v(window_size * window_size);
        size_t pos = 0;
        for (size_t ii = i - ws; ii <= i + ws; ii++) {
            for (size_t jj = j - ws; jj <= j + ws; jj++) {
                v[pos++] = im.at<T>(ii, jj);
            }
        }
        std::sort(v.begin(), v.end());
        // median value
        r.at<T>(i,j) = v[window_size * window_size / 2 + 1];
    }
}
