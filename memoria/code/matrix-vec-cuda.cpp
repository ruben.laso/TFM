else if (NN > 1 && NN < num_blocks - 2) { // Mid blocks
    if (i == 0) { // First row of mid block
        res[ii] = A.c6*v[N*n] +
            A.c4*v[N*n+n]     + A.c5*v[N*n+n+1]   +
            A.c1_1*v[N*n+2*n] + A.c2*v[N*n+2*n+1] + A.c3*v[N*n+2*n+2] +
            A.c4*v[N*n+3*n]   + A.c5*v[N*n+3*n+1] +
            A.c6*v[N*n+4*n];
    }
    else if (i == 1) { // Second row of mid block
        res[ii] = A.c6*v[N*n+1] +
            A.c5*v[N*n+n]   + A.c4*v[N*n+n+1]   + A.c5*v[N*n+n+2]   +
            A.c2*v[N*n+2*n] + A.c1*v[N*n+2*n+1] + A.c2*v[N*n+2*n+2] + A.c3*v[N*n+2*n+3] +
            A.c5*v[N*n+3*n] + A.c4*v[N*n+3*n+1] + A.c5*v[N*n+3*n+2] +
            A.c6*v[N*n+4*n+1];
    }
    else if (i > 1 && i < n-2) { // Mid rows of mid block
        res[ii] = A.c6*v[N*n+i] +
            A.c5*v[N*n+n+i-1]   + A.c4*v[N*n+n+i]     + A.c5*v[N*n+n+i+1]   +
            A.c3*v[N*n+2*n+i-2] + A.c2*v[N*n+2*n+i-1] + A.c1*v[N*n+2*n+i]   + A.c2*v[N*n+2*n+i+1] + A.c3*v[N*n+2*n+i+2] +
            A.c5*v[N*n+3*n+i-1] + A.c4*v[N*n+3*n+i]   + A.c5*v[N*n+3*n+i+1] +
            A.c6*v[N*n+4*n+i];
    }
    else if (i == n-2) { // Penultimate row of mid block
        res[ii] = A.c6*v[N*n+i] +
            A.c5*v[N*n+n+i-1]   + A.c4*v[N*n+n+i]     + A.c5*v[N*n+n+i+1]   +
            A.c3*v[N*n+2*n+i-2] + A.c2*v[N*n+2*n+i-1] + A.c1*v[N*n+2*n+i]   + A.c2*v[N*n+2*n+i+1] +
            A.c5*v[N*n+3*n+i-1] + A.c4*v[N*n+3*n+i]   + A.c5*v[N*n+3*n+i+1] +
            A.c6*v[N*n+4*n+i];
    }
    else if (i == n-1) { // Last row of mid block
        res[ii] = A.c6*v[N*n+i] +
            A.c5*v[N*n+n+i-1]   + A.c4*v[N*n+n+i]     +
            A.c3*v[N*n+2*n+i-2] + A.c2*v[N*n+2*n+i-1] + A.c1_1*v[N*n+2*n+i] +
            A.c5*v[N*n+3*n+i-1] + A.c4*v[N*n+3*n+i]   +
            A.c6*v[N*n+4*n+i];
    }
}
